$(function () {	
	
	var paginaCarritoCorreoElectronicoRequerido = $("#paginaCarritoCorreoElectronicoRequerido").html();
	var paginaCarritoCorreoElectronicoInvalido = $("#paginaCarritoCorreoElectronicoInvalido").html();

    $("#validateEmailUser").validate({
     rules: {
        correo: { 
        	required: true,
        	email: true
        }
      },
      messages: {
        correo: { 
        	required: paginaCarritoCorreoElectronicoRequerido,
        	email: paginaCarritoCorreoElectronicoInvalido
        }
      },
      errorElement : 'span',
      errorLabelContainer: '#errorMsgCorreo'    
    });
  });


function showPopupEmail() {
	$('#popupEmail').bPopup();
}

function closePopup(id) {
	$("#usuarioCorreo").val("");
	$("#errorMsgCorreo").empty();
	$("#"+id).bPopup().close();
}

function validateEmailUser(){
	
	$(".loader-page").fadeIn();	
	if (!$('#validateEmailUser').valid()) {
		$(".loader-page").fadeOut(10);
		return false;
    }

	$("#validateEmailUser").submit();
}


function refreshProduct(product,remove,amount){
	var form = $("#idFormRmoveProduct");
	var url = form.attr('action');					 
	var data = "product="+product+"&remove="+remove+"&amount="+amount;
	var paginaCarritoErrorPeticion = $("#paginaCarritoErrorPeticion").html();
	
	$.ajax({
		url : url,		
		type: 'GET',
		async: true,	
		data : data,
		beforeSend: function(){
			$(".loader-page").fadeIn();
		},    
		success : function(data) {	
			$(".loader-page").fadeOut(10);
			if(data.indexOf("error_section")>=0){	
				$("#errorModificarProducto").empty();
				$("#errorModificarProducto").css("display","block");
				$("#errorModificarProducto").append('<span id="idCorreoElectronico-error" class="error" style="display: inline;">'+paginaCarritoErrorPeticion+'</span>');
			}else{
				location.reload();		
			}			
		},
		error : function(result) {			
			$(".loader-page").fadeOut(10);
			$("#errorModificarProducto").empty();
			$("#errorModificarProducto").css("display","block");
			$("#errorModificarProducto").append('<span id="idCorreoElectronico-error" class="error" style="display: inline;">'+paginaCarritoErrorPeticion+'</span>');
		}
	});	
}


function getOrderPdf(){
	var form = $("#idFormGetOrderPdf");
	var url = form.attr('action');	
	var paginaCarritoErrorPeticion = $("#paginaCarritoErrorPeticion").html();				 	
	
	$.ajax({
		url : url,		
		type: 'GET',
		async: true,
		beforeSend: function(){
			$(".loader-page").fadeIn();
		},    
		success : function(data) {	
			$(".loader-page").fadeOut(10);
			if(data.exitoso){
				var a = document.createElement('a');
				var pdfAsDataUri = "data:application/pdf;base64," + data.respuesta;
				a.download = 'cotizacionJMI.pdf';
				a.type = 'application/pdf';
				a.href = pdfAsDataUri;
				a.click();
			}else{
				$(".loader-page").fadeOut(10);
				$("#errorModificarProducto").empty();
				$("#errorModificarProducto").css("display","block");
				$("#errorModificarProducto").append('<span id="idCorreoElectronico-error" class="error" style="display: inline;">'+paginaCarritoErrorPeticion+'</span>');
			}						
		},
		error : function(result) {			
			$(".loader-page").fadeOut(10);
			$("#errorModificarProducto").empty();
			$("#errorModificarProducto").css("display","block");
			$("#errorModificarProducto").append('<span id="idCorreoElectronico-error" class="error" style="display: inline;">'+paginaCarritoErrorPeticion+'</span>');
		}
	});	
}