$('#popupWhatsApp').whatsappChatSupport();

function mostrarYocultarPopupProductoAgregado(){	 
	 $("#idDivProductoAgregadoCarritoCompras").fadeIn(500);
	 setTimeout(function() {
        $("#idDivProductoAgregadoCarritoCompras").fadeOut(1500);
    },3000);
}

function calculateTotalAmount(){				
	var form = $("#idFormAddProduct");
	var formURL = $("#idFormCalculateTotalAmount");
	var url = formURL.attr('action');	
		
	$.ajax({
		url : url,		
		type: 'POST',
		async: true,	
		data : $("#idFormAddProduct").serialize(),
		beforeSend: function(){
			$(".loader-page").fadeIn();
		},    
		success : function(data) {			
			$(".loader-page").fadeOut(10);						
			if(data.exitoso){
				$("#idCurrentPriceProduct").html(data.respuesta);			
			}else{
				$("#errorAgregarProducto").empty();
				$("#errorAgregarProducto").css("display","block");
				$("#errorAgregarProducto").append('<span id="idCorreoElectronico-error" class="error" style="display: inline;">Error al enviar petición</span>');
			}
		},
		error : function(result) {			
			$(".loader-page").fadeOut(10);
			$("#errorAgregarProducto").empty();
			$("#errorAgregarProducto").css("display","block");
			$("#errorAgregarProducto").append('<span id="idCorreoElectronico-error" class="error" style="display: inline;">Error al enviar petición</span>');
		}
	});	
}

function addProduct(event){			
	event.preventDefault();
	
	if ($("#listaCategoriaTermopares").length>0){
		if ($("#idContadorCategoriasTermopar").length<=0){
			$("#errorAgregarProducto").empty();
			var paginaProductoAgregarCaracteristicasProducto = $("#paginaProductoAgregarCaracteristicasProducto").html();
			$("#errorAgregarProducto").append('<span id="idCorreoElectronico-error" class="error" style="display: inline;">'+paginaProductoAgregarCaracteristicasProducto+'</span>');
			return false;
		}
	}
		
	var form = $("#idFormAddProduct");
	var url = form.attr('action');	
		
	$.ajax({
		url : url,		
		type: 'POST',
		async: true,	
		data : $("#idFormAddProduct").serialize(),
		beforeSend: function(){
			$(".loader-page").fadeIn();
		},    
		success : function(data) {			
			$(".loader-page").fadeOut(10);						
			if(data.indexOf("error_section")>=0){	
				$("#errorAgregarProducto").empty();
				$("#errorAgregarProducto").css("display","block");
				$("#errorAgregarProducto").append('<span id="idCorreoElectronico-error" class="error" style="display: inline;">Error al enviar petición</span>');				
			}else{
				$("#idRespuestaControlador").empty();
				$("#idRespuestaControlador").html(data);
			
				var html = $("#idRespuestaControlador").find(".offcanvas_menu").html();
				$(".offcanvas_menu").empty();
				$(".offcanvas_menu").html(html);
								
				html = $("#idRespuestaControlador").find(".header_area").html();
				$(".header_area").empty();
				$(".header_area").html(html);
				
				$(".loader-page").fadeOut(10);
				
				mostrarYocultarPopupProductoAgregado();		
					
			}			
		},
		error : function(result) {			
			$(".loader-page").fadeOut(10);
			$("#errorAgregarProducto").empty();
			$("#errorAgregarProducto").css("display","block");
			$("#errorAgregarProducto").append('<span id="idCorreoElectronico-error" class="error" style="display: inline;">Error al enviar petición</span>');
		}
	});	
}

function getFeaturesTermoparValue(element){
	$("#errorAgregarProducto").empty();
	var elementSelectedSeleccionado = $(element).attr("forthree");

	var valueElement = $('#' + elementSelectedSeleccionado).attr("value");
	var classElement = $(element).attr("forOne");
	$('.' + classElement).attr("value","");
	$('.' + classElement).prop("checked",false);
	
	var elementSelected = $(element).attr("forTwo");
	$('#' + elementSelected).val(true);

	$('#' + elementSelectedSeleccionado).attr("value",valueElement);
	$('#' + elementSelectedSeleccionado).prop("checked",true);

	var url = $("#idFormGetFeaturesTermopar").attr('action');
	
	$.ajax({
		url : url,		
		type: 'POST',
		async: true,	
		data : $("#idFormAddProduct").serialize(),
		beforeSend: function(){
			$(".loader-page").fadeIn();
		},    
		success : function(data) {	
			$(".loader-page").fadeOut(10);						
			if(data.indexOf("error_section")>=0){					
				$("#errorAgregarProducto").empty();
				$("#errorAgregarProducto").css("display","block");
				$("#errorAgregarProducto").append('<span id="idCorreoElectronico-error" class="error" style="display: inline;">Error al enviar petición</span>');					
			}else{
				$("#idRespuestaControlador").empty();
				$("#idRespuestaControlador").html(data);
						
				var html = $("#idRespuestaControlador").find("#accordionFeatureTermopar").html();
				$("#accordionFeatureTermopar").empty();
				$("#accordionFeatureTermopar").html(html);
				
				$('#accordionFeatureTermopar [data-toggle="tooltip"]').tooltip({					
				    at: "left center", 
				    my: "right top",
				    html: true
				});
								
				$(".loader-page").fadeOut(10);
				getImageFeaturesTermopar(1);				
			}			
		},
		error : function(result) {			
			$(".loader-page").fadeOut(10);
			$("#errorAgregarProducto").empty();
			$("#errorAgregarProducto").css("display","block");
			$("#errorAgregarProducto").append('<span id="idCorreoElectronico-error" class="error" style="display: inline;">Error al enviar petición</span>');
		}
	});	
}


function getFeaturesTermopar(element){	
	$("#errorAgregarProducto").empty();
	var valueElement = $(element).attr("value");
	var classElement = $(element).attr("class");
	var elementSelected = $(element).attr("for");
	var url = $("#idFormGetFeaturesTermopar").attr('action');
	
	$('.' + classElement).attr("value","");
	$('.' + classElement).prop("checked",false);
	$('#' + elementSelected).val(true);
	$(element).attr("value",valueElement);
	$(element).prop("checked",true);
		
	$.ajax({
		url : url,		
		type: 'POST',
		async: true,	
		data : $("#idFormAddProduct").serialize(),
		beforeSend: function(){
			$(".loader-page").fadeIn();
		},    
		success : function(data) {	
			$(".loader-page").fadeOut(10);						
			if(data.indexOf("error_section")>=0){					
				$("#errorAgregarProducto").empty();
				$("#errorAgregarProducto").css("display","block");
				$("#errorAgregarProducto").append('<span id="idCorreoElectronico-error" class="error" style="display: inline;">Error al enviar petición</span>');					
			}else{
				$("#idRespuestaControlador").empty();
				$("#idRespuestaControlador").html(data);
						
				var html = $("#idRespuestaControlador").find("#accordionFeatureTermopar").html();
				$("#accordionFeatureTermopar").empty();
				$("#accordionFeatureTermopar").html(html);
								
				$('#accordionFeatureTermopar [data-toggle="tooltip"]').tooltip({					
				    at: "left center", 
				    my: "right top",
				    html: true
				});
								
				$(".loader-page").fadeOut(10);
				getImageFeaturesTermopar(1);				
			}			
		},
		error : function(result) {			
			$(".loader-page").fadeOut(10);
			$("#errorAgregarProducto").empty();
			$("#errorAgregarProducto").css("display","block");
			$("#errorAgregarProducto").append('<span id="idCorreoElectronico-error" class="error" style="display: inline;">Error al enviar petición</span>');
		}
	});	
}

function getImageFeaturesTermopar(pkProducto){
    var url = $("#idFormGetImageFeaturesTermopar").attr('action');
    
    var data=""
	var pkcaracteristicas='';
		
    var idProducto=$("#idProductoPkproducto").val();    
	for(var i=0; i<=100; i++) {
		if ( $(".myTermoparClass"+i).length > 0 ) {
			var valueRadio = $('input[class="myTermoparClass'+i+'"]:checked').val();
			if(valueRadio != "undefined" && valueRadio != null) 
				if(i==0)
					pkcaracteristicas = valueRadio; 				
				else
					pkcaracteristicas = pkcaracteristicas + "," + valueRadio; 
		}	
	}
	
	data = "pkProducto="+idProducto+"&pkCaracteristicas="+pkcaracteristicas;
	
	$.ajax({
		url : url,		
		type: 'GET',
		async: true,	
		data : data,
		beforeSend: function(){
			$(".loader-page").fadeIn();
		},    
		success : function(data) {							
			$(".loader-page").fadeOut(10);
			if(data.exitoso){
				$("#idImagenProducto").attr("data-zoom-image",data.respuesta);				
				$("#idImagenProducto").attr("src",data.respuesta);					
				if(data.codigo=='true'){
					$("#idCantidadItems").removeAttr('disabled');
					$("#idButtonAddCart").removeClass('buttonBloqueado');	
					$("#idMessageTextNotAddCart").hide();
				}else{
					$("#idCantidadItems").attr('disabled','disabled')
					$("#idButtonAddCart").addClass('buttonBloqueado');
					$("#idMessageTextNotAddCart").show();	
				}
				
				calculateTotalAmount();
						
			}
			
			
		},
		error : function(result) {			
			$(".loader-page").fadeOut(10);
		}
	});	
}





