var paginaPagocorreoElectronicoRequerido = $("#paginaPagocorreoElectronicoRequerido").html();
var paginaPagocorreoElectronicoInvalido = $("#paginaPagocorreoElectronicoInvalido").html();
var paginaPagocodigoRequerido = $("#paginaPagocodigoRequerido").html();
var paginaPagonombresRequeridos = $("#paginaPagonombresRequeridos").html();
var paginaPagotelefonoRequerido = $("#paginaPagotelefonoRequerido").html();
var paginaPagorfcRequerido = $("#paginaPagorfcRequerido").html();
var paginaPagoapellidosRequeridos = $("#paginaPagoapellidosRequeridos").html();
var paginaPagocorreosNoCoinciden = $("#paginaPagocorreosNoCoinciden").html();
var paginaPagorazonSocialRequerida = $("#paginaPagorazonSocialRequerida").html();
var paginaPagocodigoPostalRequerido = $("#paginaPagocodigoPostalRequerido").html();
var paginaPagoestadoRequerido = $("#paginaPagoestadoRequerido").html();
var paginaPagonumeroExteriorRequerido = $("#paginaPagonumeroExteriorRequerido").html();
var paginaPagocalleRequerido = $("#paginaPagocalleRequerido").html();
var paginaPagodelegacionRequerido = $("#paginaPagodelegacionRequerido").html();
var paginaPagocoloniaRequerida = $("#paginaPagocoloniaRequerida").html();
var paginaPagonumeroInteriorRequerido = $("#paginaPagonumeroInteriorRequerido").html();
var paginaPagoreferenciaRequerido = $("#paginaPagoreferenciaRequerido").html();
var paginaPagoerrorPeticion = $("#paginaPagoerrorPeticion").html();
var paginaPagodatosPersonalesAsterisco = $("#paginaPagodatosPersonalesAsterisco").html();
var paginaPagoopcionesEntregaAsterisco = $("#paginaPagoopcionesEntregaAsterisco").html();
var paginaPagodatosFacturacionAsterisco = $("#paginaPagodatosFacturacionAsterisco").html();
var paginaPagodatosPersonales = $("#paginaPagodatosPersonales").html();
var paginaPagoopcionesEntrega = $("#paginaPagoopcionesEntrega").html();
var paginaPagodatosFacturacion = $("#paginaPagodatosFacturacion").html();
var paginaPagoerrorPago = $("#paginaPagoerrorPago").html();
var paginaPagonombresInvalido = $("#paginaPagonombresInvalido").html();
var paginaPagoapellidosInvalido = $("#paginaPagoapellidosInvalido").html();



(function ($) {

	$(".loader-page").fadeOut(10);
	
	jQuery.validator.addMethod("lettersonly", function(value, element) {
	  return this.optional(element) || /^[a-z\s]+$/i.test(value);
	}, "Letters only please"); 
	
	if ($('#idFormSendCode').length!=0) {
		$("#idFormSendCode").validate({
	     rules: {
	        correo: { 
	        	required: true,
	        	email: true
	        }
	      },
	      messages: {
	        correo: { 
	        	required: paginaPagocorreoElectronicoRequerido,
	        	email: paginaPagocorreoElectronicoInvalido
	        }
	      },
	      errorElement : 'span',
	      errorLabelContainer: '#errorMsgCorreo'    
	    });
	}
    
     if ($('#idFormValidateCode').length!=0) {
	     $("#idFormValidateCode").validate({
	     rules: {
	        codigo: { 
	        	required: true
	        }
	      },
	      messages: {
	        codigo: { 
	        	required: paginaPagocodigoRequerido
	        }
	      },
	      errorElement : 'span',
	      errorLabelContainer: '#errorMsgCodigo'    
	    });
	}


	if ($('#idFormDataUser').length!=0) {
	     $("#idFormDataUser").validate({
	     ignore: [],
	     rules: {
	        "cliente.nombres": { 
	        	required: true
	        },
	        "cliente.correo": { 
	        	required: true,
	        	email: true
	        },
	        "cliente.telefono": { 
	        	required: true,
	        	minlength: 10
	        },
	        "cliente.apellidos": { 
	        	required: true
	        },
	        "cliente.rfc": { 
	        	required: true
	        },
	        "correoConfirmacion": {
	        	validateEmail: true,
	        	email: true        	
	        }        
	      },
	      messages: {
	        "cliente.nombres": { 
	        	required: paginaPagonombresRequeridos,
	        	lettersonly: paginaPagonombresInvalido
	        },
	        "cliente.correo": { 
	        	required: paginaPagocorreoElectronicoRequerido,
	        	email: paginaPagocorreoElectronicoInvalido
	        },
	        "cliente.telefono": { 
	        	required: "Telefono requerido",
	        	minlength: "Telefono requerido"
	        },
	        "cliente.rfc": { 
	        	required: paginaPagorfcRequerido
	        },
	        "cliente.apellidos": { 
	        	required: paginaPagoapellidosRequeridos,
	        	lettersonly: paginaPagoapellidosInvalido
	        },
	        "correoConfirmacion": {
	        	validateEmail: paginaPagocorreosNoCoinciden,
	        	email: paginaPagocorreoElectronicoInvalido
	        }
	      },
	      errorLabelContainer: '.errorTxt'      
	    });
	 }


	 if ($('#idFormDataFacturation').length!=0) {
	     $("#idFormDataFacturation").validate({
	     ignore: [],
	     rules: {
	        "cliente.listaDatosFiscales[0].razonSocial": { 
	        	facturationData: true
	        },
	        "cliente.listaDatosFiscales[0].codigoPostal": { 
	        	facturationData: true,
	        	number: true
	        },
	        "cliente.listaDatosFiscales[0].estado": { 
	        	facturationData: true
	        },
	        "cliente.listaDatosFiscales[0].noExterior": { 
	        	facturationData: true
	        },
	        "cliente.listaDatosFiscales[0].calle": {
	        	facturationData: true        	
	        },
	        "cliente.listaDatosFiscales[0].rfc": {
	        	facturationData: true        	
	        },
	        "cliente.listaDatosFiscales[0].delegacion": {
	        	facturationData: true        	
	        },
	        "cliente.listaDatosFiscales[0].colonia": {
	        	facturationData: true        	
	        },
	        "cliente.listaDatosFiscales[0].noInterior": {
	        	facturationData: false        	
	        }        
	      },
	      messages: {
	        "cliente.listaDatosFiscales[0].razonSocial": { 
	        	facturationData: paginaPagorazonSocialRequerida
	        },
	        "cliente.listaDatosFiscales[0].codigoPostal": { 
	        	facturationData: paginaPagocodigoPostalRequerido,
	        	number: paginaPagocodigoPostalRequerido        	
	        },
	        "cliente.listaDatosFiscales[0].estado": { 
	        	facturationData: paginaPagoestadoRequerido
	        },
	        "cliente.listaDatosFiscales[0].noExterior": { 
	        	facturationData: paginaPagonumeroExteriorRequerido
	        },
	        "cliente.listaDatosFiscales[0].calle": {
	        	facturationData: paginaPagocalleRequerido
	        },
	        "cliente.listaDatosFiscales[0].rfc": {
	        	facturationData: paginaPagodelegacionRequerido     	
	        },
	        "cliente.listaDatosFiscales[0].delegacion": {
	        	facturationData: paginaPagodelegacionRequerido       	
	        },
	        "cliente.listaDatosFiscales[0].colonia": {
	        	facturationData: paginaPagocoloniaRequerida
	        },
	        "cliente.listaDatosFiscales[0].noInterior": {
	        	facturationData: paginaPagonumeroInteriorRequerido
	        }        
	      },
	      errorLabelContainer: '.errorTxt'      
	    });
	}
    
    
	if ($('#idFormDataDelivery').length!=0) {       
	    $("#idFormDataDelivery").validate({
	     ignore: [],
	     rules: {
	        "cliente.listaDomicilios[0].codigoPostal": { 
	        	required: true,
	        	number: true
	        },
	        "cliente.listaDomicilios[0].estado": { 
	        	required: true
	        },
	        "cliente.listaDomicilios[0].noExterior": { 
	        	required: true
	        },
	        "cliente.listaDomicilios[0].calle": { 
	        	required: true
	        },
	        "cliente.listaDomicilios[0].delegacion": { 
	        	required: true
	        },
	        "cliente.listaDomicilios[0].colonia": { 
	        	required: true
	        },
	        "cliente.listaDomicilios[0].noInterior": { 
	        	required: false
	        },
	        "cliente.listaDomicilios[0].referencia": { 
	        	required: true
	        }        
	      },
	      messages: {
	        "cliente.listaDomicilios[0].codigoPostal": { 
	        	required: paginaPagocodigoPostalRequerido,
	        	number: paginaPagocodigoPostalRequerido
	        },
	        "cliente.listaDomicilios[0].estado": { 
	        	required: paginaPagoestadoRequerido
	        },
	        "cliente.listaDomicilios[0].noExterior": { 
	        	required: paginaPagonumeroExteriorRequerido
	        },
	        "cliente.listaDomicilios[0].calle": { 
	        	required: paginaPagocalleRequerido
	        },
	        "cliente.listaDomicilios[0].delegacion": { 
	        	required: paginaPagodelegacionRequerido
	        },
	        "cliente.listaDomicilios[0].colonia": { 
	        	required: paginaPagocoloniaRequerida
	        },
	        "cliente.listaDomicilios[0].noInterior": { 
	        	required: paginaPagonumeroInteriorRequerido
	        },
	        "cliente.listaDomicilios[0].referencia": { 
	        	required: paginaPagoreferenciaRequerido
	        }        
	      },
	      errorLabelContainer: '.errorTxt'      
	    });
	}
    
    
    $.validator.addMethod("validateEmail",function(value,element){
	   if(($('input[name="cliente.correo"]').val())==($('input[name="correoConfirmacion"]').val())){
	     return true;
	  }else{
	    return false;
	  }
	},"");
	
	
	$.validator.addMethod("facturationData",function(value,element){
	 	if((value==null || value.length==0) && $("#solicitarFacturacion").is(':checked')){
	    	return false;
	  	}else{
	    	return true;
	  	}
	},"Please input a reason");
    
    
})(jQuery);


function showContainerTax(){	
	if($("#solicitarFacturacion").is(':checked')){
		$("#idContainerTax").show();
	}else{
		$("#idContainerTax").hide();
		$("#idDomicilioFiscalrazonSocial").val("");
		$("#idDomicilioFiscalrfc").val("");
		$("#idDomicilioFiscalcodigoPostal").val("");
		$("#idDomicilioFiscaldelegacion").val("");		
		$("#idDomicilioFiscalcolonia").val("");
		$("#idDomicilioFiscalnoExterior").val("");
		$("#idDomicilioFiscalnoInterior").val("");
		$("#idDomicilioFiscalcalle").val("");
	}
}

function showPopupEmail() {
	$('#popupEmail').bPopup();
}


function showPopupCode() {
	$('#popupCode').bPopup();
	$('#popupEmail').bPopup().close();
}



function ajaxSendCode(){	
	$("#errorMsgCorreo").empty();
	var form = $("#idFormSendCode");
	var url = form.attr('action');	
		
	if (!$('#idFormSendCode').valid()) {
		return false;
    }

	$.ajax({
		url : url,		
		type: 'GET',
		async: true,	
		data : $("#idFormSendCode").serialize(),
		beforeSend: function(){
			$(".loader-page").fadeIn();
		},    
		success : function(result) {
			$(".loader-page").fadeOut(10);
			if(result.exitoso){
				$("#idCorreoElectronicoCodigo").val($("#idCorreoElectronico").val());
				$("#idTextoCorreoElectronico").empty();
				$("#idTextoCorreoElectronico").append($("#idCorreoElectronico").val());				
				showPopupCode();
			}else{
				$("#errorMsgCorreo").empty();
				$("#errorMsgCorreo").css("display","block");
				$("#errorMsgCorreo").append('<span id="idCorreoElectronico-error" class="error" style="display: inline;">'+result.mensaje+'</span>');
			}
		},
		error : function(result) {
			$(".loader-page").fadeOut(10);
			$("#errorMsgCorreo").empty();
			$("#errorMsgCorreo").css("display","block");
			$("#errorMsgCorreo").append('<span id="idCorreoElectronico-error" class="error" style="display: inline;">' + paginaPagoerrorPeticion + '</span>');
		}
	});	
}



function ajaxShippingQuotations(){
	$("#errorMsgCorreo").empty();
	$("#secctionListPackageSend").empty();
	if (!$('#idFormDataUser').valid()) {
		return false;					
    }
    
    
    if (!$('#idFormDataDelivery').valid()) {
		return false;									
    }


	var form = $("#idFormShippingQuotations");
	var url = form.attr('action');	
	var data = $("#idFormDataFacturation").serialize() + '&' + $("#idFormDataDelivery").serialize() + '&' + $("#idFormDataUser").serialize(); 
	
	$.ajax({
		url : url,		
		type: 'POST',
		async: true,	
		data : data,
		beforeSend: function(){
			$(".loader-page").fadeIn();
		},    
		success : function(data) {
			$(".loader-page").fadeOut(10);
			if(data.indexOf("error_section")>=0){					
				$("#errorModificarProducto").empty();
				$("#errorModificarProducto").css("display","block");
				$("#errorModificarProducto").append('<span id="idCorreoElectronico-error" class="error" style="display: inline;">' + paginaPagoerrorPeticion + '</span>');
				$("#secctionListPackageSend").empty();					
			}else{
				$("#idRespuestaControlador").empty();
				$("#idRespuestaControlador").html(data);
						
				var html = $("#idRespuestaControlador").find("#secctionListPackageSend").html();
				$("#secctionListPackageSend").empty();
				$("#secctionListPackageSend").html(html);
								
				$(".loader-page").fadeOut(10);			
				initJMainQuery();	
											
			}			
			
		},
		error : function(result) {
			$(".loader-page").fadeOut(10);
			$("#errorModificarProducto").empty();
			$("#errorModificarProducto").css("display","block");
			$("#errorModificarProducto").append('<span id="idCorreoElectronico-error" class="error" style="display: inline;">' + paginaPagoerrorPeticion + '</span>');
		}
	});	
}


const formatter = new Intl.NumberFormat('en-US', {
      style: 'currency',
      currency: 'USD',
      minimumFractionDigits: 2
    })

function onChangePackage(element){	
	$(".product_items").removeClass("myPackageSelected");
	$(".classIdInputEnvio").attr('disabled','disabled');
	$(element).addClass("myPackageSelected");
	$(element).find("#idInputPkEnvio").removeAttr('disabled');
	$(element).find("#idInputPkEnvioSolicitud").removeAttr('disabled');
	var montoTotalCompraProductosString=$("#idInputHiddenCarritoTotal").val();
	
	if(montoTotalCompraProductosString.indexOf(",")>=0)
		var montoTotalCompraProductosString=$("#idInputHiddenCarritoTotal").val().replace(",","");	
		
	var montoTotalCompraProductos=parseFloat(montoTotalCompraProductosString);
	var montoTotalEnvio=parseFloat($(element).find("#idInputMontoEnvio").val());
	var montoTotalCompra=montoTotalCompraProductos+montoTotalEnvio;
	$(".idInputCostoTotalVenta").html(formatter.format(montoTotalCompra));
}


function ajaxValidateCode(){	
	$("#errorMsgCorreo").empty();
	var form = $("#idFormValidateCode");
	var url = form.attr('action');	
	
		
	if (!$('#idFormValidateCode').valid()) {
		return false;
    }

	$.ajax({
		url : url,		
		type: 'GET',
		async: true,	
		data : $("#idFormValidateCode").serialize(),
		beforeSend: function(){
			$(".loader-page").fadeIn();
		},    
		success : function(result) {
			if(result.exitoso){						
				$("#idCorreoElectronicoPago").val($("#idCorreoElectronico").val());
				$("#idFormCheckout").submit();
			}else{
				$(".loader-page").fadeOut(10);
				$("#errorMsgCodigo").empty();
				$("#errorMsgCodigo").css("display","block");
				$("#errorMsgCodigo").append('<span id="idCorreoElectronico-error" class="error" style="display: inline;">Codigo de acceso invalido</span>');
			}
		},
		error : function(result) {		
			$(".loader-page").fadeOut(10);
			$("#errorMsgCodigo").empty();
			$("#errorMsgCodigo").css("display","block");
			$("#errorMsgCodigo").append('<span id="idCorreoElectronico-error" class="error" style="display: inline;">'+result.mensaje+'</span>');
		}
	});	
}


function ajaxValidateCode(){	
	var form = $("#idFormValidateCode");
	var url = form.attr('action');	
	
		
	if (!$('#idFormValidateCode').valid()) {
		return false;
    }

	$.ajax({
		url : url,		
		type: 'GET',
		async: true,	
		data : $("#idFormValidateCode").serialize(),
		beforeSend: function(){
			$(".loader-page").fadeIn();
		},    
		success : function(result) {
			if(result.exitoso){						
				$("#idCorreoElectronicoPago").val($("#idCorreoElectronico").val());
				$("#idFormCheckout").submit();
			}else{
				$(".loader-page").fadeOut(10);
				$("#errorMsgCodigo").empty();
				$("#errorMsgCodigo").css("display","block");
				$("#errorMsgCodigo").append('<span id="idCorreoElectronico-error" class="error" style="display: inline;">Codigo de acceso invalido</span>');
			}
		},
		error : function(result) {		
			$(".loader-page").fadeOut(10);
			$("#errorMsgCodigo").empty();
			$("#errorMsgCodigo").css("display","block");
			$("#errorMsgCodigo").append('<span id="idCorreoElectronico-error" class="error" style="display: inline;">'+result.mensaje+'</span>');
		}
	});	
}


function closePopup(id) {	
	$("#idCorreoElectronicoCodigo").val("");
	$("#idCodigo").val("");
	$("#errorMsgCodigo").empty();
	$("#errorMsgCorreo").empty();
	$("#"+id).bPopup().close();
}


function ajaxGeneratePay(){		
	var enviarPago = true;
	$("#divTituloDatosPersonales").removeClass("menssageErrorTitulo");
	$("#divTituloDatosPersonales").html(paginaPagodatosPersonales);
	$("#divTituloEntrega").removeClass("menssageErrorTitulo");
	$("#divTituloEntrega").html(paginaPagoopcionesEntrega);
	$("#divTituloDatosFacturacion").removeClass("menssageErrorTitulo");
	$("#divTituloDatosFacturacion").html(paginaPagodatosFacturacion);
	
	if (!$('#idFormDataUser').valid()) {
		$("#divTituloDatosPersonales").addClass("menssageErrorTitulo");
		$("#divTituloDatosPersonales").html("paginaPagodatosPersonalesAsterisco");	
		errorEncontrado=false;					
    }
    
    
    if (!$('#idFormDataDelivery').valid()) {
		$("#divTituloEntrega").addClass("menssageErrorTitulo");
		$("#divTituloEntrega").html(paginaPagoopcionesEntregaAsterisco);
		errorEncontrado=false;									
    }else{
    	ajaxShippingQuotations();
    }


    if (!$('#idFormDataFacturation').valid()) {
		$("#divTituloDatosFacturacion").addClass("menssageErrorTitulo");
		$("#divTituloDatosFacturacion").html(paginaPagodatosFacturacionAsterisco);
		errorEncontrado=false;						
    }
    
    if(!enviarPago)
    	return false;
        
    var responseFormUser = ajaxAddUser();    
    if(!responseFormUser)
    	return false;
    
}



function ajaxSendSale(pay){
	$("#errorMsgCorreo").empty();	
	var enviarPago = true;
	
	if ($('#idFormDataUser').length!=0) {
		$("#divTituloDatosPersonales").removeClass("menssageErrorTitulo");
		if (!$('#idFormDataUser').valid()) {
			$("#divTituloDatosPersonales").addClass("menssageErrorTitulo");
			$("#divTituloDatosPersonales").html("Datos personales *");
			$(window).scrollTop($('#idFormDataUser').offset().top);												
			$("#idButtonCollapseOne").click();
			enviarPago=false;								
	    }
	}
    
    
    if ($('#idFormDataDelivery').length!=0) {
		$("#divTituloEntrega").removeClass("menssageErrorTitulo");
	    if (!$('#idFormDataDelivery').valid()) {
			$("#divTituloEntrega").addClass("menssageErrorTitulo");
			$("#divTituloEntrega").html("Opciones de entrega *");
			$("#idButtonCollapseThree").click();			
			$(window).scrollTop($('#idButtonCollapseThree').offset().top);
			enviarPago=false;											
	    }
	}

	if ($('#idFormDataFacturation').length!=0) {
		$("#divTituloDatosFacturacion").removeClass("menssageErrorTitulo");
	    if (!$('#idFormDataFacturation').valid()) {
			$("#divTituloDatosFacturacion").addClass("menssageErrorTitulo");
			$("#divTituloDatosFacturacion").html("Datos Facturación (Opcional) *");
			$("#idButtonCollapseThree1").click();			
			$(window).scrollTop($('#idButtonCollapseThree1').offset().top);			
			enviarPago=false;									
	    }
	}
    
    $("#idFormaPago").val(pay);
    
    
    if(!enviarPago)
    	return false;
    else{
    	
    	var form = $("#idFormSaleCreateSale");
		var url = form.attr('action');	
		var data = $("#idFormDataFacturation").serialize() + '&' + $("#idFormDataDelivery").serialize() + '&' + $("#idFormDataUser").serialize() + '&' + $("#idFormDataPay").serialize();
    	
    	$.ajax({
			url : url,		
			type: 'POST',
			async: true,	
			data : data,
			beforeSend: function(){
				$(".loader-page").fadeIn();
			},    
			success : function(data) {
				$(".loader-page").fadeOut(10);
				if(data.exitoso){											
					var idFormaPago = $("#idFormaPago").val();
					if(idFormaPago==1){
						window.location.href = data.respuesta;
					}else{
						var a = document.createElement('a');
						var pdfAsDataUri = "data:application/pdf;base64," + data.respuesta;
						a.download = 'cotizacionJMI.pdf';
						a.type = 'application/pdf';
						a.href = pdfAsDataUri;
						a.click();
						
						if ($('#idFormDataUser').length!=0) {
							$("#idFormDataUser").css("pointer-events","none");							
						}

						if ($('#idFormDataDelivery').length!=0) {
							$("#idFormDataDelivery").css("pointer-events","none");							
						}

						if ($('#idFormDataFacturation').length!=0) {
							$("#idFormDataFacturation").css("pointer-events","none");							
						}

						if ($('#idFormDataPay').length!=0) {
							$("#idFormDataPay").css("pointer-events","none");							
						}
						
						$("#idRowPagos").remove();
						$("#idRowGraciasCompra").show();
					}
				}else{				
					$(window).scrollTop($('#errorModificarProducto').offset().top);
					$("#errorModificarProducto").empty();
					$("#errorModificarProducto").css("display","block");
					$("#errorModificarProducto").append('<span id="idCorreoElectronico-error" class="error" style="display: inline;">Error al enviar petición</span>');
				}
							
				
			},
			error : function(result) {
				$(".loader-page").fadeOut(10);
				$("#errorModificarProducto").empty();
				$("#errorModificarProducto").css("display","block");
				$("#errorModificarProducto").append('<span id="idCorreoElectronico-error" class="error" style="display: inline;">Error al enviar petición</span>');
				$(window).scrollTop($('#errorModificarProducto').offset().top);
			}
		});	
    }
}




function ajaxAddUser(){	
	var form = $("#idFormDataUser");
	var url = form.attr('action');	
	var data = $("#idFormDataUser").serialize();
		
	if (!$('#idFormDataUser').valid()) {
		return false;
    }	

	$.ajax({
		url : url,		
		type: 'POST',
		async: true,	
		data : data,
		beforeSend: function(){
			$(".loader-page").fadeIn();
		},    
		success : function(result) {
			$(".loader-page").fadeOut(10);
			if(result.exitoso)						
				return true;
			else{				
				$("#errorPagar").html(paginaPagoerrorPago);
				return false;
			}
		},
		error : function(result) {
			$(".loader-page").fadeOut(10);
			$("#errorPagar").html(paginaPagoerrorPago);
			return false;
		}
	});	
}

function addAddressUser(codigoPostal,pais,estado,delegacion,colonia,calle,noExterior,noInterior,referencia,estatus,id){

	$("#idDomiciliocodigoPostal").val("");
	$("#idRealDomiciliocodigoPostal").val("");
	$("#idDomicilioestado").val("");
	$("#idRealDomicilioestado").val("");
	$("#idDomicilionoExterior").val("");
	$("#idRealDomicilionoExterior").val("");
	$("#idDomiciliocalle").val("");
	$("#idRealDomiciliocalle").val("");
	$("#idDomiciliodelegacion").val("");
	$("#idRealDomiciliodelegacion").val("");
	$("#idDomiciliocolonia").val("");
	$("#idRealDomiciliocolonia").val("");
	$("#idDomicilionoInterior").val("");
	$("#idRealDomicilionoInterior").val("");
	$("#idDomicilioreferencia").val("");
	$("#idRealDomicilioreferencia").val("");

	if(codigoPostal!=null && codigoPostal!='null' && codigoPostal!='NULL' && codigoPostal!=''){ 
		$("#idDomiciliocodigoPostal").val(codigoPostal);
		$("#idRealDomiciliocodigoPostal").val(codigoPostal);
	}
	
	if(estado!=null && estado!='null' && estado!='NULL' && estado!=''){
		$("#idDomicilioestado").val(estado);
		$("#idRealDomicilioestado").val(estado);
	}
		
		
	if(noExterior!=null && noExterior!='null' && noExterior!='NULL' && noExterior!=''){
		$("#idDomicilionoExterior").val(noExterior);
		$("#idRealDomicilionoExterior").val(noExterior);
	}
	
	if(calle!=null && calle!='null' && calle!='NULL' && calle!=''){
		$("#idDomiciliocalle").val(calle);
		$("#idRealDomiciliocalle").val(calle);
	}
		
	if(delegacion!=null && delegacion!='null' && delegacion!='NULL' && delegacion!=''){
		$("#idDomiciliodelegacion").val(delegacion);
		$("#idRealDomiciliodelegacion").val(delegacion);
	}
	
	if(colonia!=null && colonia!='null' && colonia!='NULL' && colonia!=''){
		$("#idDomiciliocolonia").val(colonia);
		$("#idRealDomiciliocolonia").val(colonia);
	}
		
	if(noInterior!=null && noInterior!='null' && noInterior!='NULL' && noInterior!=''){
		$("#idDomicilionoInterior").val(noInterior);
		$("#idRealDomicilionoInterior").val(noInterior);
	}
		
	if(referencia!=null && referencia!='null' && referencia!='NULL' && referencia!=''){
		$("#idDomicilioreferencia").val(referencia);
		$("#idRealDomicilioreferencia").val(referencia);
	}
	
	if(id!=null && id!='null' && id!='NULL' && id!=''){ 
		$("#idDomicilio").val(id);
		$("#idRealDomicilio").val(id);
	}
	
	if(estatus!=null && estatus!='null' && estatus!='NULL' && estatus!=''){
		$("#idDomicilioEstatus").val(estatus);
	}
	
	
	
	ajaxGeneratePay();
	
}

function addAddressTax(codigoPostal,pais,estado,delegacion,colonia,calle,noExterior,noInterior,rfc,razonSocial,estatus,id,usoCFDI){

	$("#idRealDomicilioFiscalrazonSocial").val("");
	$("#idRealDomicilioFiscalcodigoPostal").val("");
	$("#idRealDomicilioFiscalestado").val("");
	$("#idRealDomicilioFiscalnoExterior").val("");
	$("#idRealDomicilioFiscalcalle").val("");
	$("#idRealDomicilioFiscalrfc").val("");
	$("#idRealDomicilioFiscaldelegacion").val("");
	$("#idRealDomicilioFiscalcolonia").val("");
	$("#idRealDomicilioFiscalnoInterior").val("");
	$("#idDomicilioFiscalrazonSocial").val("");
	$("#idDomicilioFiscalcodigoPostal").val("");
	$("#idDomicilioFiscalestado").val("");
	$("#idDomicilioFiscalnoExterior").val("");
	$("#idDomicilioFiscalcalle").val("");
	$("#idDomicilioFiscalrfc").val("");
	$("#idDomicilioFiscaldelegacion").val("");
	$("#idDomicilioFiscalcolonia").val("");
	$("#idRealDomicilioFiscalnoInterior").val("");
	$("#idDomicilioFiscalUsoCFDI").val("");

	
	if(usoCFDI!=null && usoCFDI!='null' && usoCFDI!='NULL' && usoCFDI!=''){ 
		$("#idRealDomicilioFiscalUsoCFDI").val(usoCFDI);
		$("#idDomicilioFiscalUsoCFDI").val(usoCFDI);
	}
	
	if(razonSocial!=null && razonSocial!='null' && razonSocial!='NULL' && razonSocial!=''){ 
		$("#idRealDomicilioFiscalrazonSocial").val(razonSocial);
		$("#idDomicilioFiscalrazonSocial").val(razonSocial);
	}
	
	if(codigoPostal!=null && codigoPostal!='null' && codigoPostal!='NULL' && codigoPostal!=''){ 
		$("#idRealDomicilioFiscalcodigoPostal").val(codigoPostal);
		$("#idDomicilioFiscalcodigoPostal").val(codigoPostal);
	}

	if(estado!=null && estado!='null' && estado!='NULL' && estado!=''){ 
		$("#idRealDomicilioFiscalestado").val(estado);
		$("#idDomicilioFiscalestado").val(estado);
	}

	if(noExterior!=null && noExterior!='null' && noExterior!='NULL' && noExterior!=''){ 
		$("#idRealDomicilioFiscalnoExterior").val(noExterior);
		$("#idDomicilioFiscalnoExterior").val(noExterior);
	}

	if(calle!=null && calle!='null' && calle!='NULL' && calle!=''){ 
		$("#idRealDomicilioFiscalcalle").val(calle);
		$("#idDomicilioFiscalcalle").val(calle);
	}

	if(rfc!=null && rfc!='null' && rfc!='NULL' && rfc!=''){ 
		$("#idRealDomicilioFiscalrfc").val(rfc);
		$("#idDomicilioFiscalrfc").val(rfc);
	}

	if(delegacion!=null && delegacion!='null' && delegacion!='NULL' && delegacion!=''){ 
		$("#idRealDomicilioFiscaldelegacion").val(delegacion);
		$("#idDomicilioFiscaldelegacion").val(delegacion);
	}

	if(colonia!=null && colonia!='null' && colonia!='NULL' && colonia!=''){ 
		$("#idRealDomicilioFiscalcolonia").val(colonia);
		$("#idDomicilioFiscalcolonia").val(colonia);
	}

	if(noInterior!=null && noInterior!='null' && noInterior!='NULL' && noInterior!=''){ 
		$("#idRealDomicilioFiscalnoInterior").val(noInterior);
		$("#idDomicilioFiscalnoInterior").val(noInterior);
	}

	if(id!=null && id!='null' && id!='NULL' && id!=''){ 
		$("#idDomicilioFiscal").val(id);
		$("#idRealDomicilioFiscal").val(id);		
	}

	if(estatus!=null && estatus!='null' && estatus!='NULL' && estatus!=''){
		$("#idDomicilioFiscalEstatus").val(estatus);
	}
	
	
	ajaxGeneratePay();
}


function validateChangeAddress(id){	
	
	var modificado = false;
	if ($('#idRealDomiciliocodigoPostal').length!=0) {
  		
  		if($("#idRealDomiciliocodigoPostal").val().trim()!=$("#idDomiciliocodigoPostal").val().trim())
  			modificado = true;
  		
  		if($("#idRealDomicilioestado").val().trim()!=$("#idDomicilioestado").val().trim())
  			modificado = true;
  		
  		if($("#idRealDomicilionoExterior").val().trim()!=$("#idDomicilionoExterior").val().trim())
  			modificado = true;
  		
  		if($("#idRealDomiciliocalle").val().trim()!=$("#idDomiciliocalle").val().trim())
  			modificado = true;
  		
  		if($("#idRealDomiciliodelegacion").val().trim()!=$("#idDomiciliodelegacion").val().trim())
  			modificado = true;
  		
  		if($("#idRealDomiciliocolonia").val().trim()!=$("#idDomiciliocolonia").val().trim())
  			modificado = true;
  		
  		if($("#idRealDomicilionoInterior").val().trim()!=$("#idDomicilionoInterior").val().trim())
  			modificado = true;
  		
  		if($("#idRealDomicilioreferencia").val().trim()!=$("#idDomicilioreferencia").val().trim())
  			modificado = true;
  		
  		if(modificado==true)
			$("#idDomicilio").val("");							
		else
			$("#idDomicilio").val($("#idRealDomicilio").val());		
			
	}
}


function validateChangeAddressTax(id){
	var modificado = false;
	if ($('#idRealDomicilioFiscalrazonSocial').length!=0) {
  		
  		if($("#idRealDomicilioFiscalrazonSocial").val().trim()!=$("#idDomicilioFiscalrazonSocial").val().trim())
  			modificado = true;
  		
  		if($("#idRealDomicilioFiscalcodigoPostal").val().trim()!=$("#idDomicilioFiscalcodigoPostal").val().trim())
  			modificado = true;
  		
  		if($("#idRealDomicilioFiscalestado").val().trim()!=$("#idDomicilioFiscalestado").val().trim())
  			modificado = true;
  		
  		if($("#idRealDomicilioFiscalnoExterior").val().trim()!=$("#idDomicilioFiscalnoExterior").val().trim())
  			modificado = true;
  		
  		if($("#idRealDomicilioFiscalcalle").val().trim()!=$("#idDomicilioFiscalcalle").val().trim())
  			modificado = true;
  		
  		if($("#idRealDomicilioFiscalrfc").val().trim()!=$("#idDomicilioFiscalrfc").val().trim())
  			modificado = true;
  		
  		if($("#idRealDomicilioFiscaldelegacion").val().trim()!=$("#idDomicilioFiscaldelegacion").val().trim())
  			modificado = true;
  		
  		if($("#idRealDomicilioFiscalcolonia").val().trim()!=$("#idDomicilioFiscalcolonia").val().trim())
  			modificado = true;
  		
  		if($("#idRealDomicilioFiscalnoInterior").val().trim()!=$("#idDomicilioFiscalnoInterior").val().trim())
  			modificado = true;
  			
  		if($("#idRealDomicilioFiscalUsoCFDI").val().trim()!=$("#idDomicilioFiscalUsoCFDI").val().trim())
  			modificado = true;
  		
  		
  		if(modificado==true)
			$("#idDomicilioFiscal").val("");		
		else
			$("#idDomicilioFiscal").val($("#idRealDomicilioFiscal").val());
			
	}

}

function showPay(pay){
	
	$("#idPagarPayPal").hide();
	$("#idPagarOxxo").hide();
	if(pay==1){
		$("#idPagarPayPal").show();
		$("#idPagarOxxo").hide();	
	}else if(pay==2){
		$("#idPagarPayPal").hide();
		$("#idPagarOxxo").show();
	}
	
}

function initJMainQuery(){

 "use strict";

    new WOW().init();  

    /*---background image---*/
	function dataBackgroundImage() {
		$('[data-bgimg]').each(function () {
			var bgImgUrl = $(this).data('bgimg');
			$(this).css({
				'background-image': 'url(' + bgImgUrl + ')', // + meaning concat
			});
		});
    }
    
    $(window).on('load', function () {
        dataBackgroundImage();
    });
    
    /*---stickey menu---*/
    $(window).on('scroll',function() {    
           var scroll = $(window).scrollTop();
           if (scroll < 100) {
            $(".sticky-header").removeClass("sticky");
           }else{
            $(".sticky-header").addClass("sticky");
           }
    });
    

    /*---slider activation---*/
    $('.slider_area').owlCarousel({
        animateOut: 'fadeOut',
        autoplay: true,
		loop: true,
        nav: false,
        autoplay: false,
        autoplayTimeout: 8000,
        items: 1,
        dots:true,
    });
    
    /*---product column5 activation---*/
    $('.product_column5').on('changed.owl.carousel initialized.owl.carousel', function (event) {
        $(event.target).find('.owl-item').removeClass('last').eq(event.item.index + event.page.size - 1).addClass('last')}).owlCarousel({
        autoplay: true,
		loop: true,
        nav: true,
        autoplay: false,
        autoplayTimeout: 8000,
        items: 5,
        margin:20,
        dots:false,
        navText: ['<i class="ion-ios-arrow-left"></i>','<i class="ion-ios-arrow-right"></i>'],
        responsiveClass:true,
		responsive:{
				0:{
				items:1,
			},
            576:{
				items:2,
			},
            768:{
				items:3,
			},
            992:{
				items:4,
			},
            1200:{
				items:5,
			},


		  }
    });
    
    /*---product column5 activation---*/
    $('.product_column5_not_loop').on('changed.owl.carousel initialized.owl.carousel', function (event) {
        $(event.target).find('.owl-item').removeClass('last').eq(event.item.index + event.page.size - 1).addClass('last')}).owlCarousel({
        autoplay: true,
		loop: false,
        nav: true,
        autoplay: false,
        autoplayTimeout: 8000,
        items: 5,
        margin:20,
        dots:false,
        navText: ['<i class="ion-ios-arrow-left"></i>','<i class="ion-ios-arrow-right"></i>'],
        responsiveClass:true,
		responsive:{
				0:{
				items:1,
			},
            576:{
				items:2,
			},
            768:{
				items:3,
			},
            992:{
				items:4,
			},
            1200:{
				items:5,
			},


		  }
    });
    
   
    /*---product column4 activation---*/
       $('.product_column4_not_loop').on('changed.owl.carousel initialized.owl.carousel', function (event) {
        $(event.target).find('.owl-item').removeClass('last').eq(event.item.index + event.page.size - 1).addClass('last')}).owlCarousel({
        autoplay: true,
		loop: false,
        nav: true,
        autoplay: false,
        autoplayTimeout: 8000,
        items: 5,
        margin:20,
        dots:false,
        navText: ['<i class="ion-ios-arrow-left"></i>','<i class="ion-ios-arrow-right"></i>'],
        responsiveClass:true,
		responsive:{
				0:{
				items:1,
			},
            576:{
				items:2,
			},
            768:{
				items:3,
			},
            992:{
				items:3,
			},
            1200:{
				items:4,
			},


		  }
    });
   
   
    /*---product column4 activation---*/
       $('.product_column4').on('changed.owl.carousel initialized.owl.carousel', function (event) {
        $(event.target).find('.owl-item').removeClass('last').eq(event.item.index + event.page.size - 1).addClass('last')}).owlCarousel({
        autoplay: true,
		loop: true,
        nav: true,
        autoplay: false,
        autoplayTimeout: 8000,
        items: 5,
        margin:20,
        dots:false,
        navText: ['<i class="ion-ios-arrow-left"></i>','<i class="ion-ios-arrow-right"></i>'],
        responsiveClass:true,
		responsive:{
				0:{
				items:1,
			},
            576:{
				items:2,
			},
            768:{
				items:3,
			},
            992:{
				items:3,
			},
            1200:{
				items:4,
			},


		  }
    });
    
     /*---product column3 activation---*/
     $('.product_column3_not_loop').on('changed.owl.carousel initialized.owl.carousel', function (event) {
        $(event.target).find('.owl-item').removeClass('last').eq(event.item.index + event.page.size - 1).addClass('last')}).owlCarousel({
        autoplay: true,
		loop: false,
        nav: true,
        autoplay: false,
        autoplayTimeout: 8000,
        items: 3,
        margin:20,
        dots:false,
        navText: ['<i class="ion-ios-arrow-left"></i>','<i class="ion-ios-arrow-right"></i>'],
        responsiveClass:true,
		responsive:{
				0:{
				items:1,
			},
            576:{
				items:2,
			},
            768:{
				items:3,
			},
            992:{
				items:2,
			},
            1200:{
				items:3,
			},


		  }
    });
   
     /*---product column3 activation---*/
     $('.product_column3').on('changed.owl.carousel initialized.owl.carousel', function (event) {
        $(event.target).find('.owl-item').removeClass('last').eq(event.item.index + event.page.size - 1).addClass('last')}).owlCarousel({
        autoplay: true,
		loop: true,
        nav: true,
        autoplay: false,
        autoplayTimeout: 8000,
        items: 3,
        margin:20,
        dots:false,
        navText: ['<i class="ion-ios-arrow-left"></i>','<i class="ion-ios-arrow-right"></i>'],
        responsiveClass:true,
		responsive:{
				0:{
				items:1,
			},
            576:{
				items:2,
			},
            768:{
				items:3,
			},
            992:{
				items:2,
			},
            1200:{
				items:3,
			},


		  }
    });
    
    
    /*---productnav column3 activation---*/
    $('.productnav_column3').on('changed.owl.carousel initialized.owl.carousel', function (event) {
        $(event.target).find('.owl-item').removeClass('last').eq(event.item.index + event.page.size - 1).addClass('last')}).owlCarousel({
        autoplay: true,
		loop: true,
        nav: false,
        autoplay: false,
        autoplayTimeout: 8000,
        items: 3,
        margin:10,
        dots:false,
    });
    
    $('.productnav_column3 a').on('click',function(e){
      e.preventDefault();

      var $href = $(this).attr('href');

      $('.productnav_column3 a').removeClass('active');
      $(this).addClass('active');

      $('.product_thumb .tab-pane').removeClass('active show');
      $('.product_thumb '+ $href ).addClass('active show');

    })
    
    
     /*---productnavbottom column3 activation---*/
    $('.productnavbottom_column3').on('changed.owl.carousel initialized.owl.carousel', function (event) {
        $(event.target).find('.owl-item').removeClass('last').eq(event.item.index + event.page.size - 1).addClass('last')}).owlCarousel({
        autoplay: true,
		loop: true,
        nav: false,
        autoplay: false,
        autoplayTimeout: 8000,
        items: 3,
        margin:10,
        dots:false,
    });
    
    $('.productnavbottom_column3 a').on('click',function(e){
      e.preventDefault();

      var $href = $(this).attr('href');

      $('.productnavbottom_column3 a').removeClass('active');
      $(this).addClass('active');

      $('.product_thumbtav2 .tab-pane').removeClass('active show');
      $('.product_thumbtav2 '+ $href ).addClass('active show');

    })
    
    
    /*---featured column3 activation---*/
    $('.featured_column4').on('changed.owl.carousel initialized.owl.carousel', function (event) {
        $(event.target).find('.owl-item').removeClass('last').eq(event.item.index + event.page.size - 1).addClass('last')}).owlCarousel({
        autoplay: true,
		loop: true,
        nav: true,
        autoplay: false,
        autoplayTimeout: 8000,
        margin:20,
        items: 4,
        dots:false,
         navText: ['<i class="ion-ios-arrow-left"></i>','<i class="ion-ios-arrow-right"></i>'],
        responsiveClass:true,
		responsive:{
				0:{
				items:1,
			},
            576:{
				items:2,
			},
            768:{
				items:3,
			},
            992:{
				items:3,
			},
            1200:{
				items:4,
			},

		  }
    });
    
     /*---featured column3 activation---*/
    $('.small_product_column3').on('changed.owl.carousel initialized.owl.carousel', function (event) {
        $(event.target).find('.owl-item').removeClass('last').eq(event.item.index + event.page.size - 1).addClass('last')}).owlCarousel({
        autoplay: true,
		loop: true,
        nav: true,
        autoplay: false,
        autoplayTimeout: 8000,
        items: 3,
        margin:20,
        dots:false,
        navText: ['<i class="ion-ios-arrow-left"></i>','<i class="ion-ios-arrow-right"></i>'],
        responsiveClass:true,
		responsive:{
				0:{
				items:1,
			},
            576:{
				items:2,
			},
            768:{
				items:2,
			},
            992:{
				items:3,
			},
            1200:{
				items:3,
			},


		  }
    });
    
   
    
    /*---product column4 activation---*/
    $('.blog_column2').owlCarousel({
        autoplay: true,
		loop: true,
        nav: true,
        autoplay: false,
        autoplayTimeout: 8000,
        items: 2,
        margin:20,
        dots:false,
       navText: ['<i class="ion-ios-arrow-left"></i>','<i class="ion-ios-arrow-right"></i>'],
        responsiveClass:true,
		responsive:{
				0:{
				items:1,
			},
            576:{
				items:2,
			},
            768:{
				items:1,
			},
             992:{
				items:2,
			}, 
		  }
    });
    
    /*---blog thumb activation---*/
    $('.blog_thumb_active').owlCarousel({
        autoplay: true,
		loop: true,
        nav: true,
        autoplay: false,
        autoplayTimeout: 8000,
        items: 1,
        margin:20,
        navText: ['<i class="ion-ios-arrow-thin-left"></i>','<i class="ion-ios-arrow-thin-right"></i>'],
    });
    
    /*---brand container activation---*/
     $('.brand_container').on('changed.owl.carousel initialized.owl.carousel', function (event) {
        $(event.target).find('.owl-item').removeClass('last').eq(event.item.index + event.page.size - 1).addClass('last')}).owlCarousel({
        autoplay: true,
		loop: true,
        nav: true,
        autoplay: false,
        autoplayTimeout: 8000,
        items: 5,
        dots:false,
         navText: ['<i class="ion-ios-arrow-left"></i>','<i class="ion-ios-arrow-right"></i>'],
        responsiveClass:true,
		responsive:{
				0:{
				items:1,
			},
            400:{
				items:2,
			},
            576:{
				items:3,
			},
            768:{
				items:4,
			},
            992:{
				items:5,
			},
            1200:{
				items:5,
			},

		  }
    });
    

    /*---single product activation---*/
    $('.single-product-active').owlCarousel({
        autoplay: true,
		loop: true,
        nav: true,
        autoplay: false,
        autoplayTimeout: 8000,
        items: 4,
        margin:15,
        dots:false,
        navText: ['<i class="fa fa-angle-left"></i>','<i class="fa fa-angle-right"></i>'],
        responsiveClass:true,
		responsive:{
				0:{
				items:1,
			},
            320:{
				items:2,
			},
            420:{
				items:3,
			},
            576:{
				items:4,
			},
            992:{
				items:3,
			},
            1200:{
				items:4,
			},


		  }
    });
 
    /*---testimonial active activation---*/
    $('.testimonial_active').owlCarousel({
        autoplay: true,
		loop: true,
        nav: false,
        autoplay: false,
        autoplayTimeout: 8000,
        items: 1,
        dots:true,
    })
 
    /*---product navactive activation---*/
    $('.product_navactive').owlCarousel({
        autoplay: true,
		loop: true,
        nav: true,
        autoplay: false,
        autoplayTimeout: 8000,
        items: 4,
        dots:false,
        navText: ['<i class="fa fa-angle-left"></i>','<i class="fa fa-angle-right"></i>'],
        responsiveClass:true,
		responsive:{
				0:{
				items:1,
			},
            250:{
				items:2,
			},
            480:{
				items:3,
			},
            768:{
				items:4,
			},
		  
        }
    });

    $('.modal').on('shown.bs.modal', function (e) {
        $('.product_navactive').resize();
    })

    $('.product_navactive a').on('click',function(e){
      e.preventDefault();

      var $href = $(this).attr('href');

      $('.product_navactive a').removeClass('active');
      $(this).addClass('active');

      $('.product-details-large .tab-pane').removeClass('active show');
      $('.product-details-large '+ $href ).addClass('active show');

    })
       
    /*--- Magnific Popup Video---*/
    $('.video_popup').magnificPopup({
        type: 'iframe',
        removalDelay: 300,
        mainClass: 'mfp-fade'
    });
    
    /*--- new vidio play Video---*/
    $('.new_vidio_play').magnificPopup({
        type: 'iframe',
        removalDelay: 300,
        mainClass: 'mfp-fade'
    });
    
    /*--- Magnific Popup Video---*/
    $('.port_popup').magnificPopup({
        type: 'image',
        gallery: {
            enabled: true
        }
    });
 
    /*--- niceSelect---*/
     $('.select_option').niceSelect();
    
    /*---  Accordion---*/
    $(".faequently-accordion").collapse({
        accordion:true,
        open: function() {
        this.slideDown(300);
      },
      close: function() {
        this.slideUp(300);
      }		
    });	  

   

    /*---  ScrollUp Active ---*/
   
    
     /*---  ScrollUp Active ---*/
    $.scrollUp({
        scrollText: '<i class="fa fa-angle-double-up"></i>',
        easingType: 'linear',
        scrollSpeed: 900,
        animation: 'fade'
    });           
    
    /*---countdown activation---*/
		
	 $('[data-countdown]').each(function() {
		var $this = $(this), finalDate = $(this).data('countdown');
		$this.countdown(finalDate, function(event) {
		$this.html(event.strftime('<div class="countdown_area"><div class="single_countdown"><div class="countdown_number">%D</div><div class="countdown_title">Days</div></div><div class="single_countdown"><div class="countdown_number">%H</div><div class="countdown_title">Hours</div></div><div class="single_countdown"><div class="countdown_number">%M</div><div class="countdown_title">Mins</div></div><div class="single_countdown"><div class="countdown_number">%S</div><div class="countdown_title">Secs</div></div></div>'));     
               
       });
	});	
    
    /*---slider-range here---*/
    $( "#slider-range" ).slider({
        range: true,
        min: 0,
        max: 500,
        values: [ 0, 500 ],
        slide: function( event, ui ) {
        $( "#amount" ).val( "€" + ui.values[ 0 ] + " - €" + ui.values[ 1 ] );
       }
    });
    $( "#amount" ).val( "€" + $( "#slider-range" ).slider( "values", 0 ) +
       " - €" + $( "#slider-range" ).slider( "values", 1 ) );
    
    /*---niceSelect---*/
     $('.niceselect_option').niceSelect();
    
    /*---elevateZoom---*/
    $("#idImagenProducto").elevateZoom({
        gallery:'gallery_01', 
        responsive : true,
        cursor: 'crosshair',
        zoomType : 'inner'
    
    });  
    
    /*---portfolio Isotope activation---*/
      $('.portfolio_gallery').imagesLoaded( function() {

        var $grid = $('.portfolio_gallery').isotope({
           itemSelector: '.gird_item',
            percentPosition: true,
            masonry: {
                columnWidth: '.gird_item'
            }
        });

          /*---ilter items on button click---*/
        $('.portfolio_button').on( 'click', 'button', function() {
           var filterValue = $(this).attr('data-filter');
           $grid.isotope({ filter: filterValue });
            
           $(this).siblings('.active').removeClass('active');
           $(this).addClass('active');
        });
       
    });
    
    /*---slide toggle activation---*/
   $('.mini_cart_wrapper > a').on('click', function(event){
        if($(window).width() < 991){
            $('.mini_cart').slideToggle('medium');
        }
    });
    
    /*categories slideToggle*/
    $(".categories_title").on("click", function() {
        $(this).toggleClass('active');
        $('.categories_menu_toggle').slideToggle('medium');
    }); 

      /*------addClass/removeClass categories-------*/
   $("#cat_toggle.has-sub > a").on("click", function() {
            $(this).removeAttr('href');
            $(this).toggleClass('open').next('.categorie_sub').toggleClass('open');
            $(this).parents().siblings().find('#cat_toggle.has-sub > a').removeClass('open');
    });
    
    
    /*---MailChimp---*/
    $('#mc-form').ajaxChimp({
        language: 'en',
        callback: mailChimpResponse,
        // ADD YOUR MAILCHIMP URL BELOW HERE!
        url: 'http://devitems.us11.list-manage.com/subscribe/post?u=6bbb9b6f5827bd842d9640c82&amp;id=05d85f18ef'

    });
    function mailChimpResponse(resp) {

        if (resp.result === 'success') {
            $('.mailchimp-success').addClass('active')
            $('.mailchimp-success').html('' + resp.msg).fadeIn(900);
            $('.mailchimp-error').fadeOut(400);

        } else if(resp.result === 'error') {
            $('.mailchimp-error').html('' + resp.msg).fadeIn(900);
        }  
    }
    
    
    /* ---------------------
	 Category menu
	--------------------- */
    
   
    function categorySubMenuToggle(){
        $('.categories_menu_toggle li.menu_item_children > a').on('click', function(){
        if($(window).width() < 991){
            $(this).removeAttr('href');
            var element = $(this).parent('li');
            if (element.hasClass('open')) {
                element.removeClass('open');
                element.find('li').removeClass('open');
                element.find('ul').slideUp();
            }
            else {
                element.addClass('open');
                element.children('ul').slideDown();
                element.siblings('li').children('ul').slideUp();
                element.siblings('li').removeClass('open');
                element.siblings('li').find('li').removeClass('open');
                element.siblings('li').find('ul').slideUp();
            }
        }
        });
        $('.categories_menu_toggle li.menu_item_children > a').append('<span class="expand"></span>');
    }
    categorySubMenuToggle();


    /*---shop grid activation---*/
    $('.shop_toolbar_btn > button').on('click', function (e) {
        
		e.preventDefault();
        
        $('.shop_toolbar_btn > button').removeClass('active');
		$(this).addClass('active');
        
		var parentsDiv = $('.shop_wrapper');
		var viewMode = $(this).data('role');
        
        
		parentsDiv.removeClass('grid_3 grid_4 grid_5 grid_list').addClass(viewMode);

		if(viewMode == 'grid_3'){
			parentsDiv.children().addClass('col-lg-4 col-md-4 col-sm-6').removeClass('col-lg-3 col-cust-5 col-12');
            
		}

		if(viewMode == 'grid_4'){
			parentsDiv.children().addClass('col-lg-3 col-md-4 col-sm-6').removeClass('col-lg-4 col-cust-5 col-12');
		}
        
        if(viewMode == 'grid_list'){
			parentsDiv.children().addClass('col-12').removeClass('col-lg-3 col-lg-4 col-md-4 col-sm-6 col-cust-5');
		}
            
	});
  
    
    
   /*---Newsletter Popup activation---*/
    /*
       setTimeout(function() {
            if($.cookie('shownewsletter')==1) $('.newletter-popup').hide();
            $('#subscribe_pemail').keypress(function(e) {
                if(e.which == 13) {
                    e.preventDefault();
                    email_subscribepopup();
                }
                var name= $(this).val();
                  $('#subscribe_pname').val(name);
            });
            $('#subscribe_pemail').change(function() {
             var name= $(this).val();
                      $('#subscribe_pname').val(name);
            });
            //transition effect
            if($.cookie("shownewsletter") != 1){
                $('.newletter-popup').bPopup();
            }
            $('#newsletter_popup_dont_show_again').on('change', function(){
                if($.cookie("shownewsletter") != 1){   
                    $.cookie("shownewsletter",'1')
                }else{
                    $.cookie("shownewsletter",'0')
                }
            }); 
        }, 2500);
		*/
    
    /*---canvas menu activation---*/
    
   /*---canvas menu activation---*/
    $('.canvas_open').on('click', function(){
        $('.offcanvas_menu_wrapper,.off_canvars_overlay').addClass('active')
    });
    
    $('.canvas_close,.off_canvars_overlay').on('click', function(){
        $('.offcanvas_menu_wrapper,.off_canvars_overlay').removeClass('active')
    });
    

    
    /*---Off Canvas Menu---*/
    var $offcanvasNav = $('.offcanvas_main_menu'),
        $offcanvasNavSubMenu = $offcanvasNav.find('.sub-menu');
    $offcanvasNavSubMenu.parent().prepend('<span class="menu-expand"><i class="fa fa-angle-down"></i></span>');
    
    $offcanvasNavSubMenu.slideUp();
    
    $offcanvasNav.on('click', 'li a, li .menu-expand', function(e) {
        var $this = $(this);
        if ( ($this.parent().attr('class').match(/\b(menu-item-has-children|has-children|has-sub-menu)\b/)) && ($this.attr('href') === '#' || $this.hasClass('menu-expand')) ) {
            e.preventDefault();
            if ($this.siblings('ul:visible').length){
                $this.siblings('ul').slideUp('slow');
            }else {
                $this.closest('li').siblings('li').find('ul:visible').slideUp('slow');
                $this.siblings('ul').slideDown('slow');
            }
        }
        if( $this.is('a') || $this.is('span') || $this.attr('clas').match(/\b(menu-expand)\b/) ){
        	$this.parent().toggleClass('menu-open');
        }else if( $this.is('li') && $this.attr('class').match(/\b('menu-item-has-children')\b/) ){
        	$this.toggleClass('menu-open');
        }
    });
    
    
    /*js ripples activation*/
    $('.js-ripples').ripples({
		resolution: 512,
		dropRadius: 20,
		perturbance: 0.04
	});
    

}



