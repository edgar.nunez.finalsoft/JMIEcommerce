function removeProduct(product,remove,amount){

	var form = $("#idFormRmoveProduct");
	var url = form.attr('action');					 
	var data = "product="+product+"&remove="+remove+"&amount="+amount;
	
	$.ajax({
		url : url,		
		type: 'GET',
		async: true,	
		data : data,
		beforeSend: function(){
			$(".loader-page").fadeIn();
		},    
		success : function(data) {											
			if(data.indexOf("error_section")>=0){	
				$("#errorAgregarProducto").empty();
				$("#errorAgregarProducto").css("display","block");
				$("#errorAgregarProducto").append('<span id="idCorreoElectronico-error" class="error" style="display: inline;">Error al enviar petición</span>');				
			}else{
				$("#idRespuestaControlador").empty();
				$("#idRespuestaControlador").html(data);
			
				var html = $("#idRespuestaControlador").find(".offcanvas_menu").html();
				$(".offcanvas_menu").empty();
				$(".offcanvas_menu").html(html);
								
				html = $("#idRespuestaControlador").find(".header_area").html();
				$(".header_area").empty();
				$(".header_area").html(html);
				
				$(".loader-page").fadeOut(10);				
			}			
		},
		error : function(result) {			
			$(".loader-page").fadeOut(10);
			$("#errorAgregarProducto").empty();
			$("#errorAgregarProducto").css("display","block");
			$("#errorAgregarProducto").append('<span id="idCorreoElectronico-error" class="error" style="display: inline;">Error al enviar petición</span>');
		}
	});	
}