package com.mx.jmi.ecommerceJMI.services;

import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.mx.jmi.ecommerceJMI.beans.ClienteDTO;
import com.mx.jmi.ecommerceJMI.beans.DireccionDTO;
import com.mx.jmi.ecommerceJMI.controllers.MainController;
import com.mx.jmi.ecommerceJMI.entities.Categoria;
import com.mx.jmi.ecommerceJMI.entities.Cliente;
import com.mx.jmi.ecommerceJMI.entities.DatoFiscal;
import com.mx.jmi.ecommerceJMI.entities.Domicilio;
import com.mx.jmi.ecommerceJMI.utils.ResponseService;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@Service
public class UserServiceClient {

	private UserService service;	

	Logger logger = LoggerFactory.getLogger(UserServiceClient.class);

	ResponseService response = new ResponseService();
	
	public UserServiceClient(@Value("${server.core.JMI}") String serviceURL) {
		super();		
		OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
		Retrofit retrofit = new Retrofit.Builder().baseUrl(serviceURL).addConverterFactory(GsonConverterFactory.create()).client(httpClient.build()).build();
		service = retrofit.create(UserService.class);
	}


	public ResponseService sendCodeLogin(String email) throws Exception {
		logger.info("/sendCodeLogin?email=" + email);

		try {	
			
			Call<ResponseService> responseService = service.sendCodeLogin(email);
			Response<ResponseService> responseRRS = responseService.execute();
			response = responseRRS.body();			
			
		} catch (Exception ex) {
			throw new Exception(ex.getMessage(), ex.getCause());			
		}
		
		logger.info("/sendCodeLogin response:" + response.toString());
		return response;
	}
	
	
	public ResponseService validateCode(String email, String code) {
		logger.info("/validateCode?email=" +email+"&code="+code);

		try {			
			
			Call<ResponseService> responseService = service.validateCode(email, code);
			Response<ResponseService> responseRRS = responseService.execute();
			response = responseRRS.body();						
			
		} catch (Exception ex) {
			new Exception(ex.getMessage(), ex.getCause());			
		}
		
		logger.info("/validateCode response:" + response.toString());
		return response;
	}
	
	
	public ResponseService getClientByEmail(String email) throws Exception {
		logger.info("/getClientByEmail?email:" + email);
		ResponseService respuesta = new ResponseService();

		try {
			Call<ResponseService> responseService = service.getClientByEmail(email);
			Response<ResponseService> response = responseService.execute();
			respuesta = response.body();				

		} catch (Exception ex) {
			throw new Exception(ex.getMessage(), ex.getCause());			
		}
		
		logger.info("/getClientByEmail response:" + respuesta.toString());
		return respuesta;		
	}
	

	public ResponseService getClientById(int pkClient) throws Exception {
		logger.info("/getClientById?pkClient:" + pkClient);
		ResponseService respuesta = new ResponseService();
		
		try {
			Call<ResponseService> responseService = service.getClientById(pkClient);
			Response<ResponseService> response = responseService.execute();
			respuesta = response.body();				
			
		} catch (Exception ex) {
			throw new Exception(ex.getMessage(), ex.getCause());			
		}
		
		logger.info("/getClientById response:" + respuesta.toString());
		return respuesta;		
	}
	
	
	public ResponseService saveAddress(DireccionDTO address) throws Exception {
		logger.info("/saveAddress?address:" + address);
		ResponseService respuesta = new ResponseService();

		try {
			Call<ResponseService> responseService = service.saveAddress(address);
			Response<ResponseService> response = responseService.execute();
			respuesta = response.body();				

		} catch (Exception ex) {
			throw new Exception(ex.getMessage(), ex.getCause());			
		}
		
		logger.info("/saveAddress response:" + respuesta.toString());
		return respuesta;		
	}
	
	
	public ResponseService updateAddress(DireccionDTO address) throws Exception {
		logger.info("/updateAddress?address:" + address);
		ResponseService respuesta = new ResponseService();

		try {
			Call<ResponseService> responseService = service.updateAddress(address);
			Response<ResponseService> response = responseService.execute();
			respuesta = response.body();				

		} catch (Exception ex) {
			throw new Exception(ex.getMessage(), ex.getCause());			
		}
		
		logger.info("/updateAddress response:" + respuesta.toString());
		return respuesta;		
	}
	
	
	public ResponseService saveClient(ClienteDTO client) throws Exception {
		logger.info("/saveClient?address:" + client);
		ResponseService respuesta = new ResponseService();

		try {
			Call<ResponseService> responseService = service.saveClient(client);
			Response<ResponseService> response = responseService.execute();
			respuesta = response.body();				

		} catch (Exception ex) {
			throw new Exception(ex.getMessage(), ex.getCause());			
		}
		
		logger.info("/saveClient response:" + respuesta.toString());
		return respuesta;		
	}
	
	
	public ResponseService updateClient(ClienteDTO client) throws Exception {
		logger.info("/updateClient?address:" + client);
		ResponseService respuesta = new ResponseService();

		try {
			Call<ResponseService> responseService = service.updateClient(client);
			Response<ResponseService> response = responseService.execute();
			respuesta = response.body();				

		} catch (Exception ex) {
			throw new Exception(ex.getMessage(), ex.getCause());			
		}
		
		logger.info("/updateClient response:" + respuesta.toString());
		return respuesta;		
	}

}
