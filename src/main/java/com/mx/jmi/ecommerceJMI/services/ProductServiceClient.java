package com.mx.jmi.ecommerceJMI.services;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.mx.jmi.ecommerceJMI.controllers.MainController;
import com.mx.jmi.ecommerceJMI.entities.Categoria;
import com.mx.jmi.ecommerceJMI.entities.CotizacionEnvio;
import com.mx.jmi.ecommerceJMI.entities.Producto;
import com.mx.jmi.ecommerceJMI.entities.Venta;
import com.mx.jmi.ecommerceJMI.utils.ResponseService;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@Service
public class ProductServiceClient {

	private ProductService service;	

	Logger logger = LoggerFactory.getLogger(ProductService.class);

	ResponseService response = new ResponseService();
	
	public ProductServiceClient(@Value("${server.core.JMI}") String serviceURL) {
		super();		
		OkHttpClient.Builder httpClient = new OkHttpClient.Builder().connectTimeout(60,TimeUnit.SECONDS).writeTimeout(60,TimeUnit.SECONDS).readTimeout(60,TimeUnit.SECONDS);
		Retrofit retrofit = new Retrofit.Builder().baseUrl(serviceURL).addConverterFactory(GsonConverterFactory.create()).client(httpClient.build()).build(); 
		service = retrofit.create(ProductService.class);
	}


	public ResponseService getSubcategorias(Integer pkCategoria) throws Exception {
		logger.info("/getSubcategorias?pkCategoria:" + pkCategoria);
		ResponseService respuesta = new ResponseService();

		try {
			Call<ResponseService> responseService = service.getSubcategoriasPorCategoria(pkCategoria);
			Response<ResponseService> response = responseService.execute();
			respuesta = response.body();				

		} catch (Exception ex) {
			throw new Exception(ex.getMessage(), ex.getCause());			
		}
		
		logger.info("/getSubcategorias response:" + respuesta.toString());
		return respuesta;
	}
	
	
	public ResponseService getCategoria() throws Exception {
		logger.info("/getCategoria");
		ResponseService respuesta = new ResponseService();

		try {
			Call<ResponseService> responseService = service.getCategorias(1);
			Response<ResponseService> response = responseService.execute();
			
			if(response.code()==200)
				respuesta = response.body();				

		} catch (Exception ex) {
			throw new Exception(ex.getMessage(), ex.getCause());			
		}
		
		logger.info("/getCategorias response:" + respuesta.toString());
		return respuesta;
	}
	
	
	public ResponseService getSubcategoriasPorCategoria(Integer pkSubcategoria) throws Exception {
		logger.info("/getSubcategoriasPorCategoria?pkSubcategoria="+pkSubcategoria);
		ResponseService respuesta = new ResponseService();

		try {
			Call<ResponseService> responseService = service.getProductoBySubCategory(pkSubcategoria);
			Response<ResponseService> response = responseService.execute();
			
			if(response.code()==200)
				respuesta = response.body();				

		} catch (Exception ex) {
			throw new Exception(ex.getMessage(), ex.getCause());			
		}
		
		logger.info("/getSubcategoriasPorCategoria response:" + respuesta.toString());
		return respuesta;
	}

	
	public ResponseService getProductBySubCategory(Integer pkSubcategoria) throws Exception {
		logger.info("/getProductoBySubCategory?pkSubcategoria="+pkSubcategoria);
		ResponseService respuesta = new ResponseService();

		try {
			Call<ResponseService> responseService = service.getProductoBySubCategory(pkSubcategoria);
			Response<ResponseService> response = responseService.execute();
			
			if(response.code()==200)
				respuesta = response.body();				

		} catch (Exception ex) {
			throw new Exception(ex.getMessage(), ex.getCause());			
		}
		
		logger.info("/getProductoBySubCategory response:" + respuesta.toString());
		return respuesta;
	}
	
	
	public ResponseService getProductByCategory(Integer pkCategoria) throws Exception {
		logger.info("/getProductoByCategory?pkCategoria="+pkCategoria);
		ResponseService respuesta = new ResponseService();

		try {
			Call<ResponseService> responseService = service.getProductoByCategory(pkCategoria);
			Response<ResponseService> response = responseService.execute();
			
			if(response.code()==200)
				respuesta = response.body();				

		} catch (Exception ex) {
			throw new Exception(ex.getMessage(), ex.getCause());			
		}
		
		logger.info("/getProductoByCategory response:" + respuesta.toString());
		return respuesta;
	}
	
	
	public ResponseService getProductByName(String nombre) throws Exception {
		logger.info("/getProductByName?nombre="+nombre);
		ResponseService respuesta = new ResponseService();

		try {
			Call<ResponseService> responseService = service.getProductByName(nombre);
			Response<ResponseService> response = responseService.execute();
			
			if(response.code()==200)
				respuesta = response.body();				

		} catch (Exception ex) {
			throw new Exception(ex.getMessage(), ex.getCause());			
		}
		
		logger.info("/getProductByName response:" + respuesta.toString());
		return respuesta;
	}
	
	
	public ResponseService getSubcategoryById(Integer pkSubcategoria) throws Exception {
		logger.info("/getSubcategoryById/pkCategoria="+pkSubcategoria);
		ResponseService respuesta = new ResponseService();

		try {
			Call<ResponseService> responseService = service.getSubcategoryById(pkSubcategoria);
			Response<ResponseService> response = responseService.execute();
			
			if(response.code()==200)
				respuesta = response.body();				

		} catch (Exception ex) {
			throw new Exception(ex.getMessage(), ex.getCause());			
		}
		
		logger.info("/getSubcategoryById response:" + respuesta.toString());
		return respuesta;
	}
	
	
	public ResponseService getCategoryById(Integer pkCategoria) throws Exception {
		logger.info("/getCategoryById/pkCategoria="+pkCategoria);
		ResponseService respuesta = new ResponseService();

		try {
			Call<ResponseService> responseService = service.getCategoryById(pkCategoria);
			Response<ResponseService> response = responseService.execute();
			
			if(response.code()==200)
				respuesta = response.body();				

		} catch (Exception ex) {
			throw new Exception(ex.getMessage(), ex.getCause());			
		}
		
		logger.info("/getCategoryById response:" + respuesta.toString());
		return respuesta;
	}
	
	
	public ResponseService getProductById(Integer pkProducto) throws Exception {
		logger.info("/getProductById/pkProducto="+pkProducto);
		ResponseService respuesta = new ResponseService();

		try {
			Call<ResponseService> responseService = service.getProductById(pkProducto);
			Response<ResponseService> response = responseService.execute();
			
			if(response.code()==200)
				respuesta = response.body();				

		} catch (Exception ex) {
			throw new Exception(ex.getMessage(), ex.getCause());			
		}
		
		logger.info("/getProductById response:" + respuesta.toString());
		return respuesta;
	}
	
	
	public ResponseService getCategoryFeatureByProduct(Integer pkProducto) throws Exception {
		logger.info("/getCategoryFeatureByProduct/pkProducto="+pkProducto);
		ResponseService respuesta = new ResponseService();

		try {
			Call<ResponseService> responseService = service.getCategoryFeatureByProduct(pkProducto);
			Response<ResponseService> response = responseService.execute();
			
			if(response.code()==200)
				respuesta = response.body();				

		} catch (Exception ex) {
			throw new Exception(ex.getMessage(), ex.getCause());			
		}
		
		logger.info("/getCategoryFeatureByProduct response:" + respuesta.toString());
		return respuesta;
	}
	
	
	
	public ResponseService getFeaturesById(Integer pkProducto, Integer pkCaracteristica) throws Exception {
		logger.info("/getFeaturesById=pkProducto="+pkProducto+"&pkCaracteristica="+pkCaracteristica);
		ResponseService respuesta = new ResponseService();

		try {
			Call<ResponseService> responseService = service.getFeaturesById(pkProducto,pkCaracteristica);
			Response<ResponseService> response = responseService.execute();
			
			if(response.code()==200)
				respuesta = response.body();				

		} catch (Exception ex) {
			throw new Exception(ex.getMessage(), ex.getCause());			
		}
		
		logger.info("/getFeaturesById response:" + respuesta.toString());
		return respuesta;
	}
	
	
	public ResponseService getProductSuggested() throws Exception {
		logger.info("/getProductSuggested");
		ResponseService respuesta = new ResponseService();

		try {
			Call<ResponseService> responseService = service.getProductSuggested();
			Response<ResponseService> response = responseService.execute();
			
			if(response.code()==200)
				respuesta = response.body();				

		} catch (Exception ex) {
			throw new Exception(ex.getMessage(), ex.getCause());			
		}
		
		logger.info("/getProductSuggested response:" + respuesta.toString());
		return respuesta;
	}
	
	
	public ResponseService getImageFeatures(Integer pkProducto, String pkCaracteristicas) throws Exception {
		logger.info("/getImageFeatures?pkProducto="+pkProducto+"&pkCaracteristicas="+pkCaracteristicas);
		ResponseService respuesta = new ResponseService();

		try {
			Call<ResponseService> responseService = service.getImageFeatures(pkProducto,pkCaracteristicas);
			Response<ResponseService> response = responseService.execute();
			
			if(response.code()==200)
				respuesta = response.body();				

		} catch (Exception ex) {
			throw new Exception(ex.getMessage(), ex.getCause());			
		}
		
		logger.info("/getImageFeatures response:" + respuesta.toString());
		return respuesta;
	}
	
	
	public ResponseService getOrderPdf(Venta venta) throws Exception {
		logger.info("/getOrderPdf?venta="+venta.toString());
		ResponseService respuesta = new ResponseService();

		try {
			Call<ResponseService> responseService = service.getOrderPdf(venta);
			Response<ResponseService> response = responseService.execute();
			
			if(response.code()==200)
				respuesta = response.body();				

		} catch (Exception ex) {
			throw new Exception(ex.getMessage(), ex.getCause());			
		}
		
		logger.info("/getOrderPdf response:" + respuesta.toString());
		return respuesta;
	}
	
	
	public ResponseService calculateTotalAmount(Producto producto) throws Exception {
		logger.info("/calculateTotalAmount?producto="+producto.toString());
		ResponseService respuesta = new ResponseService();
		
		try {
			Call<ResponseService> responseService = service.calculateTotalAmount(producto);
			Response<ResponseService> response = responseService.execute();
			
			if(response.code()==200)
				respuesta = response.body();				
			
		} catch (Exception ex) {
			throw new Exception(ex.getMessage(), ex.getCause());			
		}
		
		logger.info("/calculateTotalAmount response:" + respuesta.toString());
		return respuesta;
	}
	
	
	public ResponseService getWorkSector(Integer estatus) throws Exception {
		logger.info("/getWorkSector");
		ResponseService respuesta = new ResponseService();
		
		try {
			Call<ResponseService> responseService = service.getWorkSector(estatus);
			Response<ResponseService> response = responseService.execute();
			
			if(response.code()==200)
				respuesta = response.body();				
			
		} catch (Exception ex) {
			throw new Exception(ex.getMessage(), ex.getCause());			
		}
		
		logger.info("/getWorkSector response:" + respuesta.toString());
		return respuesta;
	}
	
	
	public ResponseService getCFDIUse(Integer estatus) throws Exception {
		logger.info("/getCFDIUse");
		ResponseService respuesta = new ResponseService();
		
		try {
			Call<ResponseService> responseService = service.getCFDIUse(estatus);
			Response<ResponseService> response = responseService.execute();
			
			if(response.code()==200)
				respuesta = response.body();				
			
		} catch (Exception ex) {
			throw new Exception(ex.getMessage(), ex.getCause());			
		}
		
		logger.info("/getCFDIUse response:" + respuesta.toString());
		return respuesta;
	}
	
	
	public ResponseService getCountries(Integer estatus) throws Exception {
		logger.info("/getCountries");
		ResponseService respuesta = new ResponseService();
		
		try {
			Call<ResponseService> responseService = service.getCountries(estatus);
			Response<ResponseService> response = responseService.execute();
			
			if(response.code()==200)
				respuesta = response.body();				
			
		} catch (Exception ex) {
			throw new Exception(ex.getMessage(), ex.getCause());			
		}
		
		logger.info("/getCountries response:" + respuesta.toString());
		return respuesta;
	}
	
	
	public ResponseService getStateMexico(Integer estatus) throws Exception {
		logger.info("/getStateMexico");
		ResponseService respuesta = new ResponseService();
		
		try {
			Call<ResponseService> responseService = service.getStateMexico(estatus);
			Response<ResponseService> response = responseService.execute();
			
			if(response.code()==200)
				respuesta = response.body();				
			
		} catch (Exception ex) {
			throw new Exception(ex.getMessage(), ex.getCause());			
		}
		
		logger.info("/getStateMexico response:" + respuesta.toString());
		return respuesta;
	}
	
	
	public ResponseService getShipments(CotizacionEnvio envio) throws Exception {
		logger.info("/getShipments");
		ResponseService respuesta = new ResponseService();
		
		try {
			Call<ResponseService> responseService = service.shipments(envio);
			Response<ResponseService> response = responseService.execute();
			
			if(response.code()==200)
				respuesta = response.body();				
			
		} catch (Exception ex) {
			throw new Exception(ex.getMessage(), ex.getCause());			
		}
		
		logger.info("/getShipments response:" + respuesta.toString());
		return respuesta;
	}
	
	
	public ResponseService saveSale(Venta venta) throws Exception {
		logger.info("/saveSale");
		ResponseService respuesta = new ResponseService();
		
		try {
			Call<ResponseService> responseService = service.saveSale(venta);
			Response<ResponseService> response = responseService.execute();
			
			if(response.code()==200) {
				respuesta = response.body();
				if(!respuesta.isExitoso())
					throw new Exception(respuesta.getMensaje());
			}
			
			
		} catch (Exception ex) {
			throw new Exception(ex.getMessage(), ex.getCause());			
		}
		
		logger.info("/saveSale response:" + respuesta.toString());
		return respuesta;
	}
	
	
	public ResponseService getFeaturesById(Integer pkCaracteristica) throws Exception {
		logger.info("/getFeaturesById/pkCaracteristica="+pkCaracteristica);
		ResponseService respuesta = new ResponseService();
		
		try {
			Call<ResponseService> responseService = service.getFeaturesById(pkCaracteristica);
			Response<ResponseService> response = responseService.execute();
			
			if(response.code()==200)
				respuesta = response.body();				
			
		} catch (Exception ex) {
			throw new Exception(ex.getMessage(), ex.getCause());			
		}
		
		logger.info("/getFeaturesById response:" + respuesta.toString());
		return respuesta;
	}
}
