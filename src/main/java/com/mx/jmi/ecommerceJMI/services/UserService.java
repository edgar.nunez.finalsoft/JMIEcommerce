package com.mx.jmi.ecommerceJMI.services;

import java.util.List;

import com.mx.jmi.ecommerceJMI.beans.ClienteDTO;
import com.mx.jmi.ecommerceJMI.beans.DireccionDTO;
import com.mx.jmi.ecommerceJMI.entities.Categoria;
import com.mx.jmi.ecommerceJMI.utils.ResponseService;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface UserService {

	@GET("api/v1/token/validateCode")
	public Call<ResponseService> validateCode(@Query("email") String email, @Query("code") String code);

	@GET("api/v1/email/sendCodeLogin")
	public Call<ResponseService> sendCodeLogin(@Query("email") String email);
	
	@GET("api/v1/client/getClientByEmail")
	public Call<ResponseService> getClientByEmail(@Query("email") String email);
	
	@GET("api/v1/client/getClientById/{pkClient}")
	public Call<ResponseService> getClientById(@Path("pkClient") Integer pkClient);
		
	@POST("api/v1/address/saveAddress")
	public Call<ResponseService> saveAddress(@Body DireccionDTO address);
	
	@PUT("api/v1/address/updateAddress")
	public Call<ResponseService> updateAddress(@Body DireccionDTO address);
	
	@POST("api/v1/client/saveClient")
	public Call<ResponseService> saveClient(@Body ClienteDTO client);
	
	@PUT("api/v1/client/updateClient")
	public Call<ResponseService> updateClient(@Body ClienteDTO client);
	
}
