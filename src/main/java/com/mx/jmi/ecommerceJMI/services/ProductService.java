package com.mx.jmi.ecommerceJMI.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Value;

import com.mx.jmi.ecommerceJMI.beans.DireccionDTO;
import com.mx.jmi.ecommerceJMI.entities.Categoria;
import com.mx.jmi.ecommerceJMI.entities.CotizacionEnvio;
import com.mx.jmi.ecommerceJMI.entities.Producto;
import com.mx.jmi.ecommerceJMI.entities.Subcategoria;
import com.mx.jmi.ecommerceJMI.entities.Venta;
import com.mx.jmi.ecommerceJMI.utils.ResponseService;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ProductService {

	@GET("api/v1/category/getCategory")
	public Call<ResponseService> getCategorias(@Query("estatus") Integer estatus);
	
	@GET("api/v1/subcategory/getSubcategory")
	public Call<ResponseService> getSubcategorias();
	
	@GET("api/v1/subcategory/getSubCategoryByCategory")
	public Call<ResponseService> getSubcategoriasPorCategoria(@Query("pkCategoria") Integer pkCategoria);
	
	@GET("api/v1/product/getProductoBySubCategory")
	public Call<ResponseService> getProductoBySubCategory(@Query("pkSubcategoria") Integer pkSubcategoria);
	
	@GET("api/v1/product/getProductoByCategory")
	public Call<ResponseService> getProductoByCategory(@Query("pkCategoria") Integer pkCategoria);
	
	@GET("api/v1/subcategory/getSubcategoryById/{pkSubcategoria}")
	public Call<ResponseService> getSubcategoryById(@Path("pkSubcategoria") Integer pkSubcategoria);
	
	@GET("api/v1/category/getCategoryById/{pkCategoria}")
	public Call<ResponseService> getCategoryById(@Path("pkCategoria") Integer pkCategoria);
	
	@GET("api/v1/product/getProductByName")
	public Call<ResponseService> getProductByName(@Query("nombre") String nombre);
		
	@GET("api/v1/product/getProductById/{pkProducto}")
	public Call<ResponseService> getProductById(@Path("pkProducto") Integer pkProducto);
	
	@GET("api/v1/feature/getCategoryFeatureByProduct/{pkProducto}")
	public Call<ResponseService> getCategoryFeatureByProduct(@Path("pkProducto") Integer pkProducto);

	@GET("api/v1/feature/getFeaturesById")
	public Call<ResponseService> getFeaturesById(@Query("pkProducto") Integer pkProducto, @Query("pkCaracteristica") Integer pkCaracteristica);
	
	@GET("api/v1/product/getProductSuggested")
	public Call<ResponseService> getProductSuggested();
	
	@GET("api/v1/feature/getImageFeatures")
	public Call<ResponseService> getImageFeatures(@Query("pkProducto") Integer pkProducto, @Query("pkCaracteristicas") String pkCaracteristicas);
	
	@POST("api/v1/feature/calculateTotalAmount")
	public Call<ResponseService> calculateTotalAmount(@Body Producto producto);	
	
	@POST("api/v1/sale/getOrderPdf")
	public Call<ResponseService> getOrderPdf(@Body Venta sale);
	
	@GET("api/v1/catalog/getWorkSector")
	public Call<ResponseService> getWorkSector(@Query("estatus") Integer estatus);
	
	@GET("api/v1/catalog/getCFDIUse")
	public Call<ResponseService> getCFDIUse(@Query("estatus") Integer estatus);
	
	@GET("api/v1/catalog/getCountries")
	public Call<ResponseService> getCountries(@Query("estatus") Integer estatus);
	
	@GET("api/v1/catalog/getStateMexico")
	public Call<ResponseService> getStateMexico(@Query("estatus") Integer estatus);
	
	@POST("api/v1/skydropx/shipments")
	public Call<ResponseService> shipments(@Body CotizacionEnvio envio);

	@POST("api/v1/sale/saveSale")
	public Call<ResponseService> saveSale(@Body Venta venta);
	
	@GET("api/v1/feature/getFeaturesById/{pkCaracteristica}")
	public Call<ResponseService> getFeaturesById(@Path("pkCaracteristica") Integer pkCaracteristica);
}
