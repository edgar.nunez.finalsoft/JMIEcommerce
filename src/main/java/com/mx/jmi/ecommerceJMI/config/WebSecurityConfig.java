package com.mx.jmi.ecommerceJMI.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {	

	String[] resources = new String[] { "/include/**", "/assets/**", "/css/**", "/icons/**", "/img/**", "/js/**","/map/**","/gif/**", "/layer/**" };

	BCryptPasswordEncoder bCryptPasswordEncoder;
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.csrf().disable()
		.authorizeRequests()
		.antMatchers(resources).permitAll()
		.antMatchers("/", "/main","/listItem","/productDetails","/cart","/checkout","/api/user/**").permitAll()
//		.antMatchers("/user*").access("hasRole('USER')").anyRequest().authenticated()
		.and().formLogin().loginPage("/login").permitAll().defaultSuccessUrl("/menu")
		.failureUrl("/login?error=true").usernameParameter("username").passwordParameter("password").and().logout().permitAll().logoutSuccessUrl("/login?logout");
	}
 
	@Bean
	public BCryptPasswordEncoder passwordEncoder() {
		bCryptPasswordEncoder = new BCryptPasswordEncoder(4);
		return bCryptPasswordEncoder;
	}


}
