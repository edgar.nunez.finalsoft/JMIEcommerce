package com.mx.jmi.ecommerceJMI.beans;

import java.io.Serializable;

public class ClienteDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer pkcliente;
	private Integer idaspelcliente;
	private String nombres;
	private String apellidos;
	private String correo;
	private String telefono;
	private Integer estatus;

	public Integer getPkcliente() {
		return pkcliente;
	}

	public void setPkcliente(Integer pkcliente) {
		this.pkcliente = pkcliente;
	}

	public Integer getIdaspelcliente() {
		return idaspelcliente;
	}

	public void setIdaspelcliente(Integer idaspelcliente) {
		this.idaspelcliente = idaspelcliente;
	}

	public String getNombres() {
		return nombres;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public Integer getEstatus() {
		return estatus;
	}

	public void setEstatus(Integer estatus) {
		this.estatus = estatus;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
