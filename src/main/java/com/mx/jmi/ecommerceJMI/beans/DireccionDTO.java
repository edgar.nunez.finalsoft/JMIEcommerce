package com.mx.jmi.ecommerceJMI.beans;

import java.io.Serializable;

public class DireccionDTO implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer pkdomicilio;
	private String codigoPostal;
	private String estado;
	private String delegacion;
	private String ciudad;
	private String colonia;
	private String calle;
	private String noExterior;
	private String noInterior;
	private String referencia;
	private String estatus;
	private Integer fkcliente;
	
	
	public Integer getPkdomicilio() {
		return pkdomicilio;
	}
	public void setPkdomicilio(Integer pkdomicilio) {
		this.pkdomicilio = pkdomicilio;
	}
	public String getCodigoPostal() {
		return codigoPostal;
	}
	public void setCodigoPostal(String codigoPostal) {
		this.codigoPostal = codigoPostal;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public String getDelegacion() {
		return delegacion;
	}
	public void setDelegacion(String delegacion) {
		this.delegacion = delegacion;
	}
	public String getCiudad() {
		return ciudad;
	}
	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}
	public String getColonia() {
		return colonia;
	}
	public void setColonia(String colonia) {
		this.colonia = colonia;
	}
	public String getCalle() {
		return calle;
	}
	public void setCalle(String calle) {
		this.calle = calle;
	}
	public String getNoExterior() {
		return noExterior;
	}
	public void setNoExterior(String noExterior) {
		this.noExterior = noExterior;
	}
	public String getNoInterior() {
		return noInterior;
	}
	public void setNoInterior(String noInterior) {
		this.noInterior = noInterior;
	}
	public String getReferencia() {
		return referencia;
	}
	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}
	public String getEstatus() {
		return estatus;
	}
	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}
	public Integer getFkcliente() {
		return fkcliente;
	}
	public void setFkcliente(Integer fkcliente) {
		this.fkcliente = fkcliente;
	}
	
}
