package com.mx.jmi.ecommerceJMI.utils;

import java.lang.reflect.Type;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mx.jmi.ecommerceJMI.entities.Carrito;
import com.mx.jmi.ecommerceJMI.entities.Elemento;
import com.mx.jmi.ecommerceJMI.entities.Paginacion;
import com.mx.jmi.ecommerceJMI.entities.Producto;

public class Utils {
	
	
	
	public static String agregarAsteriscosPalabra(String palabra) {		
		if(palabra!=null && !palabra.isEmpty() && palabra.trim().length()>2) {
			return palabra.substring(0, 2)+"**********";
		}else {
			return "************";
		}		
	}
	
	
	public static String convertObjectAString(Object object){		
		Gson gson = new Gson();
		String json = gson.toJson(object);		
		return json;		
	}
	
	
	public static Paginacion createPaginationProduct(List<Producto> productos, int numeroElementosPorPagina, int paginaActual) {
		
		int paginas=0;
		int productoInicial=0;
		int productoFinal=0;
		List<Elemento> listaPaginas=new ArrayList<Elemento>();
		List<Producto> productosMostrar = new ArrayList<Producto>();
		
		if(productos!=null && productos.size()>0) {
			paginas = productos.size()/numeroElementosPorPagina;
			if((productos.size()%numeroElementosPorPagina)>0)
				paginas++;			
			
			if(paginaActual<=paginas) {		
				int elementoInicial=((paginaActual-1)*numeroElementosPorPagina);
				int elementoFinal=(paginaActual*numeroElementosPorPagina)-1;
				for(int i=elementoInicial;i<=elementoFinal;i++) {
					productoFinal++;
					productosMostrar.add(productos.get(i));
					
					if((productos.size()-1)==i)
						break;
				}				

				productoInicial = elementoInicial+1;				
			}
			
			
			
			if(paginaActual>1) 
				listaPaginas.add(new Elemento(0, "<<",String.valueOf(1)));			
			if(paginaActual>1) 
				listaPaginas.add(new Elemento(0, "<",String.valueOf(paginaActual-1)));				
			
			int contadorAux=0;
			for(int i=paginaActual;i<=paginas;i++) {
				contadorAux++;
				listaPaginas.add(new Elemento(paginaActual==i?1:0, String.valueOf(i), String.valueOf(i)));
				if(contadorAux==2)
					break;
			}
			
			
			if(paginas>2 &&  paginaActual<(paginas-1)) {
				listaPaginas.add(new Elemento(2, "...",null));				
				listaPaginas.add(new Elemento(0, String.valueOf(paginas),String.valueOf(paginas)));
			}
			if(paginaActual<paginas) 
				listaPaginas.add(new Elemento(0, ">",String.valueOf(paginaActual+1)));					
			if(paginaActual<paginas)
				listaPaginas.add(new Elemento(0, ">>",String.valueOf(paginas)));
			
				
		}
		
		
		Paginacion paginacion = new Paginacion();
		paginacion.setListaProductos(productos);
		paginacion.setNumeroProductosPorPagina(numeroElementosPorPagina);
		paginacion.setNumeroProductoInicial(productoInicial);
		paginacion.setNumeroProductoFinal(productoInicial + productoFinal - 1);
		paginacion.setNumeroProductos(productos.size());
		paginacion.setNumeroPaginas(paginas);
		paginacion.setPaginaActual(paginaActual);
		paginacion.setProductos(productosMostrar);		
		paginacion.setPaginas(listaPaginas);
		
		return paginacion;
		
	}
	
	
	public static Carrito getCart(Carrito carrito, String taxIva) {
		
		double subtotal=0.0, total=0.0, iva=0.0, totalProducto=0.0;				
		if(carrito!=null && carrito.getProductos()!=null && !carrito.getProductos().isEmpty()) {			
			
			for(Producto producto : carrito.getProductos()) {
				totalProducto=producto.getCantidad()*producto.getPrecioMenudeo();				
				subtotal=subtotal+totalProducto;				
				producto.setTotal(convertCurrencyFormat(totalProducto));
			}
			
			iva = Double.parseDouble(taxIva)/100;
			iva = subtotal * iva;
			total = iva + subtotal;
			
			carrito.setIva(convertCurrencyFormat(iva));
			carrito.setSubtotal(convertCurrencyFormat(subtotal));
			carrito.setTotal(convertCurrencyFormat(total));
		}
		
		return carrito;
	}
	
	
	public static String convertCurrencyFormat(double precio) {
		String precioStr = "0.00";
		DecimalFormat formatter = new DecimalFormat("###,###,###.00");
		if(precio>0)
			precioStr = formatter.format(precio);
	    return precioStr;
	}
	
	public static String getNumberCharString(int number, String text) {
		if(text!=null && !text.isEmpty() && text.length()>number) {
			text = text.substring(0, number) + "...";
		}		
		return text; 
	}
	
	
	public static void main(String[] arg) {
		
	}
	

}
