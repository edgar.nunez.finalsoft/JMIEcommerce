package com.mx.jmi.ecommerceJMI.utils;

import java.io.Serializable;

public class ResponseService implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String codigo;
	private String mensaje;
	private boolean exitoso;
	private Object respuesta;

	
	@Override
	public String toString() {
		return "ResponseService [codigo=" + codigo + ", mensaje=" + mensaje + ", exitoso=" + exitoso + ", respuesta="
				+ respuesta + "]";
	}

	public ResponseService() {
		super();
		exitoso=false;
	}

	public ResponseService(String codigo, String mensaje, boolean exitoso, Object respuesta) {
		super();
		this.codigo = codigo;
		this.mensaje = mensaje;
		this.exitoso = exitoso;
		this.respuesta = respuesta;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public boolean isExitoso() {
		return exitoso;
	}

	public void setExitoso(boolean exitoso) {
		this.exitoso = exitoso;
	}

	public Object getRespuesta() {
		return respuesta;
	}

	public void setRespuesta(Object respuesta) {
		this.respuesta = respuesta;
	}

}
