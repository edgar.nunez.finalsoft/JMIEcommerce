package com.mx.jmi.ecommerceJMI.controllers;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mx.jmi.ecommerceJMI.entities.CaracteristicaTermopar;
import com.mx.jmi.ecommerceJMI.entities.Carrito;
import com.mx.jmi.ecommerceJMI.entities.Categoria;
import com.mx.jmi.ecommerceJMI.entities.CategoriaTermopar;
import com.mx.jmi.ecommerceJMI.entities.Cliente;
import com.mx.jmi.ecommerceJMI.entities.CotizacionEnvio;
import com.mx.jmi.ecommerceJMI.entities.DatoFiscal;
import com.mx.jmi.ecommerceJMI.entities.Domicilio;
import com.mx.jmi.ecommerceJMI.entities.Elemento;
import com.mx.jmi.ecommerceJMI.entities.Imagen;
import com.mx.jmi.ecommerceJMI.entities.Paginacion;
import com.mx.jmi.ecommerceJMI.entities.Paquete;
import com.mx.jmi.ecommerceJMI.entities.Precio;
import com.mx.jmi.ecommerceJMI.entities.Producto;
import com.mx.jmi.ecommerceJMI.entities.Subcategoria;
import com.mx.jmi.ecommerceJMI.services.ProductServiceClient;
import com.mx.jmi.ecommerceJMI.services.UserServiceClient;
import com.mx.jmi.ecommerceJMI.utils.ResponseService;
import com.mx.jmi.ecommerceJMI.utils.Utils;

@Controller
public class PagesController {

	Logger logger = LoggerFactory.getLogger(PagesController.class);
	
	@Autowired
    ProductServiceClient productoService;

	ResponseService respuesta = new ResponseService();
	
	@Value("${pagination.number.element}")
    private int paginationNumberElement;

	@Value("${sesion.cart}")
	private String sesionCart;
	
	@Value("${sesion.user}")
	private String sesionUser;
	
	@Autowired
	private UserServiceClient userServiceClient;
	
	@Autowired
	private MessageSource messageSource;
	
	
	@GetMapping({"/","/main"})
	public String main(Model model, HttpSession session, HttpServletRequest request) {		
		logger.info("/main");
		model.addAttribute("seccion", "main");
			
		try {
			
			
			Cliente cliente = (Cliente) session.getAttribute(sesionUser);
			model.addAttribute("usuario", cliente);
			Carrito carrito = (Carrito) session.getAttribute(sesionCart);
			if(carrito!=null && carrito.isPagado()) {
				carrito=new Carrito();
				request.getSession().setAttribute(sesionCart, carrito);
			}
			
			model.addAttribute("carrito", carrito);
			
				
			respuesta = productoService.getCategoria();		
			if(respuesta.isExitoso()) {
				List<Categoria> categorias = (List<Categoria>) respuesta.getRespuesta();
				model.addAttribute("categorias", categorias);
			}
			
			
			respuesta = productoService.getProductSuggested();
			if(respuesta!=null && respuesta.isExitoso()) {
				String jsonProductos = Utils.convertObjectAString(respuesta.getRespuesta());
				Type listType = new TypeToken<List<Producto>>() {}.getType();
				List<Producto> productosSugeridos = new Gson().fromJson(jsonProductos, listType);
				for(Producto producto : productosSugeridos) 
					producto.setDescripcion(Utils.getNumberCharString(100, producto.getDescripcion()));
				
				model.addAttribute("productosSugeridos", productosSugeridos);
			}
			
			
			
		} catch (Exception e) {	
			logger.info("/main Exception-error:"+e.getMessage());
			return "error";
		}		
		
		
		return "paginaPrincipal";
	}
	
	@GetMapping("/about")
	public String about(Model model, HttpSession session, HttpServletRequest request) {		
		logger.info("/about");
		model.addAttribute("seccion", "privacyPolicy");
		
		try {
			
			Cliente cliente = (Cliente) session.getAttribute(sesionUser);
			model.addAttribute("usuario", cliente);
			Carrito carrito = (Carrito) session.getAttribute(sesionCart);
			if(carrito!=null && carrito.isPagado()) {
				carrito=new Carrito();
				request.getSession().setAttribute(sesionCart, carrito);
			}
			model.addAttribute("carrito", carrito);		
			
			
			List<Elemento> secciones = new ArrayList<Elemento>();
			secciones.add(new Elemento(0, messageSource.getMessage("seccionActual.inicio", null, LocaleContextHolder.getLocale()), "/main"));
			model.addAttribute("secciones", secciones);			
			secciones.add(new Elemento(1, messageSource.getMessage("seccionActual.quienesSomos", null, LocaleContextHolder.getLocale()), null));
			
			respuesta = productoService.getCategoria();		
			if(respuesta.isExitoso()) {
				List<Categoria> categorias = (List<Categoria>) respuesta.getRespuesta();
				model.addAttribute("categorias", categorias);
			}
			
		} catch (Exception e) {	
			logger.info("/about Exception-error:"+e.getMessage());
			return "error";
		}		
		
		
		return "paginaNosotros";
	}
	
	
	@GetMapping("/pay")
	public String pay(Model model, HttpSession session, HttpServletRequest request) {		
		logger.info("/about");
		model.addAttribute("seccion", "privacyPolicy");
		
		try {
						
			Cliente cliente = (Cliente) session.getAttribute(sesionUser);
			model.addAttribute("usuario", cliente);
			Carrito carrito = (Carrito) session.getAttribute(sesionCart);	
			model.addAttribute("carrito", carrito);		
			
			if(carrito==null || carrito.getProductos()==null || carrito.getProductos().isEmpty())
				return "redirect:/main";
						
			respuesta = productoService.getCategoria();		
			if(respuesta.isExitoso()) {
				List<Categoria> categorias = (List<Categoria>) respuesta.getRespuesta();
				model.addAttribute("categorias", categorias);
			}
			
			List<Elemento> secciones = new ArrayList<Elemento>();
			secciones.add(new Elemento(0, "Inicio", "/main"));
			secciones.add(new Elemento(0, "Carrito de compras", "/cart"));
			secciones.add(new Elemento(1, "Pagar", ""));
			model.addAttribute("secciones", secciones);	
						
			if(carrito!=null && carrito.isPagado()) {
				carrito=new Carrito();
				request.getSession().setAttribute(sesionCart, carrito);
			}
						
		} catch (Exception e) {	
			logger.info("/about Exception-error:"+e.getMessage());
			return "error";
		}		
		
		
		return "paginaPagado";
	}

	
	@GetMapping("/payError")
	public String payError(Model model, HttpSession session, HttpServletRequest request) {		
		logger.info("/about");
		model.addAttribute("seccion", "privacyPolicy");
		
		try {
			
			Cliente cliente = (Cliente) session.getAttribute(sesionUser);
			model.addAttribute("usuario", cliente);			
			Carrito carrito = (Carrito) session.getAttribute(sesionCart);
			
			if(carrito==null || carrito.getProductos()==null || carrito.getProductos().isEmpty())
				return "redirect:/main";
			
			if(carrito!=null && carrito.isPagado()) {
				carrito=new Carrito();
				request.getSession().setAttribute(sesionCart, carrito);
			}
			model.addAttribute("carrito", carrito);
			
			respuesta = productoService.getCategoria();		
			if(respuesta.isExitoso()) {
				List<Categoria> categorias = (List<Categoria>) respuesta.getRespuesta();
				model.addAttribute("categorias", categorias);
			}
			
		} catch (Exception e) {	
			logger.info("/about Exception-error:"+e.getMessage());
			return "error";
		}		
		
		
		return "paginaPagoError";
	}
	
	
	@GetMapping("/privacyPolicy")
	public String privacyPolicy(Model model, HttpSession session, HttpServletRequest request) {		
		logger.info("/privacyPolicy");
		model.addAttribute("seccion", "privacyPolicy");
		
		try {
						
			Cliente cliente = (Cliente) session.getAttribute(sesionUser);
			model.addAttribute("usuario", cliente);
			Carrito carrito = (Carrito) session.getAttribute(sesionCart);
			if(carrito!=null && carrito.isPagado()) {
				carrito=new Carrito();
				request.getSession().setAttribute(sesionCart, carrito);
			}
			model.addAttribute("carrito", carrito);		
			
			List<Elemento> secciones = new ArrayList<Elemento>();
			secciones.add(new Elemento(0, messageSource.getMessage("seccionActual.inicio", null, LocaleContextHolder.getLocale()), "/main"));
			model.addAttribute("secciones", secciones);			
			secciones.add(new Elemento(1, messageSource.getMessage("seccionActual.politicas", null, LocaleContextHolder.getLocale()), null));
			
			respuesta = productoService.getCategoria();		
			if(respuesta.isExitoso()) {
				List<Categoria> categorias = (List<Categoria>) respuesta.getRespuesta();
				model.addAttribute("categorias", categorias);
			}
			
		} catch (Exception e) {	
			logger.info("/privacyPolicy Exception-error:"+e.getMessage());
			return "error";
		}		
		
		
		return "paginaPoliticasPrivacidad";
	}
		
	
	@GetMapping("/listItem")
	public String listItem(@RequestParam String category, @RequestParam String subcategory, @RequestParam String product, @RequestParam String name,  @RequestParam String page, Model model, HttpSession session, HttpServletRequest request) throws NumberFormatException, Exception {		
		logger.info("/listItem?category="+category+"&subcategory="+subcategory+"&product="+product+"&name="+name+"&page="+page);
		model.addAttribute("seccion", "listItem");
		
		try {
			
			model.addAttribute("categoria", category);
			model.addAttribute("subcategoria", subcategory);
			model.addAttribute("producto", product);
			model.addAttribute("nombre", name);
			
			Cliente cliente = (Cliente) session.getAttribute(sesionUser);
			model.addAttribute("usuario", cliente);
			Carrito carrito = (Carrito) session.getAttribute(sesionCart);
			if(carrito!=null && carrito.isPagado()) {
				carrito=new Carrito();
				request.getSession().setAttribute(sesionCart, carrito);
			}
			model.addAttribute("carrito", carrito);			
			
			List<Producto> productos = new ArrayList<Producto>();
			int pkCategoria=0;
			
			if(category!=null && !category.isEmpty()) {
				respuesta = productoService.getSubcategorias((int)(Double.parseDouble(category)));
				if(respuesta.isExitoso()) {
					List<Subcategoria> subcategorias = (List<Subcategoria>) respuesta.getRespuesta();
					model.addAttribute("subcategorias", subcategorias);
				}
			}
			
			respuesta = productoService.getCategoria();		
			if(respuesta.isExitoso()) {
				List<Categoria> categorias = (List<Categoria>) respuesta.getRespuesta();
				model.addAttribute("categorias", categorias);
			}
			
			respuesta = productoService.getProductSuggested();
			if(respuesta!=null && respuesta.isExitoso()) {
				String jsonProductos = Utils.convertObjectAString(respuesta.getRespuesta());
				Type listType = new TypeToken<List<Producto>>() {}.getType();
				List<Producto> productosSugeridos = new Gson().fromJson(jsonProductos, listType);
				for(Producto producto : productosSugeridos) 
					producto.setDescripcion(Utils.getNumberCharString(100, producto.getDescripcion()));
				
				model.addAttribute("productosSugeridos", productosSugeridos);
			}
			
			if(page==null || page.isEmpty())
				page="1";
			
			if(category!=null && !category.isEmpty() && (subcategory==null || subcategory.isEmpty())) {
				pkCategoria=(int)(Double.parseDouble(category));
				respuesta = productoService.getProductByCategory((int)(Double.parseDouble(category)));
				if(respuesta!=null && respuesta.isExitoso()) {
					String jsonProductos = Utils.convertObjectAString(respuesta.getRespuesta());
					Type listType = new TypeToken<List<Producto>>() {}.getType();
					List<Producto> productosPorCategoria = new Gson().fromJson(jsonProductos, listType);
					productos.addAll(productosPorCategoria);
				}
			}
			
			
			if(subcategory!=null && !subcategory.isEmpty()) {								
				respuesta = productoService.getSubcategoryById((int)(Double.parseDouble(subcategory)));
				if(respuesta!=null && respuesta.isExitoso() && respuesta.getRespuesta()!=null) {
					String jsonSubcategoria = Utils.convertObjectAString(respuesta.getRespuesta());
					Type listType = new TypeToken<Subcategoria>() {}.getType();
					Subcategoria subcategoria = new Gson().fromJson(jsonSubcategoria, listType);
					pkCategoria = subcategoria.getFkCategoria();						
				}				
				
				respuesta = productoService.getProductBySubCategory((int)(Double.parseDouble(subcategory)));
				if(respuesta!=null && respuesta.isExitoso() && respuesta.getRespuesta()!=null) {
					String jsonProductos = Utils.convertObjectAString(respuesta.getRespuesta());
					Type listType = new TypeToken<List<Producto>>() {}.getType();
					List<Producto> productosPorSubCategoria = new Gson().fromJson(jsonProductos, listType);
					productos.addAll(productosPorSubCategoria);
				}
			}
			
			
			if(name!=null && !name.isEmpty()) {
				respuesta = productoService.getProductByName(name);
				if(respuesta!=null && respuesta.isExitoso() && respuesta.getRespuesta()!=null) {
					String jsonProductos = Utils.convertObjectAString(respuesta.getRespuesta());
					Type listType = new TypeToken<List<Producto>>() {}.getType();
					List<Producto> productosPorSubCategoria = new Gson().fromJson(jsonProductos, listType);
					productos.addAll(productosPorSubCategoria);
				}
			}
			
			
			List<Elemento> secciones = new ArrayList<Elemento>();
			secciones.add(new Elemento(0, messageSource.getMessage("seccionActual.inicio", null, LocaleContextHolder.getLocale()), "/main"));
			model.addAttribute("secciones", secciones);			
			if(pkCategoria>0) {					
				respuesta = productoService.getSubcategorias(pkCategoria);
				if(respuesta.isExitoso()) {
					List<Subcategoria> subcategorias = (List<Subcategoria>) respuesta.getRespuesta();
					model.addAttribute("subcategorias", subcategorias);
				}
				
				respuesta = productoService.getCategoryById(pkCategoria);
				if(respuesta.isExitoso()) {
					String jsonCategoria = Utils.convertObjectAString(respuesta.getRespuesta());
					Type listType = new TypeToken<Categoria>() {}.getType();
					Categoria categoria = new Gson().fromJson(jsonCategoria, listType);
					secciones.add(new Elemento(1, categoria.getNombre(), "/listItem?category="+categoria.getPkcategoria()+"&subcategory=&product=&name=&page="));
					model.addAttribute("tituloCategoria", categoria.getNombre());
				}
			}
			
			
			if(productos!=null && !productos.isEmpty()) {				
				List<Producto> productosAux = new ArrayList<Producto>();
				
				for(Producto producto : productos) {
					boolean encontrado=false;
					for(Producto productoAux : productosAux) {
						if(productoAux!=null && producto!=null && productoAux.getPkproducto()!=0 && producto.getPkproducto()!=0 && productoAux.getPkproducto()==producto.getPkproducto()) { 
							encontrado=true;
							break;
						}
					}
					
					if(!encontrado)
						productosAux.add(producto);
					
				}				
				productos = productosAux;
								
				for(Producto producto : productos) {
					producto.setDescripcion(Utils.getNumberCharString(100, producto.getDescripcion()));
					if(producto!=null && producto.getListaPrecios()!=null && producto.getListaPrecios().size()>0) {
						for(Precio precio : producto.getListaPrecios()) {
							if(precio.getEstatus()>0) {								
								producto.setPrecioMenudeo(precio.getPrecioMenudeo());
								producto.setPrecioMayoreo(precio.getPrecioMayoreo());
								producto.setPrecioMenudeoFormato(Utils.convertCurrencyFormat(precio.getPrecioMenudeo()));
								producto.setPrecioMayoreoFormato(Utils.convertCurrencyFormat(precio.getPrecioMayoreo()));
								break;
							}
						}
					}
				}							
				
				Paginacion paginacion = new Paginacion();
				paginacion = Utils.createPaginationProduct(productos, paginationNumberElement, Integer.parseInt(page));				
				model.addAttribute("paginacion", paginacion);						
				
			}else {				
				Paginacion paginacion = new Paginacion();
				paginacion = Utils.createPaginationProduct(productos, paginationNumberElement, Integer.parseInt(page));
				model.addAttribute("paginacion", paginacion);
			}
			
		
		} catch (Exception e) {	
			logger.info("/main Exception-error:"+e.getMessage());
			return "error";
		}		
		
		return "paginaListaProductos";
	}	
	
	@GetMapping("/productDetails")
	public String productDetails(@RequestParam String product, @RequestParam String category, @RequestParam String subcategory, @RequestParam String cart, Model model, HttpSession session, HttpServletRequest request) {			
		logger.info("/productDetails&product="+product+"&category="+category+"&subcategory="+subcategory+"&cart="+cart);
		model.addAttribute("seccion", "productDetails");
		
		try {
			
			Cliente cliente = (Cliente) session.getAttribute(sesionUser);
			model.addAttribute("usuario", cliente);
			Carrito carrito = (Carrito) session.getAttribute(sesionCart);
			if(carrito!=null && carrito.isPagado()) {
				carrito=new Carrito();
				request.getSession().setAttribute(sesionCart, carrito);
			}
			model.addAttribute("carrito", carrito);
			
			respuesta = productoService.getSubcategorias(1);
			if(respuesta.isExitoso()) {
				List<Subcategoria> subcategorias = (List<Subcategoria>) respuesta.getRespuesta();
				model.addAttribute("subcategoriaTermopares", subcategorias);
			}
			
			respuesta = productoService.getCategoria();		
			if(respuesta.isExitoso()) {
				List<Categoria> categorias = (List<Categoria>) respuesta.getRespuesta();
				model.addAttribute("categorias", categorias);
			}
			
			respuesta = productoService.getProductSuggested();
			if(respuesta!=null && respuesta.isExitoso()) {
				String jsonProductos = Utils.convertObjectAString(respuesta.getRespuesta());
				Type listType = new TypeToken<List<Producto>>() {}.getType();
				List<Producto> productosSugeridos = new Gson().fromJson(jsonProductos, listType);
				for(Producto producto : productosSugeridos) 
					producto.setDescripcion(Utils.getNumberCharString(100, producto.getDescripcion()));
				
				model.addAttribute("productosSugeridos", productosSugeridos);
			}
						
			Producto producto = new Producto();
			respuesta = productoService.getProductById((int)(Double.parseDouble(product)));
			if(respuesta.isExitoso() && respuesta.getRespuesta()!=null) {				
				String jsonProducto = Utils.convertObjectAString(respuesta.getRespuesta());
				Type listType = new TypeToken<Producto>() {}.getType();
				producto = new Gson().fromJson(jsonProducto, listType);
				
				if(producto!=null && producto.getListaPrecios()!=null && producto.getListaPrecios().size()>0) {
					for(Precio precio : producto.getListaPrecios()) {
						if(precio.getEstatus()>0) {
							producto.setPrecioMenudeo(precio.getPrecioMenudeo());
							producto.setPrecioMayoreo(precio.getPrecioMayoreo());
							producto.setPrecioMenudeoFormato(Utils.convertCurrencyFormat(precio.getPrecioMenudeo()));
							producto.setPrecioMayoreoFormato(Utils.convertCurrencyFormat(precio.getPrecioMayoreo()));
							break;
						}
					}
				}
				
				
				if(cart!=null && !cart.isEmpty()) {
					producto.setCantidad(carrito.getProductos().get(Integer.parseInt(cart)).getCantidad());
					producto.setPrecioMenudeoFormato(carrito.getProductos().get(Integer.parseInt(cart)).getPrecioMenudeoFormato());
				}
				
				respuesta = productoService.getCategoryFeatureByProduct((int)(Double.parseDouble(product)));
				if(respuesta.isExitoso() && respuesta.getRespuesta()!=null) {
					String jsonCategoriasTermopar = Utils.convertObjectAString(respuesta.getRespuesta());
					listType = new TypeToken<List<CategoriaTermopar>>() {}.getType();
					List<CategoriaTermopar> categoriasTermopar = new Gson().fromJson(jsonCategoriasTermopar, listType);					
					if(categoriasTermopar!=null && !categoriasTermopar.isEmpty()) { 
						producto.setListaCategoriaTermopar(categoriasTermopar);
												
						if(cart==null || cart.isEmpty()) {
							respuesta = productoService.getFeaturesById((int)(Double.parseDouble(product)),0);
							if(respuesta.isExitoso() && respuesta.getRespuesta()!=null) {
								jsonProducto = Utils.convertObjectAString(respuesta.getRespuesta());
								listType = new TypeToken<List<CaracteristicaTermopar>>() {}.getType();
								List<CaracteristicaTermopar> listaCaracteristicasTermopar = new Gson().fromJson(jsonProducto, listType);
								
								if(listaCaracteristicasTermopar!=null && !listaCaracteristicasTermopar.isEmpty() && producto.getListaCategoriaTermopar()!=null && !producto.getListaCategoriaTermopar().isEmpty()) { 
									producto.getListaCategoriaTermopar().get(0).setListaCaracteristicasTermopar(listaCaracteristicasTermopar);
								}							
							}
						}else {
							String caracteristicasImagenTermopar="";
							Double precioTermopar=0.0;
							
							if(carrito.getProductos().get(Integer.parseInt(cart)).getListaCategoriaTermopar()!=null && !carrito.getProductos().get(Integer.parseInt(cart)).getListaCategoriaTermopar().isEmpty()) {
								for(int i=0;i<carrito.getProductos().get(Integer.parseInt(cart)).getListaCategoriaTermopar().size();i++) {																		
									
									if(carrito.getProductos().get(Integer.parseInt(cart)).getListaCategoriaTermopar().get(i).getListaCaracteristicasTermopar()!=null && !carrito.getProductos().get(Integer.parseInt(cart)).getListaCategoriaTermopar().get(i).getListaCaracteristicasTermopar().isEmpty()) {										
										if(i==0)
											caracteristicasImagenTermopar = String.valueOf(carrito.getProductos().get(Integer.parseInt(cart)).getListaCategoriaTermopar().get(i).getListaCaracteristicasTermopar().get(0).getPkcaracteristicatermopar());
										else
											caracteristicasImagenTermopar = caracteristicasImagenTermopar + ',' + carrito.getProductos().get(Integer.parseInt(cart)).getListaCategoriaTermopar().get(i).getListaCaracteristicasTermopar().get(0).getPkcaracteristicatermopar();
									}
									
									if((i>0 && carrito.getProductos().get(Integer.parseInt(cart)).getListaCategoriaTermopar().get(i-1).getListaCaracteristicasTermopar()!=null && !carrito.getProductos().get(Integer.parseInt(cart)).getListaCategoriaTermopar().get(i-1).getListaCaracteristicasTermopar().isEmpty()) || i==0) {
										respuesta = productoService.getFeaturesById((int)(Double.parseDouble(product)),i==0?0:carrito.getProductos().get(Integer.parseInt(cart)).getListaCategoriaTermopar().get(i-1).getListaCaracteristicasTermopar().get(0).getPkPadre());
										if(respuesta.isExitoso() && respuesta.getRespuesta()!=null) {
											jsonProducto = Utils.convertObjectAString(respuesta.getRespuesta());
											listType = new TypeToken<List<CaracteristicaTermopar>>() {}.getType();
											List<CaracteristicaTermopar> listaCaracteristicasTermopar = new Gson().fromJson(jsonProducto, listType);
											
											if(listaCaracteristicasTermopar!=null && !listaCaracteristicasTermopar.isEmpty()) {
												for(int j=0;j<listaCaracteristicasTermopar.size();j++) {
													listaCaracteristicasTermopar.get(j).setBloqueado(true);
													if(carrito.getProductos().get(Integer.parseInt(cart)).getListaCategoriaTermopar().get(i).getListaCaracteristicasTermopar()!=null && !carrito.getProductos().get(Integer.parseInt(cart)).getListaCategoriaTermopar().get(i).getListaCaracteristicasTermopar().isEmpty() && listaCaracteristicasTermopar.get(j).getPkcaracteristicatermopar()==carrito.getProductos().get(Integer.parseInt(cart)).getListaCategoriaTermopar().get(i).getListaCaracteristicasTermopar().get(0).getPkcaracteristicatermopar()) {
														listaCaracteristicasTermopar.get(j).setSeleccionado(listaCaracteristicasTermopar.get(j).getPkcaracteristicatermopar());
														listaCaracteristicasTermopar.get(j).setValores(carrito.getProductos().get(Integer.parseInt(cart)).getListaCategoriaTermopar().get(i).getListaCaracteristicasTermopar().get(0).getValores());														
														break;
													}
												}
											}	
											producto.getListaCategoriaTermopar().get(i).setListaCaracteristicasTermopar(listaCaracteristicasTermopar);
										}
									}
								}
							}
							
							respuesta = productoService.getImageFeatures(producto.getPkproducto(),caracteristicasImagenTermopar);
							if(respuesta.isExitoso() && respuesta.getRespuesta()!=null && !respuesta.getRespuesta().toString().isEmpty()) {
								if (producto.getListaProductoImagen()==null)
									producto.setListaProductoImagen(new ArrayList<Imagen>());
									
								producto.getListaProductoImagen().add(0,new Imagen((String)respuesta.getRespuesta()));									
							}
							
						}				
					}							
				}				
				model.addAttribute("producto", producto);				
			}
			
			List<Elemento> secciones = new ArrayList<Elemento>();
			secciones.add(new Elemento(0, messageSource.getMessage("seccionActual.inicio", null, LocaleContextHolder.getLocale()), "/main"));

			if(category!=null && !category.isEmpty()) {
				respuesta = productoService.getCategoryById((int)(Double.parseDouble(category)));
				if(respuesta.isExitoso()) {
					String jsonCategoria = Utils.convertObjectAString(respuesta.getRespuesta());
					Type listType = new TypeToken<Categoria>() {}.getType();
					Categoria categoria = new Gson().fromJson(jsonCategoria, listType);
					secciones.add(new Elemento(0, categoria.getNombre(), "/listItem?category="+category+"&subcategory=&product=&name=&page="));											
				}
			}			
			
			if(subcategory!=null && !subcategory.isEmpty()) {
				respuesta = productoService.getSubcategoryById((int)(Double.parseDouble(subcategory)));
				if(respuesta!=null && respuesta.isExitoso()) {
					String jsonSubcategoria = Utils.convertObjectAString(respuesta.getRespuesta());
					Type listType = new TypeToken<Subcategoria>() {}.getType();
					Subcategoria subcategoria = new Gson().fromJson(jsonSubcategoria, listType);
					secciones.add(new Elemento(0, subcategoria.getNombre(), "/listItem?category=&subcategory="+subcategory+"&product=&name=&page="));						
				}
			}
			
			if(producto!=null) {
				secciones.add(new Elemento(1, producto.getNombre(), null));
			}
			
			model.addAttribute("secciones", secciones);						
			
		} catch (Exception e) {	
			logger.info("/main Exception-error:"+e.getMessage());
			return "error";
		}		
		
		return "paginaProducto";
	}
	
	
	@GetMapping("/cart")
	public String cart(Model model, HttpSession session, HttpServletRequest request) {		
		logger.info("/cart");
		model.addAttribute("seccion", "cart");
		
		try {
			
			Cliente cliente = (Cliente) session.getAttribute(sesionUser);
			model.addAttribute("usuario", cliente);
			Carrito carrito = (Carrito) session.getAttribute(sesionCart);
			
			List<Elemento> secciones = new ArrayList<Elemento>();
			secciones.add(new Elemento(0, messageSource.getMessage("seccionActual.inicio", null, LocaleContextHolder.getLocale()), "/main"));
			secciones.add(new Elemento(1, messageSource.getMessage("seccionActual.carritoCompras", null, LocaleContextHolder.getLocale()), "/cart"));
			model.addAttribute("secciones", secciones);
			
			respuesta = productoService.getCategoria();		
			if(respuesta.isExitoso()) {
				List<Categoria> categorias = (List<Categoria>) respuesta.getRespuesta();
				model.addAttribute("categorias", categorias);
			}			
			
			if(carrito==null || carrito.getProductos()==null || carrito.getProductos().isEmpty())
				return "redirect:/main";
						
			model.addAttribute("carrito", carrito);
			
			
		} catch (Exception e) {	
			logger.info("/main Exception-error:"+e.getMessage());
			return "error";
		}		
		
		return "paginaCarrito";
	}
	
	
	@PostMapping("/checkout")
	public String checkout(@ModelAttribute Cliente user, Model model, HttpSession session, HttpServletRequest request) {
		logger.info("/checkout?correoElectronico="+user.getCorreo());
		model.addAttribute("seccion", "checkout");
		
		try {
			
			Carrito carrito = (Carrito) session.getAttribute(sesionCart);
			if(carrito!=null && carrito.isPagado()) {
				carrito=new Carrito();
				request.getSession().setAttribute(sesionCart, carrito);
			}
			model.addAttribute("carrito", carrito);
			
			List<Elemento> secciones = new ArrayList<Elemento>();
			secciones.add(new Elemento(0, messageSource.getMessage("seccionActual.inicio", null, LocaleContextHolder.getLocale()), "/main"));
			secciones.add(new Elemento(0, messageSource.getMessage("seccionActual.carritoCompras", null, LocaleContextHolder.getLocale()), "/cart"));
			secciones.add(new Elemento(1, messageSource.getMessage("seccionActual.pagar", null, LocaleContextHolder.getLocale()), ""));
			model.addAttribute("secciones", secciones);						
			
			respuesta = productoService.getCategoria();		
			if(respuesta.isExitoso()) {
				List<Categoria> categorias = (List<Categoria>) respuesta.getRespuesta();
				model.addAttribute("categorias", categorias);
			}
			
			
			respuesta = productoService.getWorkSector(1);		
			if(respuesta.isExitoso()) {
				List<Elemento> listaSectores = (List<Elemento>) respuesta.getRespuesta();
				model.addAttribute("listaSectores", listaSectores);
			} 

			
			respuesta = productoService.getCFDIUse(1);		
			if(respuesta.isExitoso()) {
				List<Elemento> listaUsoCFDI = (List<Elemento>) respuesta.getRespuesta();
				model.addAttribute("listaUsoCFDI", listaUsoCFDI);
			}			
			
			respuesta = productoService.getStateMexico(1);		
			if(respuesta.isExitoso()) {
				List<Elemento> listaEstados = (List<Elemento>) respuesta.getRespuesta();
				model.addAttribute("listaEstados", listaEstados);
			}
			
			
			if(carrito==null || carrito.getProductos()==null || carrito.getProductos().isEmpty())
				return "redirect:/main";
			
			Cliente cliente = (Cliente) session.getAttribute(sesionUser);
			if(cliente!=null && cliente.getPkcliente()>0 && cliente.isAutenticado()) {
				model.addAttribute("usuario", cliente);	
				return "paginaPago";
			}else {
				
				respuesta = userServiceClient.getClientByEmail(user.getCorreo());						
				if(respuesta != null && respuesta.isExitoso() && respuesta.getRespuesta()!=null) {
					String jsonCategoria = Utils.convertObjectAString(respuesta.getRespuesta());
					Type listType = new TypeToken<List<Cliente>>() {}.getType();
	
					List<Cliente> clientes = new Gson().fromJson(jsonCategoria, listType);
					if(clientes!=null && !clientes.isEmpty()) {				
						cliente = clientes.get(0);
						Cliente usuario = new Cliente();					
						usuario.setPkcliente(cliente.getPkcliente());
						usuario.setNombres(Utils.agregarAsteriscosPalabra(cliente.getNombres()));
						usuario.setApellidos(Utils.agregarAsteriscosPalabra(cliente.getApellidos()));
						usuario.setCorreo(Utils.agregarAsteriscosPalabra(cliente.getCorreo()));
						usuario.setTelefono(Utils.agregarAsteriscosPalabra(cliente.getTelefono()));
						usuario.setSector(Utils.agregarAsteriscosPalabra(cliente.getSector()));
						usuario.setRfc(Utils.agregarAsteriscosPalabra(cliente.getRfc()));
						usuario.setAutenticado(false);
						
						usuario.setListaDomicilios(new ArrayList<>());
						usuario.getListaDomicilios().add(new Domicilio());
						usuario.getListaDomicilios().get(0).setPkdomicilio(cliente.getListaDomicilios().get(0).getPkdomicilio());
						usuario.getListaDomicilios().get(0).setCodigoPostal(Utils.agregarAsteriscosPalabra(cliente.getListaDomicilios().get(0).getCodigoPostal()));
						usuario.getListaDomicilios().get(0).setDelegacion(Utils.agregarAsteriscosPalabra(cliente.getListaDomicilios().get(0).getDelegacion()));
						usuario.getListaDomicilios().get(0).setEstado(Utils.agregarAsteriscosPalabra(cliente.getListaDomicilios().get(0).getEstado()));
						usuario.getListaDomicilios().get(0).setCiudad(Utils.agregarAsteriscosPalabra(cliente.getListaDomicilios().get(0).getCiudad()));
						usuario.getListaDomicilios().get(0).setColonia(Utils.agregarAsteriscosPalabra(cliente.getListaDomicilios().get(0).getColonia()));
						usuario.getListaDomicilios().get(0).setNoExterior(Utils.agregarAsteriscosPalabra(cliente.getListaDomicilios().get(0).getNoExterior()));
						usuario.getListaDomicilios().get(0).setNoInterior(Utils.agregarAsteriscosPalabra(cliente.getListaDomicilios().get(0).getNoInterior()));
						usuario.getListaDomicilios().get(0).setCalle(Utils.agregarAsteriscosPalabra(cliente.getListaDomicilios().get(0).getCalle()));
						usuario.getListaDomicilios().get(0).setReferencia(Utils.agregarAsteriscosPalabra(cliente.getListaDomicilios().get(0).getReferencia()));
						usuario.getListaDomicilios().get(0).setPais(cliente.getListaDomicilios().get(0).getPais());
						
						CotizacionEnvio shipments = new CotizacionEnvio();			
						shipments.setApellidos(cliente.getApellidos());
						shipments.setNombres(cliente.getNombres());
						shipments.setCorreo(cliente.getCorreo());
						shipments.setTelefono(cliente.getTelefono());
						shipments.setCalle(cliente.getListaDomicilios().get(0).getCalle());
						shipments.setCodigopostal(cliente.getListaDomicilios().get(0).getCodigoPostal());
						shipments.setColonia(cliente.getListaDomicilios().get(0).getColonia());
						shipments.setDelegacion(cliente.getListaDomicilios().get(0).getDelegacion());
						shipments.setEstado(cliente.getListaDomicilios().get(0).getEstado());
						shipments.setNoexterior(cliente.getListaDomicilios().get(0).getNoExterior());
						shipments.setNointerior(cliente.getListaDomicilios().get(0).getNoInterior());
						shipments.setListaPedidos(new ArrayList<Producto>());
						for(Producto producto : carrito.getProductos()) {
							if(producto!=null) {
								Producto productoAux = new Producto();
								productoAux.setFkProducto(producto.getPkproducto());
								productoAux.setCantidad(producto.getCantidad());	
								shipments.getListaPedidos().add(productoAux);
							}
						}
						
						respuesta = productoService.getShipments(shipments);
						if(respuesta.isExitoso() && respuesta.getRespuesta()!=null) {				
							String jsonProducto = Utils.convertObjectAString(respuesta.getRespuesta());
							listType = new TypeToken<List<Paquete>>() {}.getType();
							List<Paquete> listaPaquetes = new Gson().fromJson(jsonProducto, listType);
							List<Paquete> listaPaquetesAux = new ArrayList<Paquete>();
							listaPaquetesAux.add(listaPaquetes.get(0));
							model.addAttribute("listaPaquetes", listaPaquetesAux);							
						} 
						
						request.getSession().setAttribute(sesionUser, usuario);					
						
						model.addAttribute("usuario", usuario);
						model.addAttribute("correoElectronico", cliente.getCorreo());
						
						logger.info("/validateEmail return=paginaPagoUsuario");
						return "paginaPagoUsuario";
					}else {					
						Cliente usuario = new Cliente();
						usuario.setListaDomicilios(new ArrayList<Domicilio>());
						usuario.setListaDatosFiscales(new ArrayList<DatoFiscal>());
						model.addAttribute("usuario", usuario);					
						model.addAttribute("correoElectronico", user.getCorreo());					
						return "paginaPago";
					}
					
				}else if(respuesta != null && respuesta.isExitoso() && respuesta.getRespuesta()==null) {
					logger.info("/validateEmail return=paginaPago");
					Cliente usuario = new Cliente();
					usuario.setListaDomicilios(new ArrayList<Domicilio>());
					usuario.setListaDatosFiscales(new ArrayList<DatoFiscal>());
					model.addAttribute("usuario", usuario);					
					model.addAttribute("correoElectronico", user.getCorreo());					
					return "paginaPago";
				}else {
					logger.error("/checkout Exception-error:"+respuesta.toString());
					return "paginaCarrito";
				}
			}
	
			
		} catch (Exception e) {	
			logger.error("/validateEmail Exception-error:"+e.getMessage());
			respuesta=new ResponseService();
			respuesta.setMensaje(e.getMessage());
			return "paginaCarrito";
		}		
	}
	
	
	  
	
	
}
