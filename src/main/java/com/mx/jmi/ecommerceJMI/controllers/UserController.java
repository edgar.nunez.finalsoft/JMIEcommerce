package com.mx.jmi.ecommerceJMI.controllers;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mx.jmi.ecommerceJMI.beans.ClienteDTO;
import com.mx.jmi.ecommerceJMI.beans.DireccionDTO;
import com.mx.jmi.ecommerceJMI.entities.Categoria;
import com.mx.jmi.ecommerceJMI.entities.Cliente;
import com.mx.jmi.ecommerceJMI.entities.DatoFiscal;
import com.mx.jmi.ecommerceJMI.entities.Domicilio;
import com.mx.jmi.ecommerceJMI.entities.Subcategoria;
import com.mx.jmi.ecommerceJMI.services.UserServiceClient;
import com.mx.jmi.ecommerceJMI.utils.ResponseService;
import com.mx.jmi.ecommerceJMI.utils.Utils;

@Controller
public class UserController {

	Logger logger = LoggerFactory.getLogger(UserController.class);
	
	private ResponseService respuesta = new ResponseService();
	
	@Autowired
	private UserServiceClient userServiceClient;
	
	@Value("${sesion.cart}")
	private String sesionCart;
	
	@Value("${sesion.user}")
	private String sesionUser;
		
	@GetMapping("/signOff")
	public String signOff(HttpSession session) {		
		logger.info("/signOff");
		
		try {			
			
			session.removeAttribute(sesionCart);
			session.removeAttribute(sesionUser);
			
		} catch (Exception e) {	
			logger.info("/signOff Exception-error:"+e.getMessage());
			return "error";
		}		
		
		return "redirect:/main";		
	}
	
	@GetMapping("/api/user/sendCode")
	public ResponseEntity<ResponseService> sendCode(@RequestParam String correo, Model model) {
		logger.info("/api/user/sendCode?correoElectronico="+ correo);
		
		try {
						
			respuesta = userServiceClient.sendCodeLogin(correo);
			
			if(respuesta.isExitoso()) {
				respuesta = new ResponseService();
				respuesta.setExitoso(true);
			}else {
				respuesta = new ResponseService();
				respuesta.setExitoso(false);
			}
				
			model.addAttribute("correoElectronico", correo);
			return new ResponseEntity<>(respuesta, HttpStatus.OK);			
		} catch (Exception e) {	
			logger.error("/api/user/sendCode Exception-error:"+e.getMessage());
			respuesta=new ResponseService();
			respuesta.setMensaje(e.getMessage());
			return new ResponseEntity<>(respuesta, HttpStatus.CONFLICT);
		}		
	}

	
	@GetMapping("/api/user/validateCode")
	public ResponseEntity<ResponseService> validateCode(@RequestParam String correo, @RequestParam String codigo, Model model, HttpServletRequest request) {
		logger.info("/api/user/validateCode?correoElectronico="+ correo+"&codigo="+codigo);
		
		try {
			respuesta = new ResponseService();
			respuesta = userServiceClient.validateCode(correo,codigo);	
			if(respuesta.isExitoso()) {
				respuesta = userServiceClient.getClientByEmail(correo);						
				if(respuesta != null && respuesta.isExitoso() && respuesta.getRespuesta()!=null) {
					String jsonCategoria = Utils.convertObjectAString(respuesta.getRespuesta());
					Type listType = new TypeToken<List<Cliente>>() {}.getType();

					List<Cliente> clientes = new Gson().fromJson(jsonCategoria, listType);
					if(clientes!=null && !clientes.isEmpty()) {
						Cliente cliente = clientes.get(0);
						cliente.setAutenticado(true);
						request.getSession().setAttribute(sesionUser, cliente);
						respuesta.setExitoso(true);
					}
				}
				
			}else {
				respuesta = new ResponseService();
				respuesta.setExitoso(false);
			}
			
			return new ResponseEntity<>(respuesta, HttpStatus.OK);			
		} catch (Exception e) {	
			logger.error("/api/user/validateCode Exception-error:"+e.getMessage());
			respuesta=new ResponseService();
			respuesta.setMensaje(e.getMessage());
			return new ResponseEntity<>(respuesta, HttpStatus.CONFLICT);
		}		
	}
	
	
	@PostMapping("/api/user/addUser")
	public ResponseEntity<ResponseService> addUser(@ModelAttribute Cliente usuario, Model model, HttpSession session, HttpServletRequest request) {
		logger.info("/api/user/addUser?client="+usuario.toString());
		
		try {
			
			respuesta = new ResponseService();
			Cliente cliente = (Cliente) session.getAttribute(sesionUser);
			
			
			if(cliente!=null && cliente.getPkcliente()>0) {
				
				boolean modificado=false;				
				if (cliente.getApellidos()!=null && usuario.getApellidos()!=null 
						&& !usuario.getApellidos().isEmpty() 
						&& !cliente.getApellidos().trim().equalsIgnoreCase(usuario.getApellidos().trim()))
					modificado=true;
				
				if (cliente.getCorreo()!=null && usuario.getCorreo()!=null 
						&& !usuario.getCorreo().isEmpty() 
						&& !cliente.getCorreo().trim().equalsIgnoreCase(usuario.getCorreo().trim()))
					modificado=true;
				
				if (cliente.getEstatus()!=usuario.getEstatus())
					modificado=true;
				
				if (cliente.getNombres()!=null && usuario.getNombres()!=null 
						&& !usuario.getNombres().isEmpty() 
						&& !cliente.getNombres().trim().equalsIgnoreCase(usuario.getNombres().trim()))
					modificado=true;
				
				if (cliente.getTelefono()!=null && usuario.getTelefono()!=null 
						&& !usuario.getTelefono().isEmpty() 
						&& !cliente.getTelefono().trim().equalsIgnoreCase(usuario.getTelefono().trim()))
					modificado=true;
				
				if(!modificado) {
					
					return new ResponseEntity<>(new ResponseService(null, null, true, null), HttpStatus.OK);
					
				}else {
					
					ClienteDTO clienteDTO = new ClienteDTO();
					clienteDTO.setApellidos(usuario.getApellidos());
					clienteDTO.setCorreo(usuario.getCorreo());
					clienteDTO.setEstatus(usuario.getEstatus());
					clienteDTO.setNombres(usuario.getNombres());
					clienteDTO.setTelefono(usuario.getTelefono());
					clienteDTO.setPkcliente(cliente.getPkcliente());
					
					respuesta = userServiceClient.updateClient(clienteDTO);					
					if(respuesta.isExitoso())  
						respuesta = new ResponseService(null, null, true, null);
					else 
						respuesta = new ResponseService(null, null, false, null);
															
					
				}				
			}else if(cliente!=null && cliente.getPkcliente()<1) {
				
				ClienteDTO clienteDTO = new ClienteDTO();
				clienteDTO.setApellidos(usuario.getApellidos());
				clienteDTO.setCorreo(usuario.getCorreo());
				clienteDTO.setEstatus(usuario.getEstatus());
				clienteDTO.setNombres(usuario.getNombres());
				clienteDTO.setTelefono(usuario.getTelefono());
				
				
				respuesta = userServiceClient.saveClient(clienteDTO);
				if(respuesta.isExitoso()) 
					respuesta = new ResponseService(null, null, true, null);
				else 
					respuesta = new ResponseService(null, null, false, null);
				
			}
			
			
			return new ResponseEntity<>(respuesta, HttpStatus.OK);			
		} catch (Exception e) {	
			logger.error("/api/user/addUser Exception-error:"+e.getMessage());
			respuesta=new ResponseService();
			respuesta.setMensaje(e.getMessage());
			return new ResponseEntity<>(respuesta, HttpStatus.CONFLICT);
		}		
	}
	
}
