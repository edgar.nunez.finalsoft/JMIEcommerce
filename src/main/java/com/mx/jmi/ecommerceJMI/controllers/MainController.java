package com.mx.jmi.ecommerceJMI.controllers;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.mx.jmi.ecommerceJMI.services.ProductServiceClient;
import com.mx.jmi.ecommerceJMI.utils.ResponseService;

@Controller
public class MainController {
	
    Logger logger = LoggerFactory.getLogger(MainController.class);
    
    @Value("${ecommerce.version}")
    private String ecommerceversion;

    @Value("${server.port}")
    private String serverport;

    @Value("${server.servlet.contextPath}")
    private String serverservletcontextPath;

    @Value("${spring.mvc.view.prefix}")
    private String springmvcviewprefix;

    @Value("${spring.mvc.view.suffix}")
    private String springmvcviewsuffix;

    @Value("${spring.thymeleaf.enabled}")
    private String springthymeleafenabled;

    @Value("${spring.messages.basename}")
    private String springmessagesbasename;

    @Value("${server.core.JMI}")
    private String servercoreJMI;

    @Value("${sesion.cart}")
    private String sesioncart;

    @Value("${sesion.user}")
    private String sesionuser;

    @Value("${pagination.number.element}")
    private String paginationnumberelement;

    @Value("${tax.iva}")
    private String taxiva;

    @GetMapping("/version")
	public ResponseEntity<String> main(Model model, HttpSession session) {
    	
    	logger.info("/EcommerceJMI V:" + ecommerceversion);    	
    	logger.info("ecommerce.version = " + ecommerceversion);
    	logger.info("server.port = " + serverport);
    	logger.info("server.servlet.contextPath = " + serverservletcontextPath);
    	logger.info("spring.mvc.view.prefix = " + springmvcviewprefix);
    	logger.info("spring.mvc.view.suffix = " + springmvcviewsuffix);
    	logger.info("spring.thymeleaf.enabled = " + springthymeleafenabled);
    	logger.info("spring.messages.basename = " + springmessagesbasename);
    	logger.info("server.core.JMI = " + servercoreJMI);
    	logger.info("sesion.cart = " + sesioncart);
    	logger.info("sesion.user = " + sesionuser);
    	logger.info("pagination.number.element = " + paginationnumberelement);
    	logger.info("tax.iva = " + taxiva);
    	
    	return new ResponseEntity<>("EcommerceJMI V:" + ecommerceversion, HttpStatus.OK);    	
    }
   

}
