package com.mx.jmi.ecommerceJMI.controllers;

import java.lang.reflect.Type;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.core.ExceptionDepthComparator;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mx.jmi.ecommerceJMI.entities.CaracteristicaTermopar;
import com.mx.jmi.ecommerceJMI.entities.Carrito;
import com.mx.jmi.ecommerceJMI.entities.CategoriaTermopar;
import com.mx.jmi.ecommerceJMI.entities.Cliente;
import com.mx.jmi.ecommerceJMI.entities.CotizacionEnvio;
import com.mx.jmi.ecommerceJMI.entities.DatoFiscal;
import com.mx.jmi.ecommerceJMI.entities.Domicilio;
import com.mx.jmi.ecommerceJMI.entities.Imagen;
import com.mx.jmi.ecommerceJMI.entities.Paquete;
import com.mx.jmi.ecommerceJMI.entities.Precio;
import com.mx.jmi.ecommerceJMI.entities.PrecioCaracteristicaTermopar;
import com.mx.jmi.ecommerceJMI.entities.Producto;
import com.mx.jmi.ecommerceJMI.entities.Venta;
import com.mx.jmi.ecommerceJMI.services.ProductService;
import com.mx.jmi.ecommerceJMI.services.ProductServiceClient;
import com.mx.jmi.ecommerceJMI.services.UserServiceClient;
import com.mx.jmi.ecommerceJMI.utils.ResponseService;
import com.mx.jmi.ecommerceJMI.utils.Utils;

@Controller
public class ProductController {

	@Autowired
	private ProductServiceClient productoService;	

	Logger logger = LoggerFactory.getLogger(ProductController.class);

	ResponseService respuesta = new ResponseService();	
	
	@Value("${sesion.user}")
	private String sesionUser;
	
	@Value("${sesion.cart}")
	private String sesionCart;
	
	@Value("${tax.iva}")
	private String taxIva;
	
	@Autowired
	private UserServiceClient userServiceClient;
	
	@Autowired
	private MessageSource messageSource;
	
	
	@PostMapping("/api/product/calculateTotalAmount")
	public ResponseEntity<ResponseService> calculateTotalAmount(@ModelAttribute Producto product, HttpServletRequest request, HttpSession session) {
		logger.info("/api/product/calculateTotalAmount");
		respuesta = new ResponseService();
		try {
		
			if(product.getPkproducto()!=0) {
				Producto producto = new Producto();				
				producto.setPkproducto(product.getPkproducto());
				if(product.getListaCategoriaTermopar()!=null && !product.getListaCategoriaTermopar().isEmpty()) {
					producto.setListaCaracteristicasTermopar(new ArrayList<CaracteristicaTermopar>());
					for(CategoriaTermopar categoria : product.getListaCategoriaTermopar()) {
						if(categoria.getListaCaracteristicasTermopar()!=null && !categoria.getListaCaracteristicasTermopar().isEmpty()) {
							for(CaracteristicaTermopar caracteristica : categoria.getListaCaracteristicasTermopar()) {
								if(caracteristica.getSeleccionado()>0) {
									CaracteristicaTermopar caracteristicaAux = new CaracteristicaTermopar();
									caracteristicaAux.setPkcaracteristicatermopar(caracteristica.getSeleccionado());
									if(caracteristica.getValores()!=null && !caracteristica.getValores().isEmpty()) {
										if( caracteristica.getValores().size()==3
												&& caracteristica.getValores().get(0)!=null && !caracteristica.getValores().get(0).isEmpty()
												&& caracteristica.getValores().get(1)!=null && !caracteristica.getValores().get(1).isEmpty() 
												&& caracteristica.getValores().get(2)!=null && !caracteristica.getValores().get(2).isEmpty()) {
											Double total = Double.valueOf(caracteristica.getValores().get(1))/Double.valueOf(caracteristica.getValores().get(2));
											total = total + Double.valueOf(caracteristica.getValores().get(0));
											caracteristicaAux.setValue(total);
										}else if(caracteristica.getValores().size()==1 && caracteristica.getValores().get(0)!=null && !caracteristica.getValores().get(0).isEmpty())
											caracteristicaAux.setValue(Double.valueOf(caracteristica.getValores().get(0)));
									}
									producto.getListaCaracteristicasTermopar().add(caracteristicaAux);
								}
							}
						}
					}
				}
				
				respuesta = productoService.calculateTotalAmount(producto);
				if(respuesta.isExitoso())
					respuesta.setRespuesta(Utils.convertCurrencyFormat(Double.parseDouble(String.valueOf(respuesta.getRespuesta()))));
			}
						
			return new ResponseEntity<>(respuesta, HttpStatus.OK);			
		} catch (Exception e) {	
			logger.error("/api/product/calculateTotalAmount Exception-error:"+e.getMessage()); 
			respuesta=new ResponseService();
			respuesta.setMensaje(e.getMessage());
			return new ResponseEntity<>(respuesta, HttpStatus.CONFLICT);
		}		
	}
	
	
	@PostMapping("/api/sale/createSale")
	public ResponseEntity<ResponseService> createSale(@ModelAttribute Venta sale, Model model, HttpServletRequest request, HttpSession session) {
		logger.info("/api/sale/createSale?sale="+ sale.toString());
		
		try {
			if(sale==null || sale.getCliente()==null) throw new Exception("Bad Request");
			if(sale.getPkEnvio()==null || sale.getPkEnvio().isEmpty()) throw new Exception("Delivery not selected");
			if(sale.getFormaPago()==null || sale.getFormaPago()==0) throw new Exception("Bad Request");
						
			Cliente usuario = (Cliente) session.getAttribute(sesionUser);			
			Carrito carrito = (Carrito) session.getAttribute(sesionCart);
			
			Venta ventaServidor = new Venta();
				
			//Cliente nuevo
			if(sale.getCliente().getPkcliente()==0) {
				ventaServidor.setCliente(new Cliente());				
				ventaServidor.getCliente().setNombres(sale.getCliente().getNombres());
				ventaServidor.getCliente().setApellidos(sale.getCliente().getApellidos());
				ventaServidor.getCliente().setCorreo(sale.getCliente().getCorreo());
				ventaServidor.getCliente().setTelefono(sale.getCliente().getTelefono());
				ventaServidor.getCliente().setSector(sale.getCliente().getSector());
				ventaServidor.getCliente().setRfc(sale.getCliente().getRfc());	
				
				ventaServidor.getCliente().setListaDomicilios(new ArrayList<Domicilio>());
				Domicilio domicilio = new Domicilio();
				domicilio.setCodigoPostal(sale.getCliente().getListaDomicilios().get(0).getCodigoPostal());
				domicilio.setDelegacion(sale.getCliente().getListaDomicilios().get(0).getDelegacion());
				domicilio.setEstado(sale.getCliente().getListaDomicilios().get(0).getEstado());
				domicilio.setColonia(sale.getCliente().getListaDomicilios().get(0).getColonia());
				domicilio.setNoExterior(sale.getCliente().getListaDomicilios().get(0).getNoExterior());
				domicilio.setNoInterior(sale.getCliente().getListaDomicilios().get(0).getNoInterior());
				domicilio.setCalle(sale.getCliente().getListaDomicilios().get(0).getCalle());
				domicilio.setReferencia(sale.getCliente().getListaDomicilios().get(0).getReferencia());				
				ventaServidor.getCliente().getListaDomicilios().add(domicilio);
				
				if(sale.getCliente().getListaDatosFiscales()!=null && !sale.getCliente().getListaDatosFiscales().isEmpty() && sale.getCliente().getListaDatosFiscales().get(0).getRazonSocial()!=null && !sale.getCliente().getListaDatosFiscales().get(0).getRazonSocial().isEmpty()) {
					ventaServidor.getCliente().setListaDomiciliosFiscales(new ArrayList<DatoFiscal>());
					DatoFiscal datoFiscal = new DatoFiscal();
					datoFiscal.setRazonSocial(sale.getCliente().getListaDatosFiscales().get(0).getRazonSocial());
					datoFiscal.setRfc(sale.getCliente().getListaDatosFiscales().get(0).getRfc());
					datoFiscal.setCodigoPostal(sale.getCliente().getListaDatosFiscales().get(0).getCodigoPostal());
					datoFiscal.setDelegacion(sale.getCliente().getListaDatosFiscales().get(0).getDelegacion());
					datoFiscal.setEstado(sale.getCliente().getListaDatosFiscales().get(0).getEstado());
					datoFiscal.setColonia(sale.getCliente().getListaDatosFiscales().get(0).getColonia());
					datoFiscal.setNoExterior(sale.getCliente().getListaDatosFiscales().get(0).getNoExterior());
					datoFiscal.setNoInterior(sale.getCliente().getListaDatosFiscales().get(0).getNoInterior());
					datoFiscal.setCalle(sale.getCliente().getListaDatosFiscales().get(0).getCalle());
					datoFiscal.setUsoCFDI(sale.getCliente().getListaDatosFiscales().get(0).getUsoCFDI());						
					ventaServidor.getCliente().getListaDomiciliosFiscales().add(datoFiscal);
				}				
			//Cliente registrado pero con campos abiertos
			}else if(sale.getCliente().getPkcliente()!=0 && usuario.isAutenticado()) {
				ventaServidor.setCliente(new Cliente());
				ventaServidor.getCliente().setPkcliente(sale.getCliente().getPkcliente());
				
				if(!sale.getCliente().getNombres().trim().equalsIgnoreCase(usuario.getNombres()) ||
						!sale.getCliente().getApellidos().trim().equalsIgnoreCase(usuario.getApellidos()) ||
						!sale.getCliente().getTelefono().trim().equalsIgnoreCase(usuario.getTelefono()) ||
						!sale.getCliente().getRfc().trim().equalsIgnoreCase(usuario.getRfc()) ||
						!sale.getCliente().getSector().trim().equalsIgnoreCase(usuario.getSector())) {				
					ventaServidor.getCliente().setNombres(sale.getCliente().getNombres());
					ventaServidor.getCliente().setApellidos(sale.getCliente().getApellidos());
					ventaServidor.getCliente().setTelefono(sale.getCliente().getTelefono());
					ventaServidor.getCliente().setSector(sale.getCliente().getSector());
					ventaServidor.getCliente().setRfc(sale.getCliente().getRfc());
				}
				ventaServidor.getCliente().setCorreo(usuario.getCorreo());
				
				ventaServidor.getCliente().setListaDomicilios(new ArrayList<Domicilio>());				
				int pkDomicilioEncontrado=0;
				for(Domicilio domicilio : usuario.getListaDomicilios()) {					
					if(sale.getCliente().getListaDomicilios().get(0).getCodigoPostal().trim().equalsIgnoreCase(domicilio.getCodigoPostal().trim()) && 
							sale.getCliente().getListaDomicilios().get(0).getDelegacion().trim().equalsIgnoreCase(domicilio.getDelegacion().trim()) && 
							sale.getCliente().getListaDomicilios().get(0).getEstado().trim().equalsIgnoreCase(domicilio.getEstado().trim()) &&
							sale.getCliente().getListaDomicilios().get(0).getColonia().trim().equalsIgnoreCase(domicilio.getColonia().trim()) && 
							sale.getCliente().getListaDomicilios().get(0).getNoExterior().trim().equalsIgnoreCase(domicilio.getNoExterior().trim()) && 
							sale.getCliente().getListaDomicilios().get(0).getNoInterior().trim().equalsIgnoreCase(domicilio.getNoInterior().trim()) &&
							sale.getCliente().getListaDomicilios().get(0).getCalle().trim().equalsIgnoreCase(domicilio.getCalle().trim()) &&
							sale.getCliente().getListaDomicilios().get(0).getReferencia().trim().equalsIgnoreCase(domicilio.getReferencia().trim())) {
						pkDomicilioEncontrado=domicilio.getPkdomicilio();						
						break;
					}					
				}
				
				if(pkDomicilioEncontrado>0) {
					ventaServidor.getCliente().setListaDomicilios(new ArrayList<Domicilio>());
					Domicilio domicilio = new Domicilio();										
					domicilio.setPkdomicilio(pkDomicilioEncontrado);				
					ventaServidor.getCliente().getListaDomicilios().add(domicilio);									
				}else {
					ventaServidor.getCliente().setListaDomicilios(new ArrayList<Domicilio>());
					Domicilio domicilio = new Domicilio();					
					domicilio.setCodigoPostal(sale.getCliente().getListaDomicilios().get(0).getCodigoPostal());
					domicilio.setDelegacion(sale.getCliente().getListaDomicilios().get(0).getDelegacion());
					domicilio.setEstado(sale.getCliente().getListaDomicilios().get(0).getEstado());
					domicilio.setColonia(sale.getCliente().getListaDomicilios().get(0).getColonia());
					domicilio.setNoExterior(sale.getCliente().getListaDomicilios().get(0).getNoExterior());
					domicilio.setNoInterior(sale.getCliente().getListaDomicilios().get(0).getNoInterior());
					domicilio.setCalle(sale.getCliente().getListaDomicilios().get(0).getCalle());
					domicilio.setReferencia(sale.getCliente().getListaDomicilios().get(0).getReferencia());				
					ventaServidor.getCliente().getListaDomicilios().add(domicilio);		
				}
				
				
				if(sale.getCliente().getListaDatosFiscales()!=null && !sale.getCliente().getListaDatosFiscales().isEmpty() && sale.getCliente().getListaDatosFiscales().get(0).getRazonSocial()!=null && !sale.getCliente().getListaDatosFiscales().get(0).getRazonSocial().isEmpty()) {
					int pkDatosFiscalesEncontrado=0;
					if(usuario.getListaDatosFiscales()!=null && !usuario.getListaDatosFiscales().isEmpty()) {
						for(DatoFiscal datoFiscal : usuario.getListaDatosFiscales()) {
							if(sale.getCliente().getListaDatosFiscales().get(0).getRazonSocial().trim().equalsIgnoreCase(datoFiscal.getRazonSocial().trim()) &&
								sale.getCliente().getListaDatosFiscales().get(0).getRfc().trim().equalsIgnoreCase(datoFiscal.getRfc().trim()) &&
								sale.getCliente().getListaDatosFiscales().get(0).getCodigoPostal().trim().equalsIgnoreCase(datoFiscal.getCodigoPostal().trim()) &&
								sale.getCliente().getListaDatosFiscales().get(0).getDelegacion().trim().equalsIgnoreCase(datoFiscal.getDelegacion().trim()) &&
								sale.getCliente().getListaDatosFiscales().get(0).getEstado().trim().equalsIgnoreCase(datoFiscal.getEstado().trim()) &&
								sale.getCliente().getListaDatosFiscales().get(0).getColonia().trim().equalsIgnoreCase(datoFiscal.getColonia().trim()) &&
								sale.getCliente().getListaDatosFiscales().get(0).getNoExterior().trim().equalsIgnoreCase(datoFiscal.getNoExterior().trim()) &&
								sale.getCliente().getListaDatosFiscales().get(0).getNoInterior().trim().equalsIgnoreCase(datoFiscal.getNoInterior().trim()) &&
								sale.getCliente().getListaDatosFiscales().get(0).getCalle().trim().equalsIgnoreCase(datoFiscal.getCalle().trim()) &&
								sale.getCliente().getListaDatosFiscales().get(0).getUsoCFDI().trim().equalsIgnoreCase(datoFiscal.getUsoCFDI().trim())							
							){
								pkDatosFiscalesEncontrado=datoFiscal.getPkDatoFiscal();
								break;
							}						
						}
					}
					
					if(pkDatosFiscalesEncontrado>0) {
						ventaServidor.getCliente().setListaDomiciliosFiscales(new ArrayList<DatoFiscal>());
						DatoFiscal datoFiscal = new DatoFiscal();
						datoFiscal.setPkDatoFiscal(pkDatosFiscalesEncontrado);
						ventaServidor.getCliente().getListaDomiciliosFiscales().add(datoFiscal);
					}else {
						ventaServidor.getCliente().setListaDomiciliosFiscales(new ArrayList<DatoFiscal>());
						DatoFiscal datoFiscal = new DatoFiscal();
						datoFiscal.setRazonSocial(sale.getCliente().getListaDatosFiscales().get(0).getRazonSocial());
						datoFiscal.setRfc(sale.getCliente().getListaDatosFiscales().get(0).getRfc());
						datoFiscal.setCodigoPostal(sale.getCliente().getListaDatosFiscales().get(0).getCodigoPostal());
						datoFiscal.setDelegacion(sale.getCliente().getListaDatosFiscales().get(0).getDelegacion());
						datoFiscal.setEstado(sale.getCliente().getListaDatosFiscales().get(0).getEstado());
						datoFiscal.setColonia(sale.getCliente().getListaDatosFiscales().get(0).getColonia());
						datoFiscal.setNoExterior(sale.getCliente().getListaDatosFiscales().get(0).getNoExterior());
						datoFiscal.setNoInterior(sale.getCliente().getListaDatosFiscales().get(0).getNoInterior());
						datoFiscal.setCalle(sale.getCliente().getListaDatosFiscales().get(0).getCalle());
						datoFiscal.setUsoCFDI(sale.getCliente().getListaDatosFiscales().get(0).getUsoCFDI());						
						ventaServidor.getCliente().getListaDomiciliosFiscales().add(datoFiscal);
					}					
				}
				
			//Cliente registrado pero con campos cerrados
			}else if(sale.getCliente().getPkcliente()!=0 && !usuario.isAutenticado()) {
				ventaServidor.setCliente(new Cliente());
				ventaServidor.getCliente().setPkcliente(sale.getCliente().getPkcliente());
				ventaServidor.getCliente().setCorreo(usuario.getCorreo());
				ventaServidor.getCliente().setListaDomicilios(new ArrayList<Domicilio>());
				Domicilio domicilio = new Domicilio();
				domicilio.setPkdomicilio(usuario.getListaDomicilios().get(0).getPkdomicilio());
				ventaServidor.getCliente().getListaDomicilios().add(domicilio);				
			}
			
			ventaServidor.setPkEnvio(sale.getPkEnvio());
			ventaServidor.setFormaPago(sale.getFormaPago());
			ventaServidor.setPkEnvioSolicitud(sale.getPkEnvioSolicitud());
			
			ventaServidor.setListaPedidos(new ArrayList<Producto>());
			for(Producto producto : carrito.getProductos()) {
				if(producto!=null) {
					Producto productoAux = new Producto();
					productoAux.setFkProducto(producto.getPkproducto());
					productoAux.setCantidad(producto.getCantidad());
					
					if(producto.getListaCategoriaTermopar()!=null && !producto.getListaCategoriaTermopar().isEmpty()) {
						productoAux.setListaPedidoCaracteristica(new ArrayList<CaracteristicaTermopar>());
						for(CategoriaTermopar categoriaTermoparAux : producto.getListaCategoriaTermopar()) {
							if(categoriaTermoparAux.getListaCaracteristicasTermopar()!=null && !categoriaTermoparAux.getListaCaracteristicasTermopar().isEmpty()) {
								CaracteristicaTermopar caracteristicaAux = new CaracteristicaTermopar();
								caracteristicaAux.setFkCaracteristicaTermopar(categoriaTermoparAux.getListaCaracteristicasTermopar().get(0).getPkcaracteristicatermopar());
								if(categoriaTermoparAux.getListaCaracteristicasTermopar().get(0).getValores()!=null && !categoriaTermoparAux.getListaCaracteristicasTermopar().get(0).getValores().isEmpty()) {
									if( categoriaTermoparAux.getListaCaracteristicasTermopar().get(0).getValores().size()==3
											&& categoriaTermoparAux.getListaCaracteristicasTermopar().get(0).getValores().get(0)!=null && !categoriaTermoparAux.getListaCaracteristicasTermopar().get(0).getValores().get(0).isEmpty()
											&& categoriaTermoparAux.getListaCaracteristicasTermopar().get(0).getValores().get(1)!=null && !categoriaTermoparAux.getListaCaracteristicasTermopar().get(0).getValores().get(1).isEmpty() 
											&& categoriaTermoparAux.getListaCaracteristicasTermopar().get(0).getValores().get(2)!=null && !categoriaTermoparAux.getListaCaracteristicasTermopar().get(0).getValores().get(2).isEmpty()) {
										Double total = Double.valueOf(categoriaTermoparAux.getListaCaracteristicasTermopar().get(0).getValores().get(1))/Double.valueOf(categoriaTermoparAux.getListaCaracteristicasTermopar().get(0).getValores().get(2));
										total = total + Double.valueOf(categoriaTermoparAux.getListaCaracteristicasTermopar().get(0).getValores().get(0));
										caracteristicaAux.setValue(total);
									}else if(categoriaTermoparAux.getListaCaracteristicasTermopar().get(0).getValores().size()==1 && categoriaTermoparAux.getListaCaracteristicasTermopar().get(0).getValores().get(0)!=null && !categoriaTermoparAux.getListaCaracteristicasTermopar().get(0).getValores().get(0).isEmpty())
										caracteristicaAux.setValue(Double.valueOf(categoriaTermoparAux.getListaCaracteristicasTermopar().get(0).getValores().get(0)));
								}
								productoAux.getListaPedidoCaracteristica().add(caracteristicaAux);
							}
						}						
					}					
					ventaServidor.getListaPedidos().add(productoAux);
				}
			}
			
			respuesta = productoService.saveSale(ventaServidor);
			
			if(respuesta.isExitoso()) {
				carrito.setPagado(true);
				request.getSession().setAttribute(sesionCart, carrito);
				model.addAttribute("carrito", carrito);				
			}
							
			return new ResponseEntity<>(respuesta, HttpStatus.OK);			
			
		} catch (Exception e) {	
			logger.error("/api/sale/createSale Exception-error:"+e.getMessage());
			respuesta=new ResponseService();			
			
			if(e.getMessage().equalsIgnoreCase("Delivery not selected")) 				
				respuesta.setMensaje(messageSource.getMessage("mensajeError.formaEntregaNoSeleccioada", null, LocaleContextHolder.getLocale()));
			else if(e.getMessage().equalsIgnoreCase("Existing Customer")) 				
				respuesta.setMensaje(messageSource.getMessage("mensajeError.correoElectronicoEnUso", null, LocaleContextHolder.getLocale()));
			else 
				respuesta.setMensaje(messageSource.getMessage("mensajeError.errorPeticion", null, LocaleContextHolder.getLocale()));
							
			return new ResponseEntity<>(respuesta, HttpStatus.OK);
		}	
		
		
	}
	
	@GetMapping("/api/sale/getOrderPdf")
	public ResponseEntity<ResponseService> getOrderPdf(HttpServletRequest request, HttpSession session) {
		logger.info("/api/sale/getOrderPdf");
		
		try {
			
			Venta venta = new Venta();
			venta.setListaPedidos(new ArrayList<Producto>());

			Carrito carrito = (Carrito) session.getAttribute(sesionCart);
			if(carrito!=null && carrito.getProductos()!=null && !carrito.getProductos().isEmpty()) {				
				for(Producto producto : carrito.getProductos()) {
					if(producto.getPkproducto()!=0) {
						Producto productoAux = new Producto();
						productoAux.setCantidad(producto.getCantidad());
						productoAux.setFkProducto(producto.getPkproducto());
						if(producto.getListaCategoriaTermopar()!=null && !producto.getListaCategoriaTermopar().isEmpty()) {
							productoAux.setListaPedidoCaracteristica(new ArrayList<CaracteristicaTermopar>());
							for(CategoriaTermopar categoria : producto.getListaCategoriaTermopar()) {
								if(categoria!=null && categoria.getListaCaracteristicasTermopar()!=null && !categoria.getListaCaracteristicasTermopar().isEmpty() 
								&& categoria.getListaCaracteristicasTermopar().get(0)!=null && categoria.getListaCaracteristicasTermopar().get(0).getPkcaracteristicatermopar()!=0) {
									CaracteristicaTermopar caracteristica = new CaracteristicaTermopar();
									caracteristica.setFkCaracteristicaTermopar(categoria.getListaCaracteristicasTermopar().get(0).getPkcaracteristicatermopar());
									
									if(categoria.getListaCaracteristicasTermopar().get(0).getValores()!=null && !categoria.getListaCaracteristicasTermopar().get(0).getValores().isEmpty() 
											&& categoria.getListaCaracteristicasTermopar().get(0).getValores().size()==3
											&& categoria.getListaCaracteristicasTermopar().get(0).getValores().get(0)!=null && !categoria.getListaCaracteristicasTermopar().get(0).getValores().get(0).isEmpty()
											&& categoria.getListaCaracteristicasTermopar().get(0).getValores().get(1)!=null && !categoria.getListaCaracteristicasTermopar().get(0).getValores().get(1).isEmpty()
											&& categoria.getListaCaracteristicasTermopar().get(0).getValores().get(2)!=null && !categoria.getListaCaracteristicasTermopar().get(0).getValores().get(2).isEmpty()) {
										Double total = Double.valueOf(categoria.getListaCaracteristicasTermopar().get(0).getValores().get(1))/Double.valueOf(categoria.getListaCaracteristicasTermopar().get(0).getValores().get(2));
										total = total + Double.valueOf(categoria.getListaCaracteristicasTermopar().get(0).getValores().get(0));
										caracteristica.setValue(total);
									} else if(categoria.getListaCaracteristicasTermopar().get(0).getValores()!=null && !categoria.getListaCaracteristicasTermopar().get(0).getValores().isEmpty() 
											&& categoria.getListaCaracteristicasTermopar().get(0).getValores().size()==1
											&& categoria.getListaCaracteristicasTermopar().get(0).getValores().get(0)!=null && !categoria.getListaCaracteristicasTermopar().get(0).getValores().get(0).isEmpty())
										caracteristica.setValue(Double.valueOf(categoria.getListaCaracteristicasTermopar().get(0).getValores().get(0)));
									
									productoAux.getListaPedidoCaracteristica().add(caracteristica);									
								}
							}
						}						
						venta.getListaPedidos().add(productoAux);
					}						
				}
				
				respuesta = productoService.getOrderPdf(venta);
				
				if(!respuesta.isExitoso()) {
					respuesta = new ResponseService();
					respuesta.setExitoso(false);
				}				
			}
				
			return new ResponseEntity<>(respuesta, HttpStatus.OK);			
		} catch (Exception e) {	
			logger.error("/api/sale/getOrderPdf Exception-error:"+e.getMessage());
			respuesta=new ResponseService();
			respuesta.setMensaje(e.getMessage());
			return new ResponseEntity<>(respuesta, HttpStatus.CONFLICT);
		}		
	}
	
	
	@PostMapping("/api/cart/addProduct")
	public String addProduct(@ModelAttribute Producto product, Model model, HttpServletRequest request, HttpSession session) {
		logger.info("/api/cart/addProduct?product="+ product.toString());
		
		try {
						
			respuesta = productoService.getProductById((product.getPkproducto()));
			if(respuesta.isExitoso()) {				
				String jsonProducto = Utils.convertObjectAString(respuesta.getRespuesta());
				Type listType = new TypeToken<Producto>() {}.getType();
				Producto producto = new Gson().fromJson(jsonProducto, listType);
				
				if(producto!=null && producto.getListaPrecios()!=null && producto.getListaPrecios().size()>0) {
					for(Precio precio : producto.getListaPrecios()) {
						if(precio.getEstatus()>0) {
							producto.setFkPrecio(precio.getPkprecio());
							producto.setPrecioMenudeo(precio.getPrecioMenudeo());
							producto.setPrecioMayoreo(precio.getPrecioMayoreo());
							producto.setPrecioMenudeoFormato(Utils.convertCurrencyFormat(precio.getPrecioMenudeo()));
							producto.setPrecioMayoreoFormato(Utils.convertCurrencyFormat(precio.getPrecioMayoreo()));
							producto.setCantidad(product.getCantidad());
							break;
						}
					}
				}
				
				if(producto.getComprar()==0) {
					respuesta=new ResponseService();					
					return "error";
				}
				
				
				if(product.getListaCategoriaTermopar()!=null && !product.getListaCategoriaTermopar().isEmpty()) {
					String caracteristicasImagenTermopar="";
					producto.setPkproducto(product.getPkproducto());
					producto.setListaCategoriaTermopar(new ArrayList<CategoriaTermopar>());
					producto.setListaCaracteristicasTermopar(new ArrayList<CaracteristicaTermopar>());
					for(CategoriaTermopar categoria : product.getListaCategoriaTermopar()) {
						CategoriaTermopar categoriaTermopar = new CategoriaTermopar(categoria.getPkcategoriatermopar(), null, null, 0, new ArrayList<CaracteristicaTermopar>()); 									
						if(categoria.getListaCaracteristicasTermopar()!=null && !categoria.getListaCaracteristicasTermopar().isEmpty()) {										
							for(CaracteristicaTermopar caracteristica : categoria.getListaCaracteristicasTermopar()) {
								if(caracteristica.getSeleccionado()>0) {
									categoriaTermopar.getListaCaracteristicasTermopar().add(new CaracteristicaTermopar(caracteristica.getSeleccionado(), null, null, null, 0, false, caracteristica.getPkPadre(),caracteristica.getValores()));
									
									if(caracteristicasImagenTermopar.isEmpty())
										caracteristicasImagenTermopar = String.valueOf(caracteristica.getSeleccionado());
									else
										caracteristicasImagenTermopar = caracteristicasImagenTermopar + ',' + caracteristica.getSeleccionado();
									
									CaracteristicaTermopar caracteristicaAux = new CaracteristicaTermopar();
									caracteristicaAux.setPkcaracteristicatermopar(caracteristica.getSeleccionado());
									if(caracteristica.getValores()!=null && !caracteristica.getValores().isEmpty()) {
										caracteristicaAux.setValue(Double.valueOf(caracteristica.getValores().get(0)));
									}
									producto.getListaCaracteristicasTermopar().add(caracteristicaAux);
									
									break;
								}
							}								
						}	
						producto.getListaCategoriaTermopar().add(categoriaTermopar);
					}		
					
					respuesta = productoService.getImageFeatures(producto.getPkproducto(),caracteristicasImagenTermopar);
					if(respuesta.isExitoso() && respuesta.getRespuesta()!=null && !respuesta.getRespuesta().toString().isEmpty()) {
						producto.setListaProductoImagen(new ArrayList<Imagen>());
						producto.getListaProductoImagen().add(new Imagen((String)respuesta.getRespuesta()));									
					}
					
					respuesta = productoService.calculateTotalAmount(producto);
					if(respuesta.isExitoso()) {
						producto.setPrecioMenudeoFormato(Utils.convertCurrencyFormat(Double.parseDouble(String.valueOf(respuesta.getRespuesta()))));						
						producto.setPrecioMenudeo(Double.valueOf(String.valueOf(respuesta.getRespuesta())));						
					}
				}		
							
				
				Carrito carrito = (Carrito) session.getAttribute(sesionCart);
				
				if(carrito==null) 
					carrito=new Carrito();
					
				if(carrito.getProductos()==null)
					carrito.setProductos(new ArrayList<Producto>());
				
				int encontrado=-1;
				for(Producto productoAux : carrito.getProductos()) {
					if(productoAux.getPkproducto()==producto.getPkproducto()) {
						if(productoAux.getListaCategoriaTermopar()==null || productoAux.getListaCategoriaTermopar().isEmpty() ||
							producto.getListaCategoriaTermopar()==null || producto.getListaCategoriaTermopar().isEmpty()){
								encontrado = carrito.getProductos().indexOf(productoAux);
								break;
						}else {
							if(productoAux.getListaCategoriaTermopar().size()==producto.getListaCategoriaTermopar().size()) {
								ArrayList<Boolean> encontradaCaracteristica = new ArrayList<Boolean>();
								for(int i=0;i<producto.getListaCategoriaTermopar().size();i++) {
									if(productoAux.getListaCategoriaTermopar().get(i).getPkcategoriatermopar()==producto.getListaCategoriaTermopar().get(i).getPkcategoriatermopar()) {
										if((productoAux.getListaCategoriaTermopar().get(i).getListaCaracteristicasTermopar()!=null && producto.getListaCategoriaTermopar().get(i).getListaCaracteristicasTermopar()!=null)
									    && (!productoAux.getListaCategoriaTermopar().get(i).getListaCaracteristicasTermopar().isEmpty() && !producto.getListaCategoriaTermopar().get(i).getListaCaracteristicasTermopar().isEmpty())
										&& (productoAux.getListaCategoriaTermopar().get(i).getListaCaracteristicasTermopar().get(0).getPkcaracteristicatermopar()==producto.getListaCategoriaTermopar().get(i).getListaCaracteristicasTermopar().get(0).getPkcaracteristicatermopar())) {											
											if((productoAux.getListaCategoriaTermopar().get(i).getListaCaracteristicasTermopar().get(0).getValores()==null || productoAux.getListaCategoriaTermopar().get(i).getListaCaracteristicasTermopar().get(0).getValores().isEmpty()
											&& productoAux.getListaCategoriaTermopar().get(i).getListaCaracteristicasTermopar().get(0).getValores().isEmpty() || producto.getListaCategoriaTermopar().get(i).getListaCaracteristicasTermopar().get(0).getValores().isEmpty())) {
												encontradaCaracteristica.add(true);
											}else {
												if(productoAux.getListaCategoriaTermopar().get(i).getListaCaracteristicasTermopar().get(0).getValores().get(0).trim().equalsIgnoreCase(producto.getListaCategoriaTermopar().get(i).getListaCaracteristicasTermopar().get(0).getValores().get(0).trim())) {
													encontradaCaracteristica.add(true);
												}else {
													encontradaCaracteristica.add(false);
													break;
												}
											}
										}else {
											encontradaCaracteristica.add(false);
											break;
										}
									}else {
										encontradaCaracteristica.add(false);
										break;
									}
								}
								
								
								boolean caracteristicasIguales = true;
								for(Boolean encontradoBooleanAux : encontradaCaracteristica) {
									if (!encontradoBooleanAux) {
										caracteristicasIguales=false;
										break;
									}										
								}
								
								if(caracteristicasIguales) {
									encontrado = carrito.getProductos().indexOf(productoAux);
									break;
								}									
							}
						}						
					} 
					
				}
				
				if(encontrado>-1) {
					Producto productoAux = carrito.getProductos().get(encontrado);
					productoAux.setCantidad(productoAux.getCantidad()+product.getCantidad());
					carrito.getProductos().set(encontrado, productoAux);
				}else
					carrito.getProductos().add(producto);
				
				carrito = Utils.getCart(carrito,taxIva);
				request.getSession().setAttribute(sesionCart, carrito);
				model.addAttribute("carrito", carrito);
				
				return "cabeceraInferior"; 	
			}
			return "error";
			
		} catch (Exception e) {	
			logger.error("/api/cart/addProduct Exception-error:"+e.getMessage());
			respuesta=new ResponseService();
			respuesta.setMensaje(e.getMessage());
			return "error";
		}	
	}
	
	
	@GetMapping("/api/product/getImageFeaturesTermopar")
	public ResponseEntity<ResponseService> getImageFeaturesTermopar(@RequestParam int pkProducto, @RequestParam String pkCaracteristicas) {
		logger.info("/api/product/getImageFeaturesTermopar?pkProducto="+ pkProducto+"&pkCaracteristicas="+pkCaracteristicas);
		try {
			
			respuesta = productoService.getImageFeatures(pkProducto,pkCaracteristicas);
			if(!respuesta.isExitoso()) {
				respuesta = new ResponseService();
				respuesta.setExitoso(false);				
			}
			
			respuesta.setCodigo("true");
			String[] pkCaracteristicasAux = pkCaracteristicas.toString().split(",");
			for(String pkCaracteristicaAux : pkCaracteristicasAux) {
				ResponseService respuestaAux = productoService.getFeaturesById(Integer.parseInt(pkCaracteristicaAux));
				if(respuestaAux.isExitoso()) {
					String jsonProducto = Utils.convertObjectAString(respuestaAux.getRespuesta());
					Type listType = new TypeToken<CaracteristicaTermopar>() {}.getType();
					CaracteristicaTermopar caracteristicaTermoparAux = new Gson().fromJson(jsonProducto, listType);
					for(PrecioCaracteristicaTermopar precioAux : caracteristicaTermoparAux.getListaPrecio()) {
						if(precioAux.getEstatus()>0) {
							if(precioAux.getPreciomenudeo()<0) {
								respuesta.setCodigo("false");
								break;
							}							
						}
					}
				}
			}
			
			
			
				
			return new ResponseEntity<>(respuesta, HttpStatus.OK);			
		} catch (Exception e) {	
			logger.error("/api/product/getImageFeaturesTermopar Exception-error:"+e.getMessage());
			respuesta=new ResponseService();
			respuesta.setMensaje(e.getMessage());
			return new ResponseEntity<>(respuesta, HttpStatus.CONFLICT);
		}				
	}
	
	
	@GetMapping("/api/cart/removeProduct")
	public String removeProduct(@RequestParam int product, @RequestParam int amount, @RequestParam int remove, Model model, HttpServletRequest request, HttpSession session) {
		logger.info("/api/cart/removeProduct?product="+ product+"&amount="+amount+"&remove="+remove);
		
		try {
						
			Carrito carrito = (Carrito) session.getAttribute(sesionCart);
			
			if(amount<=0)
				amount=1;
			
			for(int i=0;i<carrito.getProductos().size();i++) {
				if(product==i) {
					if(remove>0)
						carrito.getProductos().remove(i);
					else 
						carrito.getProductos().get(i).setCantidad(amount);
												
					break;
				}
			}
			
			carrito = Utils.getCart(carrito,taxIva);
			request.getSession().setAttribute(sesionCart, carrito);
			model.addAttribute("carrito", carrito);
			
			return "cabeceraInferior"; 	
			
			
		} catch (Exception e) {	
			logger.error("/api/cart/addProduct Exception-error:"+e.getMessage());
			respuesta=new ResponseService();
			respuesta.setMensaje(e.getMessage());
			return "error";
		}	
	}
	
	
	@PostMapping("/api/product/getFeaturesTermopar")
	public String getFeaturesTermopar(@ModelAttribute Producto product, Model model, HttpServletRequest request, HttpSession session) {
		logger.info("/api/cart/getFeaturesTermopar?product="+ product.toString());
		
		try {
						
			if(product!=null && product.getPkproducto()>0) {
				if(product.getListaCategoriaTermopar()!=null && !product.getListaCategoriaTermopar().isEmpty()) {
					int categoriaSeleccionada=0;
					int caracteristicaSeleccionada=0, pkPadreSeleccionado=0;
					for(int i=0;i<product.getListaCategoriaTermopar().size();i++) {
						if(product.getListaCategoriaTermopar().get(i).getListaCaracteristicasTermopar()!=null && product.getListaCategoriaTermopar().get(i).getListaCaracteristicasTermopar().size()>0) {
							for(int j=0;j<product.getListaCategoriaTermopar().get(i).getListaCaracteristicasTermopar().size();j++) {
								if(product.getListaCategoriaTermopar().get(i).getListaCaracteristicasTermopar().get(j).isModificado()) {
									categoriaSeleccionada=i;
									caracteristicaSeleccionada=product.getListaCategoriaTermopar().get(i).getListaCaracteristicasTermopar().get(j).getPkcaracteristicatermopar();
									pkPadreSeleccionado=product.getListaCategoriaTermopar().get(i).getListaCaracteristicasTermopar().get(j).getPkPadre();
									break;
								}									
							}									
						}			
						if(caracteristicaSeleccionada!=0)
							break;
					}
					
					if(categoriaSeleccionada!=product.getListaCategoriaTermopar().size() && pkPadreSeleccionado!=0) {
						respuesta = productoService.getFeaturesById(product.getPkproducto(),pkPadreSeleccionado);
						if(respuesta.isExitoso() && respuesta.getRespuesta()!=null) {
							String jsonProducto = Utils.convertObjectAString(respuesta.getRespuesta());
							Type listType = new TypeToken<List<CaracteristicaTermopar>>() {}.getType();
							List<CaracteristicaTermopar> listaCaracteristicasTermopar = new Gson().fromJson(jsonProducto, listType);
							
							if(listaCaracteristicasTermopar!=null && !listaCaracteristicasTermopar.isEmpty()) 
								product.getListaCategoriaTermopar().get(categoriaSeleccionada+1).setListaCaracteristicasTermopar(listaCaracteristicasTermopar);
								
							
							for(int i=product.getListaCategoriaTermopar().size()-1;i>categoriaSeleccionada+1;i--) {
								if(product.getListaCategoriaTermopar().get(i).getListaCaracteristicasTermopar()!=null && product.getListaCategoriaTermopar().get(i).getListaCaracteristicasTermopar().size()>0) {									
									product.getListaCategoriaTermopar().get(i).setListaCaracteristicasTermopar(null);									
								}
							}

							model.addAttribute("producto", product);
						}			
					}
				}
			}
			
			return "seccionCaracteristicasTermopar"; 	
			
		} catch (Exception e) {	
			logger.error("/api/cart/getFeaturesTermopar Exception-error:"+e.getMessage());
			respuesta=new ResponseService();
			respuesta.setMensaje(e.getMessage());
			return "error";
		}	
	}
	
	
	@PostMapping("/api/shipping/quotation")
	public String getQuotation(@ModelAttribute Venta sale, Model model, HttpServletRequest request, HttpSession session) {
		logger.info("/api/shipping/quotation?sale="+ sale.toString());
		
		try {
			
			if(sale==null || sale.getCliente()==null || sale.getCliente().getNombres()==null || sale.getCliente().getNombres().isEmpty()) throw new Exception("Bad Request"); 
			if(sale==null || sale.getCliente()==null || sale.getCliente().getApellidos()==null || sale.getCliente().getApellidos().isEmpty()) throw new Exception("Bad Request"); 
			if(sale==null || sale.getCliente()==null || sale.getCliente().getCorreo()==null || sale.getCliente().getCorreo().isEmpty()) throw new Exception("Bad Request"); 
			if(sale==null || sale.getCliente()==null || sale.getCliente().getTelefono()==null || sale.getCliente().getTelefono().isEmpty()) throw new Exception("Bad Request"); 
			if(sale==null || sale.getCliente()==null || sale.getCliente().getListaDomicilios()==null || sale.getCliente().getListaDomicilios().isEmpty() || sale.getCliente().getListaDomicilios().get(0).getCalle()==null || sale.getCliente().getListaDomicilios().get(0).getCalle().isEmpty()) throw new Exception("Bad Request"); 			 
			if(sale==null || sale.getCliente()==null || sale.getCliente().getListaDomicilios()==null || sale.getCliente().getListaDomicilios().isEmpty() || sale.getCliente().getListaDomicilios().get(0).getCodigoPostal()==null || sale.getCliente().getListaDomicilios().get(0).getCodigoPostal().isEmpty()) throw new Exception("Bad Request"); 
			if(sale==null || sale.getCliente()==null || sale.getCliente().getListaDomicilios()==null || sale.getCliente().getListaDomicilios().isEmpty() || sale.getCliente().getListaDomicilios().get(0).getColonia()==null || sale.getCliente().getListaDomicilios().get(0).getColonia().isEmpty()) throw new Exception("Bad Request"); 
			if(sale==null || sale.getCliente()==null || sale.getCliente().getListaDomicilios()==null || sale.getCliente().getListaDomicilios().isEmpty() || sale.getCliente().getListaDomicilios().get(0).getDelegacion()==null || sale.getCliente().getListaDomicilios().get(0).getDelegacion().isEmpty()) throw new Exception("Bad Request"); 
			if(sale==null || sale.getCliente()==null || sale.getCliente().getListaDomicilios()==null || sale.getCliente().getListaDomicilios().isEmpty() || sale.getCliente().getListaDomicilios().get(0).getEstado()==null || sale.getCliente().getListaDomicilios().get(0).getEstado().isEmpty()) throw new Exception("Bad Request"); 
			if(sale==null || sale.getCliente()==null || sale.getCliente().getListaDomicilios()==null || sale.getCliente().getListaDomicilios().isEmpty() || sale.getCliente().getListaDomicilios().get(0).getNoExterior()==null || sale.getCliente().getListaDomicilios().get(0).getNoExterior().isEmpty()) throw new Exception("Bad Request"); 
			if(sale==null || sale.getCliente()==null || sale.getCliente().getListaDomicilios()==null || sale.getCliente().getListaDomicilios().isEmpty() || sale.getCliente().getListaDomicilios().get(0).getNoInterior()==null || sale.getCliente().getListaDomicilios().get(0).getNoInterior().isEmpty()) sale.getCliente().getListaDomicilios().get(0).setNoInterior("none");
			
			 
			CotizacionEnvio shipments = new CotizacionEnvio();			
			shipments.setApellidos(sale.getCliente().getApellidos());
			shipments.setNombres(sale.getCliente().getNombres());
			shipments.setCorreo(sale.getCliente().getCorreo());
			shipments.setTelefono(sale.getCliente().getTelefono());
			shipments.setCalle(sale.getCliente().getListaDomicilios().get(0).getCalle());
			shipments.setCodigopostal(sale.getCliente().getListaDomicilios().get(0).getCodigoPostal());
			shipments.setColonia(sale.getCliente().getListaDomicilios().get(0).getColonia());
			shipments.setDelegacion(sale.getCliente().getListaDomicilios().get(0).getDelegacion());
			shipments.setEstado(sale.getCliente().getListaDomicilios().get(0).getEstado());
			shipments.setNoexterior(sale.getCliente().getListaDomicilios().get(0).getNoExterior());
			shipments.setNointerior(sale.getCliente().getListaDomicilios().get(0).getNoInterior());
			
			Carrito carrito = (Carrito) session.getAttribute(sesionCart);
			
			if(carrito==null || carrito.getProductos()==null || carrito.getProductos().isEmpty()) throw new Exception("Bad Request");
			
			shipments.setListaPedidos(new ArrayList<Producto>());
			for(Producto producto : carrito.getProductos()) {
				if(producto!=null) {
					Producto productoAux = new Producto();
					productoAux.setFkProducto(producto.getPkproducto());
					productoAux.setCantidad(producto.getCantidad());
					
					if(producto.getListaCategoriaTermopar()!=null && !producto.getListaCategoriaTermopar().isEmpty()) {
						productoAux.setListaPedidoCaracteristica(new ArrayList<CaracteristicaTermopar>());
						for(CategoriaTermopar categoriaTermoparAux : producto.getListaCategoriaTermopar()) {
							if(categoriaTermoparAux.getListaCaracteristicasTermopar()!=null && !categoriaTermoparAux.getListaCaracteristicasTermopar().isEmpty()) {
								CaracteristicaTermopar caracteristicaAux = new CaracteristicaTermopar();
								caracteristicaAux.setFkCaracteristicaTermopar(categoriaTermoparAux.getListaCaracteristicasTermopar().get(0).getPkcaracteristicatermopar());
								if(categoriaTermoparAux.getListaCaracteristicasTermopar().get(0).getValores()!=null && !categoriaTermoparAux.getListaCaracteristicasTermopar().get(0).getValores().isEmpty()) {
									if( categoriaTermoparAux.getListaCaracteristicasTermopar().get(0).getValores().size()==3
											&& categoriaTermoparAux.getListaCaracteristicasTermopar().get(0).getValores().get(0)!=null && !categoriaTermoparAux.getListaCaracteristicasTermopar().get(0).getValores().get(0).isEmpty()
											&& categoriaTermoparAux.getListaCaracteristicasTermopar().get(0).getValores().get(1)!=null && !categoriaTermoparAux.getListaCaracteristicasTermopar().get(0).getValores().get(1).isEmpty() 
											&& categoriaTermoparAux.getListaCaracteristicasTermopar().get(0).getValores().get(2)!=null && !categoriaTermoparAux.getListaCaracteristicasTermopar().get(0).getValores().get(2).isEmpty()) {
										Double total = Double.valueOf(categoriaTermoparAux.getListaCaracteristicasTermopar().get(0).getValores().get(1))/Double.valueOf(categoriaTermoparAux.getListaCaracteristicasTermopar().get(0).getValores().get(2));
										total = total + Double.valueOf(categoriaTermoparAux.getListaCaracteristicasTermopar().get(0).getValores().get(0));
										caracteristicaAux.setValue(total);
									}else if(categoriaTermoparAux.getListaCaracteristicasTermopar().get(0).getValores().size()==1 && categoriaTermoparAux.getListaCaracteristicasTermopar().get(0).getValores().get(0)!=null && !categoriaTermoparAux.getListaCaracteristicasTermopar().get(0).getValores().get(0).isEmpty())
										caracteristicaAux.setValue(Double.valueOf(categoriaTermoparAux.getListaCaracteristicasTermopar().get(0).getValores().get(0)));
								}
								productoAux.getListaPedidoCaracteristica().add(caracteristicaAux);
							}
						}
						
					}
					
					shipments.getListaPedidos().add(productoAux);
				}
			}
						
			respuesta = productoService.getShipments(shipments);
			if(respuesta.isExitoso()) {				
				String jsonProducto = Utils.convertObjectAString(respuesta.getRespuesta());
				Type listType = new TypeToken<List<Paquete>>() {}.getType();
				List<Paquete> listaPaquetes = new Gson().fromJson(jsonProducto, listType);
				model.addAttribute("listaPaquetes", listaPaquetes);
			} else
				throw new Exception("Bad Request");
							
			return "seccionListaPaqueterias"; 	
			
		} catch (Exception e) {	
			logger.error("/api/shipping/quotation Exception-error:"+e.getMessage());
			respuesta=new ResponseService();
			respuesta.setMensaje(e.getMessage());
			return "error";
		}	
	}
	
}
