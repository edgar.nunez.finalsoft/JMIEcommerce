package com.mx.jmi.ecommerceJMI;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.PropertySource;

@SpringBootApplication
public class EcommerceJmiApplication {

	public static void main(String[] args) {
		SpringApplication.run(EcommerceJmiApplication.class, args);
		
		System.out.print("Init Ecommerce Application");
	}
	
	

}
