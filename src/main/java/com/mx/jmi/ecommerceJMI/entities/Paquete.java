package com.mx.jmi.ecommerceJMI.entities;

public class Paquete {

	private String id;
	private String idEnvio;
	private Integer idProveedor;
	private String proveedor;
	private String servicio;
	private String dias;
	private String monto;

	
	public Paquete() {
		super();
	}

	public Paquete(String id, Integer idProveedor, String proveedor, String servicio, String dias, String monto) {
		super();
		this.id = id;
		this.idProveedor = idProveedor;
		this.proveedor = proveedor;
		this.servicio = servicio;
		this.dias = dias;
		this.monto = monto;
	}

	public Integer getIdProveedor() {
		return idProveedor;
	}

	public String getIdEnvio() {
		return idEnvio;
	}

	public void setIdEnvio(String idEnvio) {
		this.idEnvio = idEnvio;
	}

	public void setIdProveedor(Integer idProveedor) {
		this.idProveedor = idProveedor;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getProveedor() {
		return proveedor;
	}

	public void setProveedor(String proveedor) {
		this.proveedor = proveedor;
	}

	public String getServicio() {
		return servicio;
	}

	public void setServicio(String servicio) {
		this.servicio = servicio;
	}

	public String getDias() {
		return dias;
	}

	public void setDias(String dias) {
		this.dias = dias;
	}

	public String getMonto() {
		return monto;
	}

	public void setMonto(String monto) {
		this.monto = monto;
	}

}
