package com.mx.jmi.ecommerceJMI.entities;

import java.util.List;

public class CotizacionEnvio {

	private String codigopostal;
	private String estado;
	private String delegacion;
	private String colonia;
	private String calle;
	private String noexterior;
	private String nointerior;
	private String correo;
	private String telefono;
	private String nombres;
	private String apellidos;
	private List<Producto> listaPedidos;

	public String getCodigopostal() {
		return codigopostal;
	}

	public void setCodigopostal(String codigopostal) {
		this.codigopostal = codigopostal;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getDelegacion() {
		return delegacion;
	}

	public void setDelegacion(String delegacion) {
		this.delegacion = delegacion;
	}

	public String getColonia() {
		return colonia;
	}

	public void setColonia(String colonia) {
		this.colonia = colonia;
	}

	public String getCalle() {
		return calle;
	}

	public void setCalle(String calle) {
		this.calle = calle;
	}

	public String getNoexterior() {
		return noexterior;
	}

	public void setNoexterior(String noexterior) {
		this.noexterior = noexterior;
	}

	public String getNointerior() {
		return nointerior;
	}

	public void setNointerior(String nointerior) {
		this.nointerior = nointerior;
	}

	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getNombres() {
		return nombres;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public List<Producto> getListaPedidos() {
		return listaPedidos;
	}

	public void setListaPedidos(List<Producto> listaPedidos) {
		this.listaPedidos = listaPedidos;
	}

}
