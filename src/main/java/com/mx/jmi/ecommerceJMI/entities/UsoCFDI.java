package com.mx.jmi.ecommerceJMI.entities;

public class UsoCFDI {

	private int pkUsoCFDI;
	private String clave;
	private String nombre;

	public int getPkUsoCFDI() {
		return pkUsoCFDI;
	}

	public void setPkUsoCFDI(int pkUsoCFDI) {
		this.pkUsoCFDI = pkUsoCFDI;
	}

	public String getClave() {
		return clave;
	}

	public void setClave(String clave) {
		this.clave = clave;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	@Override
	public String toString() {
		return "UsoCFDI [pkUsoCFDI=" + pkUsoCFDI + ", clave=" + clave + ", nombre=" + nombre + "]";
	}

}
