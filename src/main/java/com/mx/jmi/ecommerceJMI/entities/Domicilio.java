package com.mx.jmi.ecommerceJMI.entities;

import java.io.Serializable;

public class Domicilio implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1L;
	private int pkdomicilio;
	private String codigoPostal;
	private String pais;
	private String estado;
	private String delegacion;
	private String colonia;
	private String ciudad;
	private String calle;
	private String noExterior;
	private String noInterior;
	private String referencia;
	private String pkEnvio;
	private int estatus;
	private int index;

	
	public String getPkEnvio() {
		return pkEnvio;
	}

	public void setPkEnvio(String pkEnvio) {
		this.pkEnvio = pkEnvio;
	}

	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}

	public int getPkdomicilio() {
		return pkdomicilio;
	}

	public void setPkdomicilio(int pkdomicilio) {
		this.pkdomicilio = pkdomicilio;
	}

	public String getCodigoPostal() {
		return codigoPostal;
	}

	public void setCodigoPostal(String codigoPostal) {
		this.codigoPostal = codigoPostal;
	}

	public String getPais() {
		return pais;
	}

	public void setPais(String pais) {
		this.pais = pais;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getDelegacion() {
		return delegacion;
	}

	public void setDelegacion(String delegacion) {
		this.delegacion = delegacion;
	}

	public String getColonia() {
		return colonia;
	}

	public void setColonia(String colonia) {
		this.colonia = colonia;
	}

	public String getCiudad() {
		return ciudad;
	}

	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}

	public String getCalle() {
		return calle;
	}

	public void setCalle(String calle) {
		this.calle = calle;
	}

	public String getNoExterior() {
		return noExterior;
	}

	public void setNoExterior(String noExterior) {
		this.noExterior = noExterior;
	}

	public String getNoInterior() {
		return noInterior;
	}

	public void setNoInterior(String noInterior) {
		this.noInterior = noInterior;
	}

	public String getReferencia() {
		return referencia;
	}

	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}

	public int getEstatus() {
		return estatus;
	}

	public void setEstatus(int estatus) {
		this.estatus = estatus;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return "Domicilio [pkdomicilio=" + pkdomicilio + ", codigoPostal=" + codigoPostal + ", pais=" + pais
				+ ", estado=" + estado + ", delegacion=" + delegacion + ", colonia=" + colonia + ", ciudad=" + ciudad
				+ ", calle=" + calle + ", noExterior=" + noExterior + ", noInterior=" + noInterior + ", referencia="
				+ referencia + ", estatus=" + estatus + "]";
	}

}
