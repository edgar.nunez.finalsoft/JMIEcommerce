
package com.mx.jmi.ecommerceJMI.entities;

import java.io.Serializable;

public class Subcategoria implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int pkSubCategoria;
	private String nombre;
	private String descripcion;
	private String imagen;
	private int estatus;
	private int fkCategoria;
	private Categoria categoria;

	
	
	public int getFkCategoria() {
		return fkCategoria;
	}

	public void setFkCategoria(int fkCategoria) {
		this.fkCategoria = fkCategoria;
	}

	public Categoria getCategoria() {
		return categoria;
	}

	public void setCategoria(Categoria categoria) {
		this.categoria = categoria;
	}

	public int getPkSubCategoria() {
		return pkSubCategoria;
	}

	public void setPkSubCategoria(int pkSubCategoria) {
		this.pkSubCategoria = pkSubCategoria;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getImagen() {
		return imagen;
	}

	public void setImagen(String imagen) {
		this.imagen = imagen;
	}

	public int getEstatus() {
		return estatus;
	}

	public void setEstatus(int estatus) {
		this.estatus = estatus;
	}

}
