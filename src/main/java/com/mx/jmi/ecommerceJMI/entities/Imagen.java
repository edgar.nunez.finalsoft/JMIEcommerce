package com.mx.jmi.ecommerceJMI.entities;

import java.io.Serializable;

public class Imagen implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String url;

	public Imagen() {
		super();
	}

	public Imagen(String url) {
		super();
		this.url = url;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	@Override
	public String toString() {
		return "Imagen [url=" + url + "]";
	}

}
