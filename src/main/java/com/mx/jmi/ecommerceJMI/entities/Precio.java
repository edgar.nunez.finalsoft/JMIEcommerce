package com.mx.jmi.ecommerceJMI.entities;

import java.io.Serializable;

public class Precio implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1L;
	private int pkprecio;
	private Double precioMenudeo;
	private Double precioMayoreo;
	private int estatus;
	private int fkproducto;

	public int getPkprecio() {
		return pkprecio;
	}

	public void setPkprecio(int pkprecio) {
		this.pkprecio = pkprecio;
	}

	

	public Double getPrecioMenudeo() {
		return precioMenudeo;
	}

	public void setPrecioMenudeo(Double precioMenudeo) {
		this.precioMenudeo = precioMenudeo;
	}

	public Double getPrecioMayoreo() {
		return precioMayoreo;
	}

	public void setPrecioMayoreo(Double precioMayoreo) {
		this.precioMayoreo = precioMayoreo;
	}

	public int getEstatus() {
		return estatus;
	}

	public void setEstatus(int estatus) {
		this.estatus = estatus;
	}

	public int getFkproducto() {
		return fkproducto;
	}

	public void setFk_producto(int fkproducto) {
		this.fkproducto = fkproducto;
	}

}
