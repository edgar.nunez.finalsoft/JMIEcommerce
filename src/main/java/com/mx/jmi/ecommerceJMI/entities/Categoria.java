package com.mx.jmi.ecommerceJMI.entities;

import java.io.Serializable;

import com.google.gson.annotations.SerializedName;

public class Categoria {
	@SerializedName("pkCategoria")
	private int pkcategoria; 
	private String nombre;
	private String descripcion;
	private String imagen;
	private int estatus;
	private Subcategoria subcategorias;

	
	

	public Categoria(int pkCategoria, String nombre, String descripcion, String imagen, int estatus,
			Subcategoria subcategorias) {
		super();
		this.pkcategoria = pkCategoria;
		this.nombre = nombre;
		this.descripcion = descripcion;
		this.imagen = imagen;
		this.estatus = estatus;
		this.subcategorias = subcategorias;
	}

	public Subcategoria getSubcategorias() {
		return subcategorias;
	}

	public void setSubcategorias(Subcategoria subcategorias) {
		this.subcategorias = subcategorias;
	}

	public int getPkcategoria() {
		return pkcategoria;
	}

	public void setPkcategoria(int pkcategoria) {
		this.pkcategoria = pkcategoria;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getImagen() {
		return imagen;
	}

	public void setImagen(String imagen) {
		this.imagen = imagen;
	}

	public int getEstatus() {
		return estatus;
	}

	public void setEstatus(int estatus) {
		this.estatus = estatus;
	}

}
