package com.mx.jmi.ecommerceJMI.entities;

import java.io.Serializable;
import java.util.List;

public class Producto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int fkProducto;;
	private int pkproducto;
	private int idAspelProducto;
	private String nombre;
	private String descripcion;
	private String imagen;
	private Double precioMenudeo;
	private Double precioMayoreo;
	private int comprar;
	private int estatus;
	private Subcategoria subcategoria;
	private List<Precio> listaPrecios;
	private int cantidad;
	private int fkPrecio;
	private List<CategoriaTermopar> listaCategoriaTermopar;
	private String total;
	private String precioMenudeoFormato;
	private String precioMayoreoFormato;
	private List<Imagen> listaProductoImagen;
	private List<Archivo> listaArchivos;
	private List<CaracteristicaTermopar> listaPedidoCaracteristica;
	private List<CaracteristicaTermopar> listaCaracteristicasTermopar;

	public List<CaracteristicaTermopar> getListaCaracteristicasTermopar() {
		return listaCaracteristicasTermopar;
	}

	public void setListaCaracteristicasTermopar(List<CaracteristicaTermopar> listaCaracteristicasTermopar) {
		this.listaCaracteristicasTermopar = listaCaracteristicasTermopar;
	}

	public List<CaracteristicaTermopar> getListaPedidoCaracteristica() {
		return listaPedidoCaracteristica;
	}

	public void setListaPedidoCaracteristica(List<CaracteristicaTermopar> listaPedidoCaracteristica) {
		this.listaPedidoCaracteristica = listaPedidoCaracteristica;
	}

	public int getFkProducto() {
		return fkProducto;
	}

	public void setFkProducto(int fkProducto) {
		this.fkProducto = fkProducto;
	}

	public List<Archivo> getListaArchivos() {
		return listaArchivos;
	}

	public void setListaArchivos(List<Archivo> listaArchivos) {
		this.listaArchivos = listaArchivos;
	}

	public List<Imagen> getListaProductoImagen() {
		return listaProductoImagen;
	}

	public void setListaProductoImagen(List<Imagen> listaProductoImagen) {
		this.listaProductoImagen = listaProductoImagen;
	}

	public String getPrecioMenudeoFormato() {
		return precioMenudeoFormato;
	}

	public void setPrecioMenudeoFormato(String precioMenudeoFormato) {
		this.precioMenudeoFormato = precioMenudeoFormato;
	}

	public String getPrecioMayoreoFormato() {
		return precioMayoreoFormato;
	}

	public void setPrecioMayoreoFormato(String precioMayoreoFormato) {
		this.precioMayoreoFormato = precioMayoreoFormato;
	}

	public String getTotal() {
		return total;
	}

	public void setTotal(String total) {
		this.total = total;
	}

	public List<CategoriaTermopar> getListaCategoriaTermopar() {
		return listaCategoriaTermopar;
	}

	public void setListaCategoriaTermopar(List<CategoriaTermopar> listaCategoriaTermopar) {
		this.listaCategoriaTermopar = listaCategoriaTermopar;
	}

	public int getFkPrecio() {
		return fkPrecio;
	}

	public void setFkPrecio(int fkPrecio) {
		this.fkPrecio = fkPrecio;
	}

	public int getCantidad() {
		return cantidad;
	}

	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}

	public List<Precio> getListaPrecios() {
		return listaPrecios;
	}

	public void setListaPrecios(List<Precio> listaPrecios) {
		this.listaPrecios = listaPrecios;
	}

	public Double getPrecioMenudeo() {
		return precioMenudeo;
	}

	public void setPrecioMenudeo(Double precioMenudeo) {
		this.precioMenudeo = precioMenudeo;
	}

	public Double getPrecioMayoreo() {
		return precioMayoreo;
	}

	public void setPrecioMayoreo(Double precioMayoreo) {
		this.precioMayoreo = precioMayoreo;
	}

	public int getPkproducto() {
		return pkproducto;
	}

	public void setPkproducto(int pkproducto) {
		this.pkproducto = pkproducto;
	}

	public int getIdAspelProducto() {
		return idAspelProducto;
	}

	public void setIdAspelProducto(int idAspelProducto) {
		this.idAspelProducto = idAspelProducto;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getImagen() {
		return imagen;
	}

	public void setImagen(String imagen) {
		this.imagen = imagen;
	}

	public int getEstatus() {
		return estatus;
	}

	public void setEstatus(int estatus) {
		this.estatus = estatus;
	}

	public Subcategoria getSubcategoria() {
		return subcategoria;
	}

	public void setSubcategoria(Subcategoria subcategoria) {
		this.subcategoria = subcategoria;
	}

	public int getComprar() {
		return comprar;
	}

	public void setComprar(int comprar) {
		this.comprar = comprar;
	}

	@Override
	public String toString() {
		return "Producto [pkproducto=" + pkproducto + ", idAspelProducto=" + idAspelProducto + ", nombre=" + nombre
				+ ", descripcion=" + descripcion + ", imagen=" + imagen + ", precioMenudeo=" + precioMenudeo
				+ ", precioMayoreo=" + precioMayoreo + ", estatus=" + estatus + ", subcategoria=" + subcategoria
				+ ", listaPrecios=" + listaPrecios + ", cantidad=" + cantidad + ", fkPrecio=" + fkPrecio
				+ ", listaCategoriaTermopar=" + listaCategoriaTermopar + ", total=" + total + ", precioMenudeoFormato="
				+ precioMenudeoFormato + ", precioMayoreoFormato=" + precioMayoreoFormato + ", listaProductoImagen="
				+ listaProductoImagen + "]";
	}

}
