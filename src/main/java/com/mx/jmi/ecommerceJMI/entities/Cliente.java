package com.mx.jmi.ecommerceJMI.entities;

import java.io.Serializable;
import java.util.List;

public class Cliente implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int pkcliente;
	private int fkCliente;
	private int idAspelCliente;
	private String nombres;
	private String apellidos;
	private String correo;
	private String telefono;
	private int estatus;
	private List<Domicilio> listaDomicilios;
	private List<DatoFiscal> listaDomiciliosFiscales;
	private List<DatoFiscal> listaDatosFiscales;
	private List<Sectores> listaSectores;
	private List<UsoCFDI> listaUsoCFDI;
	private String sector;
	private String rfc;
	private boolean autenticado;

	public Cliente() {
		super();
	}

	public int getFkCliente() {
		return fkCliente;
	}

	public void setFkCliente(int fkCliente) {
		this.fkCliente = fkCliente;
	}

	public List<DatoFiscal> getListaDatosFiscales() {
		return listaDatosFiscales;
	}

	public void setListaDatosFiscales(List<DatoFiscal> listaDatosFiscales) {
		this.listaDatosFiscales = listaDatosFiscales;
	}

	public String getRfc() {
		return rfc;
	}

	public void setRfc(String rfc) {
		this.rfc = rfc;
	}

	public List<UsoCFDI> getListaUsoCFDI() {
		return listaUsoCFDI;
	}

	public void setListaUsoCFDI(List<UsoCFDI> listaUsoCFDI) {
		this.listaUsoCFDI = listaUsoCFDI;
	}

	public String getSector() {
		return sector;
	}

	public void setSector(String sector) {
		this.sector = sector;
	}

	public List<Sectores> getListaSectores() {
		return listaSectores;
	}

	public void setListaSectores(List<Sectores> listaSectores) {
		this.listaSectores = listaSectores;
	}

	public boolean isAutenticado() {
		return autenticado;
	}

	public void setAutenticado(boolean autenticado) {
		this.autenticado = autenticado;
	}

	public int getPkcliente() {
		return pkcliente;
	}

	public void setPkcliente(int pkcliente) {
		this.pkcliente = pkcliente;
	}

	public int getIdAspelCliente() {
		return idAspelCliente;
	}

	public void setIdAspelCliente(int idAspelCliente) {
		this.idAspelCliente = idAspelCliente;
	}

	public String getNombres() {
		return nombres;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public int getEstatus() {
		return estatus;
	}

	public void setEstatus(int estatus) {
		this.estatus = estatus;
	}

	public List<Domicilio> getListaDomicilios() {
		return listaDomicilios;
	}

	public void setListaDomicilios(List<Domicilio> listaDomicilios) {
		this.listaDomicilios = listaDomicilios;
	}

	public List<DatoFiscal> getListaDomiciliosFiscales() {
		return listaDomiciliosFiscales;
	}

	public void setListaDomiciliosFiscales(List<DatoFiscal> listaDomiciliosFiscales) {
		this.listaDomiciliosFiscales = listaDomiciliosFiscales;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Cliente(int pkcliente, int idAspelCliente, String nombres, String apellidos, String correo, String telefono,
			int estatus, List<Domicilio> listaDomicilios, List<DatoFiscal> listaDomiciliosFiscales,
			List<Sectores> listaSectores, List<UsoCFDI> listaUsoCFDI, String sector, String rfc, boolean autenticado) {
		super();
		this.pkcliente = pkcliente;
		this.idAspelCliente = idAspelCliente;
		this.nombres = nombres;
		this.apellidos = apellidos;
		this.correo = correo;
		this.telefono = telefono;
		this.estatus = estatus;
		this.listaDomicilios = listaDomicilios;
		this.listaDomiciliosFiscales = listaDomiciliosFiscales;
		this.listaSectores = listaSectores;
		this.listaUsoCFDI = listaUsoCFDI;
		this.sector = sector;
		this.rfc = rfc;
		this.autenticado = autenticado;
	}

	

}
