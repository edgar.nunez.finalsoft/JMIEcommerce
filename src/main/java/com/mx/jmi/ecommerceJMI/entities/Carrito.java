package com.mx.jmi.ecommerceJMI.entities;

import java.util.List;

public class Carrito {

	private List<Producto> productos;
	private String total;
	private String totalEnvio;
	private String subtotal;
	private String iva;
	private boolean pagado;
	
	
	public boolean isPagado() {
		return pagado;
	}

	public void setPagado(boolean pagado) {
		this.pagado = pagado;
	}

	public String getTotalEnvio() {
		return totalEnvio;
	}

	public void setTotalEnvio(String totalEnvio) {
		this.totalEnvio = totalEnvio;
	}

	public String getIva() {
		return iva;
	}

	public void setIva(String iva) {
		this.iva = iva;
	}

	public List<Producto> getProductos() {
		return productos;
	}

	public void setProductos(List<Producto> productos) {
		this.productos = productos;
	}

	public String getTotal() {
		return total;
	}

	public void setTotal(String total) {
		this.total = total;
	}

	public String getSubtotal() {
		return subtotal;
	}

	public void setSubtotal(String subtotal) {
		this.subtotal = subtotal;
	}

}
