package com.mx.jmi.ecommerceJMI.entities;

import java.util.List;

public class CaracteristicaTermopar {

	private int pkcaracteristicatermopar;
	private int fkCaracteristicaTermopar;
	private String nombre;
	private String imagen;
	private String descripcion;
	private String estatus;
	private int seleccionado;
	private int pkPadre;
	private int tipo;
	private boolean bloqueado;
	private boolean modificado;
	private List<String> valores;
	private List<PrecioCaracteristicaTermopar> listaPrecio;
	private double value;

	
	public String getImagen() {
		return imagen;
	}

	public void setImagen(String imagen) {
		this.imagen = imagen;
	}

	public List<PrecioCaracteristicaTermopar> getListaPrecio() {
		return listaPrecio;
	}

	public void setListaPrecio(List<PrecioCaracteristicaTermopar> listaPrecio) {
		this.listaPrecio = listaPrecio;
	}

	public double getValue() {
		return value;
	}

	public void setValue(double value) {
		this.value = value;
	}

	public int getFkCaracteristicaTermopar() {
		return fkCaracteristicaTermopar;
	}

	public void setFkCaracteristicaTermopar(int fkCaracteristicaTermopar) {
		this.fkCaracteristicaTermopar = fkCaracteristicaTermopar;
	}

	public List<String> getValores() {
		return valores;
	}

	public void setValores(List<String> valores) {
		this.valores = valores;
	}

	public int getTipo() {
		return tipo;
	}

	public void setTipo(int tipo) {
		this.tipo = tipo;
	}

	public int getPkPadre() {
		return pkPadre;
	}

	public void setPkPadre(int pkPadre) {
		this.pkPadre = pkPadre;
	}

	public CaracteristicaTermopar() {
		super();
	}

	public CaracteristicaTermopar(int pkcaracteristicatermopar, String nombre, String descripcion, String estatus,
			int seleccionado, boolean modificado, int pkPadre, List<String> valores) {
		super();
		this.pkcaracteristicatermopar = pkcaracteristicatermopar;
		this.nombre = nombre;
		this.descripcion = descripcion;
		this.estatus = estatus;
		this.seleccionado = seleccionado;
		this.modificado = modificado;
		this.pkPadre = pkPadre;
		this.valores = valores;
	}

	public boolean isBloqueado() {
		return bloqueado;
	}

	public void setBloqueado(boolean bloqueado) {
		this.bloqueado = bloqueado;
	}

	public boolean isModificado() {
		return modificado;
	}

	public void setModificado(boolean modificado) {
		this.modificado = modificado;
	}

	public int getSeleccionado() {
		return seleccionado;
	}

	public void setSeleccionado(int seleccionado) {
		this.seleccionado = seleccionado;
	}

	public int getPkcaracteristicatermopar() {
		return pkcaracteristicatermopar;
	}

	public void setPkcaracteristicatermopar(int pkcaracteristicatermopar) {
		this.pkcaracteristicatermopar = pkcaracteristicatermopar;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getEstatus() {
		return estatus;
	}

	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}

}
