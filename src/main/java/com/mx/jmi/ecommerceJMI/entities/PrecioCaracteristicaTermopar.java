package com.mx.jmi.ecommerceJMI.entities;

import java.io.Serializable;

public class PrecioCaracteristicaTermopar implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int pkpreciocaracteristica;
	private Double preciomenudeo;
	private Double preciomayoreo;
	private int estatus;

	public int getPkpreciocaracteristica() {
		return pkpreciocaracteristica;
	}

	public void setPkpreciocaracteristica(int pkpreciocaracteristica) {
		this.pkpreciocaracteristica = pkpreciocaracteristica;
	}

	public Double getPreciomenudeo() {
		return preciomenudeo;
	}

	public void setPreciomenudeo(Double preciomenudeo) {
		this.preciomenudeo = preciomenudeo;
	}

	public Double getPreciomayoreo() {
		return preciomayoreo;
	}

	public void setPreciomayoreo(Double preciomayoreo) {
		this.preciomayoreo = preciomayoreo;
	}

	public int getEstatus() {
		return estatus;
	}

	public void setEstatus(int estatus) {
		this.estatus = estatus;
	}

	@Override
	public String toString() {
		return "PrecioCaracteristicaTermopar [pkpreciocaracteristica=" + pkpreciocaracteristica + ", precioMenudeo="
				+ preciomenudeo + ", precioMayoreo=" + preciomayoreo + ", estatus=" + estatus + "]";
	}

}
