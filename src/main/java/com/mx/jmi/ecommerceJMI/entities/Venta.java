package com.mx.jmi.ecommerceJMI.entities;

import java.util.List;

public class Venta {

	private Integer pkventa;
	private String pkEnvio;
	private String pkEnvioSolicitud;
	private String fechaHora;
	private Integer formaPago;
	private Double montoTotal;
	private Double montoSubtotal;
	private Double montoIVA;
	private Cliente cliente;
	private List<Producto> listaPedidos;

	@Override
	public String toString() {
		return "Venta [pkventa=" + pkventa + ", fechaHora=" + fechaHora + ", formaPago=" + formaPago + ", montoTotal="
				+ montoTotal + ", montoSubtotal=" + montoSubtotal + ", montoIVA=" + montoIVA + ", cliente=" + cliente
				+ ", listaPedidos=" + listaPedidos + "]";
	}

	public String getPkEnvio() {
		return pkEnvio;
	}

	public String getPkEnvioSolicitud() {
		return pkEnvioSolicitud;
	}

	public void setPkEnvioSolicitud(String pkEnvioSolicitud) {
		this.pkEnvioSolicitud = pkEnvioSolicitud;
	}

	public void setPkEnvio(String pkEnvio) {
		this.pkEnvio = pkEnvio;
	}

	public Integer getPkventa() {
		return pkventa;
	}

	public void setPkventa(Integer pkventa) {
		this.pkventa = pkventa;
	}

	public String getFechaHora() {
		return fechaHora;
	}

	public void setFechaHora(String fechaHora) {
		this.fechaHora = fechaHora;
	}

	public Integer getFormaPago() {
		return formaPago;
	}

	public void setFormaPago(Integer formaPago) {
		this.formaPago = formaPago;
	}

	public Double getMontoTotal() {
		return montoTotal;
	}

	public void setMontoTotal(Double montoTotal) {
		this.montoTotal = montoTotal;
	}

	public Double getMontoSubtotal() {
		return montoSubtotal;
	}

	public void setMontoSubtotal(Double montoSubtotal) {
		this.montoSubtotal = montoSubtotal;
	}

	public Double getMontoIVA() {
		return montoIVA;
	}

	public void setMontoIVA(Double montoIVA) {
		this.montoIVA = montoIVA;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public List<Producto> getListaPedidos() {
		return listaPedidos;
	}

	public void setListaPedidos(List<Producto> listaPedidos) {
		this.listaPedidos = listaPedidos;
	}

}
