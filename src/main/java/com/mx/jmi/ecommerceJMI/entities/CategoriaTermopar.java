package com.mx.jmi.ecommerceJMI.entities;

import java.util.List;

public class CategoriaTermopar {

	private int pkcategoriatermopar;
	private String nombre;
	private String descripcion;
	private int estatus;
	private List<CaracteristicaTermopar> listaCaracteristicasTermopar;
	
	public CategoriaTermopar() {
		super();
	}

	public CategoriaTermopar(int pkcategoriatermopar, String nombre, String descripcion, int estatus,
			List<CaracteristicaTermopar> listaCaracteristicasTermopar) {
		super();
		this.pkcategoriatermopar = pkcategoriatermopar;
		this.nombre = nombre;
		this.descripcion = descripcion;
		this.estatus = estatus;
		this.listaCaracteristicasTermopar = listaCaracteristicasTermopar;
	}

	public List<CaracteristicaTermopar> getListaCaracteristicasTermopar() {
		return listaCaracteristicasTermopar;
	}

	public void setListaCaracteristicasTermopar(List<CaracteristicaTermopar> listaCaracteristicasTermopar) {
		this.listaCaracteristicasTermopar = listaCaracteristicasTermopar;
	}

	public int getPkcategoriatermopar() {
		return pkcategoriatermopar;
	}

	public void setPkcategoriatermopar(int pkcategoriatermopar) {
		this.pkcategoriatermopar = pkcategoriatermopar;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public int getEstatus() {
		return estatus;
	}

	public void setEstatus(int estatus) {
		this.estatus = estatus;
	}

	@Override
	public String toString() {
		return "CategoriaTermopar [pkcategoriatermopar=" + pkcategoriatermopar + ", nombre=" + nombre + ", descripcion="
				+ descripcion + ", estatus=" + estatus + "]";
	}
	
	

}
