package com.mx.jmi.ecommerceJMI.entities;

public class Sectores {

	private int pkCatalogoSectores;
	private String nombre;

	public int getPkCatalogoSectores() {
		return pkCatalogoSectores;
	}

	public void setPkCatalogoSectores(int pkCatalogoSectores) {
		this.pkCatalogoSectores = pkCatalogoSectores;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Sectores(int pkCatalogoSectores, String nombre) {
		super();
		this.pkCatalogoSectores = pkCatalogoSectores;
		this.nombre = nombre;
	}

}
