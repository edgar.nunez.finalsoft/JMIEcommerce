package com.mx.jmi.ecommerceJMI.entities;

import java.io.Serializable;
import java.util.List;

public class DatoFiscal implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1L;
	private int pkDatoFiscal;
	private String razonSocial;
	private String rfc;
	private String codigoPostal;
	private String pais;
	private String estado;
	private String delegacion;
	private String ciudad;
	private String colonia;
	private String calle;
	private String noExterior;
	private String noInterior;
	private String referencia;
	private int estatus;
	private int index;	
	private String usoCFDI;

	
	public String getUsoCFDI() {
		return usoCFDI;
	}

	public void setUsoCFDI(String usoCFDI) {
		this.usoCFDI = usoCFDI;
	}

	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}

	public String getColonia() {
		return colonia;
	}

	public void setColonia(String colonia) {
		this.colonia = colonia;
	}

	public int getPkDatoFiscal() {
		return pkDatoFiscal;
	}

	public void setPkDatoFiscal(int pkDatoFiscal) {
		this.pkDatoFiscal = pkDatoFiscal;
	}

	public String getRazonSocial() {
		return razonSocial;
	}

	public void setRazonSocial(String razonSocial) {
		this.razonSocial = razonSocial;
	}

	public String getRfc() {
		return rfc;
	}

	public void setRfc(String rfc) {
		this.rfc = rfc;
	}

	public String getCodigoPostal() {
		return codigoPostal;
	}

	public void setCodigoPostal(String codigoPostal) {
		this.codigoPostal = codigoPostal;
	}

	public String getPais() {
		return pais;
	}

	public void setPais(String pais) {
		this.pais = pais;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getDelegacion() {
		return delegacion;
	}

	public void setDelegacion(String delegacion) {
		this.delegacion = delegacion;
	}

	public String getCiudad() {
		return ciudad;
	}

	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}

	public String getCalle() {
		return calle;
	}

	public void setCalle(String calle) {
		this.calle = calle;
	}

	public String getNoExterior() {
		return noExterior;
	}

	public void setNoExterior(String noExterior) {
		this.noExterior = noExterior;
	}

	public String getNoInterior() {
		return noInterior;
	}

	public void setNoInterior(String noInterior) {
		this.noInterior = noInterior;
	}

	public String getReferencia() {
		return referencia;
	}

	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}

	public int getEstatus() {
		return estatus;
	}

	public void setEstatus(int estatus) {
		this.estatus = estatus;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return "DatoFiscal [pkDatoFiscal=" + pkDatoFiscal + ", razonSocial=" + razonSocial + ", rfc=" + rfc
				+ ", codigoPostal=" + codigoPostal + ", pais=" + pais + ", estado=" + estado + ", delegacion="
				+ delegacion + ", ciudad=" + ciudad + ", colonia=" + colonia + ", calle=" + calle + ", noExterior="
				+ noExterior + ", noInterior=" + noInterior + ", referencia=" + referencia + ", estatus=" + estatus
				+ ", index=" + index + ", usoCFDI=" + usoCFDI + "]";
	}

	
	

}
