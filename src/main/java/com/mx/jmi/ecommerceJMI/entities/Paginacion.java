package com.mx.jmi.ecommerceJMI.entities;

import java.util.List;

public class Paginacion {
		
	private int numeroPaginas;
	private List<Elemento> paginas;
	private int numeroProductosPorPagina;
	private int paginaActual;
	private int numeroProductoInicial;
	private int numeroProductoFinal;
	private int numeroProductos;
	private List<Producto> listaProductos;
	private List<Producto> productos;
	
	
	public List<Elemento> getPaginas() {
		return paginas;
	}
	public void setPaginas(List<Elemento> paginas) {
		this.paginas = paginas;
	}
	public int getNumeroProductos() {
		return numeroProductos;
	}
	public void setNumeroProductos(int numeroProductos) {
		this.numeroProductos = numeroProductos;
	}
	public int getNumeroProductoInicial() {
		return numeroProductoInicial;
	}
	public void setNumeroProductoInicial(int numeroProductoInicial) {
		this.numeroProductoInicial = numeroProductoInicial;
	}
	public int getNumeroProductoFinal() {
		return numeroProductoFinal;
	}
	public void setNumeroProductoFinal(int numeroProductoFinal) {
		this.numeroProductoFinal = numeroProductoFinal;
	}
	public int getNumeroPaginas() {
		return numeroPaginas;
	}
	public void setNumeroPaginas(int numeroPaginas) {
		this.numeroPaginas = numeroPaginas;
	}	
	public int getNumeroProductosPorPagina() {
		return numeroProductosPorPagina;
	}
	public void setNumeroProductosPorPagina(int numeroProductosPorPagina) {
		this.numeroProductosPorPagina = numeroProductosPorPagina;
	}
	public int getPaginaActual() {
		return paginaActual;
	}
	public void setPaginaActual(int paginaActual) {
		this.paginaActual = paginaActual;
	}
	public List<Producto> getListaProductos() {
		return listaProductos;
	}
	public void setListaProductos(List<Producto> listaProductos) {
		this.listaProductos = listaProductos;
	}
	public List<Producto> getProductos() {
		return productos;
	}
	public void setProductos(List<Producto> productos) {
		this.productos = productos;
	}
		
	
}
