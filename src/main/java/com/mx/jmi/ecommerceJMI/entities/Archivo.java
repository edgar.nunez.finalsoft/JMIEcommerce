package com.mx.jmi.ecommerceJMI.entities;

public class Archivo {

	private int pkarchivo;
	private String nombre;
	private String descripcion;
	private String url;
	private int tipo;
	private int estatus;

	public int getPkarchivo() {
		return pkarchivo;
	}

	public void setPkarchivo(int pkarchivo) {
		this.pkarchivo = pkarchivo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public int getTipo() {
		return tipo;
	}

	public void setTipo(int tipo) {
		this.tipo = tipo;
	}

	public int getEstatus() {
		return estatus;
	}

	public void setEstatus(int estatus) {
		this.estatus = estatus;
	}

}
